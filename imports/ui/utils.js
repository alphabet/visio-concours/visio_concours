import { Meteor } from 'meteor/meteor';
import { moment } from 'meteor/momentjs:moment';
import Centre from '../api/centre/centre';
import Commission from '../api/commission/commission';
import Concours from '../api/concours/concours';
import Academie from '../api/academie/academie';

export const getListAcademie = () => {
  const centres = Centre.find().fetch() || [];
  const tab = [];
  const added = [];
  centres.forEach((el) => {
    const acad = Academie.findOne({ _id: el.academie });
    if (added.indexOf(acad._id) === -1) {
      tab.push([acad._id, acad.name]);
      added.push(acad._id);
    }
  });
  return tab.sort((a, b) => {
    return a[1] > b[1];
  });
};

export const getListCommission = () => {
  const commissions = Commission.find().fetch() || [];
  const tab = [];
  commissions.forEach((el) => {
    if (tab.find((x) => x.link === el._id) === undefined) {
      const concours = Concours.findOne({ _id: el.concours });
      tab.push({
        view: `Concours ${concours.numero}-${concours.name} - Commission ${el.numCommission}-${el.name}`,
        link: el._id,
      });
    }
  });
  return tab.sort((a, b) => {
    return a.view > b.view;
  });
};

function academieFilter(state, listToFilter) {
  if (state === '') {
    return listToFilter;
  }
  return listToFilter.filter((el) => el.academie === state);
}

export const getListExamen = (state) => {
  const centres = Centre.find().fetch() || [];
  const tab = [];
  academieFilter(state, centres).forEach((el) => {
    if (tab.find((x) => x.link === el._id) === undefined) {
      tab.push({
        view: el.name,
        link: el._id,
      });
    }
  });
  return tab.sort((a, b) => {
    return a.view > b.view;
  });
};

export const getCandidatList = (list, value) => {
  const data = list;
  const tab = [];
  for (let i = 0; i < data.length; i += 1) {
    if (data[i].color === undefined) {
      Meteor.call('list.updateColor', {
        candidatId: data[i]._id,
        color: false,
      });
    }
    if (data[i].commission !== 'undefined' && value === data[i].commission) {
      tab.push(data[i]);
    }
    if (data[i].centre !== 'undefined' && value === data[i].centre) {
      tab.push(data[i]);
    }
  }
  return tab;
};

export const getCandidatByName = (list, name) => {
  const data = list;
  let cand = {};
  data.forEach((el) => {
    if (name === el.candidat) {
      cand = el;
    }
  });
  return cand;
};

export const getConcours = (idCommission) => {
  const commission = Commission.findOne({ _id: idCommission });
  if (commission !== undefined) {
    const concours = Concours.findOne(commission.concours);
    return concours.name;
  }
  return 'Inconnu';
};

export const getAcademieFromCentre = (idCentre) => {
  const centre = Centre.findOne({ _id: idCentre });
  if (centre !== undefined) {
    const academie = Academie.findOne(centre.academie);
    return academie.name;
  }
  return 'Inconnue';
};

export const getAcademie = (idAcademie) => {
  const acad = Academie.findOne(idAcademie);
  if (acad !== undefined) {
    return acad.name;
  }
  return 'Inconnue';
};

export const getCentre = (idCenter) => {
  const centre = Centre.findOne(idCenter);
  return centre.name;
};

export const getNumCommission = (idCommission) => {
  const comm = Commission.findOne({ _id: idCommission });
  return comm.numCommission;
};

// candidateslist

// expandCandidat adds additionnal information to candidat
export const expandCandidat = (candidat) => {
  const commission = Commission.findOne(candidat.commission);
  const concours = Concours.findOne(commission.concours);
  const centre = Centre.findOne(candidat.centre);
  return {
    ...candidat,
    numCom: commission.numCommission,
    nomConcours: concours.name,
    nomCentre: centre.name,
  };
};
export const expandCandidatList = (candidatList) => {
  return candidatList.map((candidat) => expandCandidat(candidat));
};

// (CandidatListComponent) filter per hour of passage + depending on whether they have already passed or not
export const candidatsFilter = (searchInput, startDate, endDate, candidatList) => {
  candidatList.sort((a, b) => {
    if (a.color === false && b.color === true) {
      return -1; // false
    }
    if (a.color === true && b.color === false) {
      return 1; // true
    }
    if (a.color === b.color) {
      if (a.date === b.date) {
        return a.heure > b.heure ? 1 : a.heure < b.heure ? -1 : 0;
      }
      return a.date > b.date ? 1 : a.date < b.date ? -1 : 0;
    }
    return 0;
  });
  const ele = candidatList.filter((el) => {
    const date = moment(el.date).utc().format('D MMM YY');
    const list = el.candidat
      .concat(el.heure)
      .concat(el.nomCentre)
      .concat(el.numCom)
      .concat(el.nomConcours)
      .concat(date)
      .toLowerCase();
    if (startDate === '' && endDate === '') {
      return list.indexOf(searchInput.toLowerCase()) !== -1;
    }
    if (el.date >= startDate && el.date <= endDate) {
      return list.indexOf(searchInput.toLowerCase()) !== -1;
    }
    return false;
  });
  return ele;
};

// select

export const listFilter = (searchInput, data) => {
  const list = data;
  const ele = list.filter((el) => {
    let name = '';
    if (searchInput !== undefined) {
      name = el.view.toLowerCase();
    }
    return name.indexOf(searchInput.toLowerCase()) !== -1;
  });
  return ele;
};
