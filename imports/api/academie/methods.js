import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import { _ } from 'meteor/underscore';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';

import Academie from './academie';
import Centre from '../centre/centre';
import RegEx from '../regExp';

export const createAcademie = new ValidatedMethod({
  name: 'academie.createAcademie',
  validate: Academie.schema.validator(),
  run({ name }) {
    if (!Roles.userIsInRole(this.userId, ['admin', 'saisie'])) {
      throw new Meteor.Error(
        'api.academie.createAcademie.notPermitted',
        'Vos droits sont insuffisants pour réaliser cette action',
      );
    }
    try {
      Academie.insert({
        name,
      });
    } catch (error) {
      if (error.code === 11000) {
        throw new Meteor.Error('api.academie.createAcademie.duplicateEntry', "Nom d'académie déjà existant");
      } else {
        throw error;
      }
    }
  },
});

export const updateAcademie = new ValidatedMethod({
  name: 'academie.updateAcademie',
  validate: new SimpleSchema({
    academieId: {
      type: String,
      regEx: RegEx.Id,
      label: 'Identifiant du Academie',
    },
    data: Academie.schema,
  }).validator({ clean: true }),

  run({ data, academieId }) {
    if (!Roles.userIsInRole(this.userId, ['admin', 'saisie'])) {
      throw new Meteor.Error(
        'api.academie.updateAcademie.notPermitted',
        'Vos droits sont insuffisants pour réaliser cette action',
      );
    }
    // check academie existence
    const academie = Academie.findOne(academieId);
    if (academie === undefined) {
      throw new Meteor.Error('api.academie.updateAcademie.unknownAcademie', 'Identifiant académie inconnu');
    }
    try {
      return Academie.update({ _id: academieId }, { $set: { ...data } });
    } catch (error) {
      if (error.code === 11000) {
        throw new Meteor.Error('api.academie.updateAcademie.duplicateEntry', "Nom d'académie déjà existant");
      } else {
        throw error;
      }
    }
  },
});

export const removeAcademie = new ValidatedMethod({
  name: 'academie.removeAcademie',
  validate: new SimpleSchema({
    academieId: {
      type: String,
      regEx: RegEx.Id,
      label: "Identifiant de l'Académie",
    },
  }).validator(),

  run({ academieId }) {
    if (!Roles.userIsInRole(this.userId, ['admin', 'saisie'])) {
      throw new Meteor.Error(
        'api.academie.removeAcademie.notPermitted',
        'Vos droits sont insuffisants pour réaliser cette action',
      );
    }
    // check academie existence
    const academie = Academie.findOne(academieId);
    if (academie === undefined) {
      throw new Meteor.Error('api.academie.removeAcademie.unknownAcademie', 'Identifiant academie inconnu');
    }
    // check if current user has admin rights
    const authorized = !!this.userId;
    if (!authorized) {
      throw new Meteor.Error('api.academie.removeAcademie.notPermitted', 'Connexion requise');
    }
    if (Centre.find({ academie: `${[academieId]}` }).count() === 0) {
      Academie.remove(academieId);
    } else {
      // Display error message on client
      throw new Meteor.Error(
        'api.academie.removeAcademie.relatedDataStillExist',
        'Des centres utilisent encore cette académie',
      );
    }
  },
});

// Get list of all method names on User
const LISTS_METHODS = _.pluck([createAcademie, updateAcademie, removeAcademie], 'name');

if (Meteor.isServer) {
  // Only allow 5 list operations per connection per second
  DDPRateLimiter.addRule(
    {
      name(name) {
        return _.contains(LISTS_METHODS, name);
      },

      // Rate limit per connection ID
      connectionId() {
        return true;
      },
    },
    5,
    1000,
  );
}
