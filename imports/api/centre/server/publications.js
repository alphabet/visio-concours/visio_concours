import { Meteor } from 'meteor/meteor';
import Centre from '../centre';

// publish all exam centers
Meteor.publish('centre.all', function centreAll() {
  if (!this.userId) {
    return this.ready();
  }
  return Centre.find({}, { sort: { name: 1 }, limit: 1000 });
});

Meteor.publish('centre.one', function centreOne({ centreId }) {
  if (!this.userId) {
    return this.ready();
  }
  return Centre.find({ _id: centreId }, { fields: Centre.publicFields, sort: { name: 1 }, limit: 1 });
});
