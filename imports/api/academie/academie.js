import SimpleSchema from 'simpl-schema';
import { Random } from 'meteor/random';
import { Factory } from 'meteor/dburles:factory';

const Academie = new Mongo.Collection('academie');

// Deny all client-side updates since we will be using methods to manage this collection
Academie.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

Academie.schema = new SimpleSchema({
  name: {
    type: String,
    min: 1,
    index: true,
    unique: true,
    label: "Intitulé de l'académie",
  },
});

Academie.attachSchema(Academie.schema);

Academie.publicFields = {
  _id: 1,
  name: 1,
};

Factory.define('academie', Academie, {
  name: () => Random.id(),
});

export default Academie;
