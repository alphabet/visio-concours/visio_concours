import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import { _ } from 'meteor/underscore';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';

import Commission from './commission';
import Candidat from '../list/list';
import RegEx from '../regExp';

export const checkCode = new ValidatedMethod({
  name: 'commission.checkCode',
  validate: new SimpleSchema({
    idCommission: {
      type: String,
      min: 1,
      label: 'Numéro de commission',
    },
    codeJury: {
      type: String,
      label: "Code d'accès de la commission",
    },
  }).validator(),
  run({ idCommission, codeJury }) {
    const authorized = !!this.userId;
    if (!authorized) {
      throw new Meteor.Error('api.commissions.checkCode.notPermitted', 'Connexion requise');
    }
    const item = Commission.findOne({ _id: idCommission });
    if (item === undefined) return false;
    // no validation by default
    if (item.codeJury === '') return true;
    return item.codeJury === codeJury;
  },
});

export const createCommission = new ValidatedMethod({
  name: 'commission.createCommission',
  validate: Commission.schema.validator({ clean: true }),
  run(data) {
    if (!Roles.userIsInRole(this.userId, ['admin', 'saisie'])) {
      throw new Meteor.Error(
        'api.commission.createCommission.notPermitted',
        'Vos droits sont insuffisants pour réaliser cette action',
      );
    }
    // check that numCommission is unique for this concours
    const concoursComs = Commission.find(
      { concours: data.concours, numCommission: data.numCommission },
      { fields: { _id: 1 }, limit: 1 },
    ).fetch();
    if (concoursComs.length > 0) {
      throw new Meteor.Error(
        'api.commission.createCommission.duplicateEntry',
        'Ce Numéro de commission existe déjà pour ce concours',
      );
    }
    Commission.insert({ ...data });
  },
});

export const updateCommission = new ValidatedMethod({
  name: 'commission.updateCommission',
  validate: new SimpleSchema({
    commissionId: {
      type: String,
      regEx: RegEx.Id,
      label: 'Identifiant de la commission',
    },
    data: Commission.schema,
  }).validator({ clean: true }),

  run({ data, commissionId }) {
    if (!Roles.userIsInRole(this.userId, ['admin', 'saisie'])) {
      throw new Meteor.Error(
        'api.commission.updateCommission.notPermitted',
        'Vos droits sont insuffisants pour réaliser cette action',
      );
    }
    // check commission existence
    const commission = Commission.findOne(commissionId);
    if (commission === undefined) {
      throw new Meteor.Error('api.commission.updateCommission.unknownCommission', 'Identifiant de commission inconnu');
    }
    const authorized = !!this.userId;
    if (!authorized) {
      throw new Meteor.Error('api.commission.updateCommission.notPermitted', 'Connexion requise');
    }
    // check that numCommission is unique for this concours
    const concoursComs = Commission.find(
      { concours: data.concours, numCommission: data.numCommission },
      { fields: { _id: 1 }, limit: 2 },
    ).fetch();
    concoursComs.forEach((entry) => {
      if (entry._id !== commissionId) {
        throw new Meteor.Error(
          'api.commission.updateCommission.duplicateEntry',
          'Ce Numéro de commission existe déjà pour ce concours',
        );
      }
    });
    return Commission.update({ _id: commissionId }, { $set: { ...data } }, { removeEmptyStrings: false });
  },
});

export const removeCommission = new ValidatedMethod({
  name: 'commission.removeCommission',
  validate: new SimpleSchema({
    commissionId: {
      type: String,
      regEx: RegEx.Id,
      label: 'Identifiant de la commission',
    },
  }).validator(),

  run({ commissionId }) {
    if (!Roles.userIsInRole(this.userId, ['admin', 'saisie'])) {
      throw new Meteor.Error(
        'api.commission.removeCommission.notPermitted',
        'Vos droits sont insuffisants pour réaliser cette action',
      );
    }
    // check commission existence
    const commission = Commission.findOne(commissionId);
    if (commission === undefined) {
      throw new Meteor.Error(
        'api.commission.removeCommission.unknownCommission',
        'Identifiant de la commission inconnu',
      );
    }
    // check if user is authenticated
    const authorized = !!this.userId;
    if (!authorized) {
      throw new Meteor.Error('api.commission.removeCommission.notPermitted', 'Connexion requise');
    }
    if (Candidat.find({ commission: `${[commissionId]}` }).count() === 0) {
      Commission.remove(commissionId);
    } else {
      // Display error message on client
      throw new Meteor.Error(
        'api.commission.removeCommission.relatedDataStillExist',
        'Des candidats utilisent encore cette commission',
      );
    }
  },
});

// Get list of all method names on User
const LISTS_METHODS = _.pluck([checkCode, createCommission, updateCommission, removeCommission], 'name');

if (Meteor.isServer) {
  // Only allow 5 list operations per connection per second
  DDPRateLimiter.addRule(
    {
      name(name) {
        return _.contains(LISTS_METHODS, name);
      },

      // Rate limit per connection ID
      connectionId() {
        return true;
      },
    },
    5,
    1000,
  );
}
