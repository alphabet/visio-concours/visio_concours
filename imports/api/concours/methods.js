import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import { _ } from 'meteor/underscore';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';

import Concours from './concours';
import Commission from '../commission/commission';
import List from '../list/list';
import Log from '../log/log';
import RegEx from '../regExp';

export const createConcours = new ValidatedMethod({
  name: 'concours.createConcours',
  validate: Concours.schema.validator({ clean: true }),
  run({ name, numero, start, end }) {
    if (!Roles.userIsInRole(this.userId, ['admin', 'saisie'])) {
      throw new Meteor.Error(
        'api.concours.createConcours.notPermitted',
        'Vos droits sont insuffisants pour réaliser cette action',
      );
    }
    if (end < start) {
      throw new Meteor.Error(
        'api.concours.createConcours.dateMismatch',
        'La date de fin du concours doit être postérieure à la date de début',
      );
    }
    try {
      Concours.insert({
        name,
        numero,
        start,
        end,
      });
    } catch (error) {
      if (error.code === 11000) {
        const errMsg = error.errmsg.includes('index: c2_name')
          ? 'Nom de concours déjà existant'
          : 'Numéro de concours déjà existant';
        throw new Meteor.Error('api.concours.createConcours.duplicateEntry', errMsg);
      } else {
        throw error;
      }
    }
  },
});

export const updateConcours = new ValidatedMethod({
  name: 'concours.updateConcours',
  validate: new SimpleSchema({
    concoursId: {
      type: String,
      regEx: RegEx.Id,
      label: 'Identifiant du Concours',
    },
    data: Concours.schema,
  }).validator({ clean: true }),

  run({ data, concoursId }) {
    if (!Roles.userIsInRole(this.userId, ['admin', 'saisie'])) {
      throw new Meteor.Error(
        'api.concours.updateConcours.notPermitted',
        'Vos droits sont insuffisants pour réaliser cette action',
      );
    }
    if (data.end < data.start) {
      throw new Meteor.Error(
        'api.concours.updateConcours.dateMismatch',
        'La date de fin du concours doit être postérieure à la date de début',
      );
    }
    // check concours existence
    const concours = Concours.findOne(concoursId);
    if (concours === undefined) {
      throw new Meteor.Error('api.concours.updateConcours.unknownConcours', 'Identifiant du concours inconnu');
    }
    try {
      return Concours.update({ _id: concoursId }, { $set: { ...data } });
    } catch (error) {
      if (error.code === 11000) {
        const errMsg = error.errmsg.includes('index: c2_name')
          ? 'Nom de concours déjà existant'
          : 'Numéro de concours déjà existant';
        throw new Meteor.Error('api.concours.updateConcours.duplicateEntry', errMsg);
      } else {
        throw error;
      }
    }
  },
});

export const removeConcours = new ValidatedMethod({
  name: 'concours.removeConcours',
  validate: new SimpleSchema({
    concoursId: {
      type: String,
      regEx: RegEx.Id,
      label: 'Identifiant du Concours',
    },
  }).validator(),

  run({ concoursId }) {
    if (!Roles.userIsInRole(this.userId, ['admin', 'saisie'])) {
      throw new Meteor.Error(
        'api.concours.removeConcours.notPermitted',
        'Vos droits sont insuffisants pour réaliser cette action',
      );
    }
    // check concours existence
    const concours = Concours.findOne(concoursId);
    if (concours === undefined) {
      throw new Meteor.Error('api.concours.removeConcours.unknownConcours', 'Identifiant du concours inconnu');
    }
    if (Commission.find({ concours: `${[concoursId]}` }).count() === 0) {
      Concours.remove(concoursId);
    } else {
      // Display error message on client
      throw new Meteor.Error(
        'api.concours.removeConcours.relatedDataStillExist',
        'Des commissions utilisent encore ce concours',
      );
    }
  },
});

export const purgeConcours = new ValidatedMethod({
  name: 'concours.purgeConcours',
  validate: new SimpleSchema({
    concoursIdList: {
      type: Array,
      label: "Liste d'Identifiant du Concours",
    },
    'concoursIdList.$': {
      type: String,
      regEx: RegEx.Id,
    },
  }).validator(),

  run({ concoursIdList }) {
    // check if current user has admin rights
    if (!Roles.userIsInRole(this.userId, ['admin'])) {
      throw new Meteor.Error(
        'api.concours.purgeConcours.notPermitted',
        'Vos droits sont insuffisants pour réaliser cette action',
      );
    }
    concoursIdList.forEach((concoursId) => {
      // retrieve all commissions
      const commissionIdList = Commission.find({ concours: concoursId }, { fields: { _id: 1 } })
        .fetch()
        .map((el) => el._id);
      commissionIdList.forEach((commissionId) => {
        List.remove({ commission: commissionId });
        Commission.remove(commissionId);
      });
      // retrieve all Logs for this concours
      const { numero } = Concours.findOne(concoursId);
      Log.remove({ concoursNumero: numero });
      // remove concours
      Concours.remove(concoursId);
    });
  },
});

// Get list of all method names on User
const LISTS_METHODS = _.pluck([createConcours, updateConcours, removeConcours, purgeConcours], 'name');

if (Meteor.isServer) {
  // Only allow 5 list operations per connection per second
  DDPRateLimiter.addRule(
    {
      name(name) {
        return _.contains(LISTS_METHODS, name);
      },

      // Rate limit per connection ID
      connectionId() {
        return true;
      },
    },
    5,
    1000,
  );
}
