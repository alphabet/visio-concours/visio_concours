import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useRouteMatch, useHistory, Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

import { listFilter } from '../utils';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
  titleBackgroud: {
    background: '#99ddff',
    borderRadius: '3px 3px 0 0',
    display: 'flex',
    justifyContent: 'space-between',
  },
  title: {
    margin: theme.spacing(3),
    fontSize: '120%',
    alignContent: 'flex-start',
  },
  titleRight: {
    alignContent: 'flex-end',
  },
  color: {
    background: '#99ddff',
  },
  toLeft: {
    width: '15%',
    margin: `${theme.spacing(1)}px 0`,
  },
  button: {
    margin: theme.spacing(3),
  },
  searchBar: {
    width: '10%',
    margin: theme.spacing(3),
  },
  table: {
    margin: `0px ${theme.spacing(3)}px`,
    width: '500px',
  },
}));

function Select({ title, data }) {
  const classes = useStyles();
  const history = useHistory();
  const match = useRouteMatch();
  const [searchInput, setSearchInput] = useState('');

  const linkToCandidat = (value) => {
    return (
      <Button component={Link} to={`${match.url}/${value}`} color="primary" variant="outlined">
        voir
      </Button>
    );
  };

  const handleSearchBar = (e) => {
    setSearchInput(e.target.value);
  };

  const listNoms = () => {
    const list = listFilter(searchInput, data).map((el) => {
      return (
        <TableRow key={el.link} hover>
          <TableCell>{linkToCandidat(el.link)}</TableCell>
          <TableCell>{el.view}</TableCell>
        </TableRow>
      );
    });
    return list;
  };

  return (
    <Paper className={classes.root}>
      <div className={classes.titleBackgroud}>
        <p className={classes.title}>{title}</p>
        <div className={classes.titleRight}>
          <TextField
            autoFocus
            className={classes.button}
            placeholder="Recherche"
            onChange={handleSearchBar}
            value={searchInput}
          />
          <Button className={classes.button} onClick={() => history.push('/home')} color="secondary" variant="outlined">
            Retour
          </Button>
        </div>
      </div>
      <div>
        <Table>
          <TableBody>{listNoms()}</TableBody>
        </Table>
      </div>
      <div className={classes.toLeft}>
        <Button className={classes.button} onClick={() => history.push('/home')} color="secondary" variant="outlined">
          Retour
        </Button>
      </div>
    </Paper>
  );
}

Select.propTypes = {
  data: PropTypes.arrayOf(PropTypes.element).isRequired,
  title: PropTypes.string.isRequired,
};

export default Select;
