# Ajouter un concours directement dans mongo sans tout réimporter

En fait c'est la collection 'commission' qui est concernée

Récupérer le nombre total de commission :

```
db.commission.find().count();
50
```

Insertion de la nouvelle entrée (les 51 viennent de la commande précédente +1) :

```
db.commission.insert({"_id": "8j6XxAhW7d4RTiQHS", "concoursNum" : "51", "concoursName" : "1500A-EAD-CRETEIL-PARIS-VERSAIL.", "name" : "Commission pour 1500A-EAD-CRETEIL-PARIS-VERSAIL.", "numCommission" : "51-1", "codeJury" : "test"});
WriteResult({ "nInserted" : 1 })
```

# Ajouter un user directement dans mongo sans tout réimporter

(ici le mot de passe sera 'eole')

```
db.users.insert({services: {password: {bcrypt: '$2a$10$w91o/mi5lRyuPdDKSUaZZOdcSXw1NfLRXY4LV7WajsqsVs5.OAw2u'}}, _id: '9XyT9XjN7TmscZkdJ', username: 'tutu'});
```

# Exemple d'id générés par Meteor :

La valeur de "\_id" est à prendre parmis celles-ci :

```
9XyT9XjN7TmscZkdJ z2wEbbzoc5dHc9QPg HumLkeTi87Yd7p3n3 ADmqZNfn9FG76Exm4 CJZYK24qJ6Lsm3XCe bHJGJwcxbbE5dXmtz ak9nZncLcqkzhjP4X B2LNwEi6hKLJsg62n XXRXPhbfrrrojioH6 Qw8pgXYhhtJWxqh2w WcJmLtaoAEqyYiiqB icNmMTDhfRB9z6vZv pk4q9n4nLbuhhrK9B HNhDxQBnJGt6XBgAf BJ5F8jzSpKuz7o5BW 7EA2Q9PgDZTa2FNpY bSyg9zgw9u4WpLdLa hiHmqyjcRj2z6Ks8y GvZPAMfMqHCB4EQfx LjqPiWsa7ZGiQHMwy TMrLR6BRoModbiMZP DPATGE5bTNQgxK98f cwA4woSyAsedaDZTu 2h2WLt5jjHPpgcGq4 4G2a8k6iscWnfJiTm fKx4JZkbAngXc4c87 FQbTcRWykSWhK6KY3 HKwbBaFkte485775s Frv6EkvPwTTygR7bH vnjHkgf7mnBacLJYJ nPNYwFbF9pnkLCJ4r eTimfNM2PQJG5NRXK zZqFR2mWL6BW69WpE d43yRsrMCSxS6PPaZ b5uP2bWrN4dfeWDpe 6uqwRfYwoqmh3zDe3 kgfEA425RFBHtmhxt f2ynBbaRvTfA4gTDY uk2LNT7FomEx78Chv wyppy8YZjWWDkgdGS QbpdmvukPjQHiJX9B mEEPhpAcpwAB642Ev vGeXxw5wevKPZ9aJt edGosiXqWzGn8NM33 myL3uzTey6tP99ECr pe7myPJWH7h8L7uLt dCEh5fJc37XEYj3xB m3BK3e7ZJrANAi2EE haenNrczFc7gNcdTP QoCpF5BBDNQQf8job P6cyr5sYQWRB4q7de p4Hy6Cdg7YGSG8sqM qfwYZSECHXtzjQWAp ZwSp63njyCF3f2nTi b7fxFjxACKJ5ZYAuW t96ie5wK4Epxadit9 kRanbZGymYenY4tXK dM5phTYR4f2j87Yih uQQgXEZ68hAwq4E5d r4GkSwFh49YmbzkhH z72kXs5Dd4cgEs92G 3MQFDt5kjCNNZYjCE QWQ3ocHto2P55x5T3 PgbFfkzvMn8tH3ipy tsMhXhPezGCKgZ8Mn ejsc7NZ3P2C3xRAjw BkNzibWG8sCzJrq5n FpXKYpev5ZRoaQtxc ujhjEjYRY5N7L4TiN op6CxbYYNtSCZ8Hv9 N88k9pxjJZNmEpgEx cEPusawttWWTb4vA8 bx8cATGFc5NDxTdQs M5wKBzBL6AZyoZy8F rMEEXQAharLGC3pjx EjNp5rr46s3tquoiy eiPoWXRhvn7TKEBxD x3pZ7NeqRsc9BupQY ebNH2Nd8ESs52Q4Qj z4gMGqQFKMswuYkQT YuGE2hcbDuPPD6HsX Rg5wbhtJtsM7Cwdua L4Er7AF3YAc67KL97 PLa8AHeesyaTxT6fq CfCwMYD8nYSNMi7zE 88w6ocpCva342k9ym WAJNCqccSSrmoPsQT xHtwDe9xGj7cduKSa Xin7QSWG6Jaq2zcHs 8vcgFYjRY3AbPeCCa nrqR8tyj8ZAjHK7LB oo6rLCjovLpP4YvvA krkm8qBqk2GiB9Fnt 8MxLMJAFK7f336bXd gyvAAqJxQEiTcdEKP dCe5Rb96Zo9AJzRmk 62E2QvMdS9f5JdppT Fz6msj9nEn7J2ADb7 7LeJLAFwYZjdkGnjN 6ZApKDt7RmWwJxm3Z P4wDTL3spGsupBRyj JL3idSP6GEsg8F7gR cMu24zvYFR6TDoRRr zrzxMxWoMserPzQPK dtjE4CaAagGW6qzYr meqTyaK7xXBMBhWkt o2BvM8tqkQL4G5zin S3JQdHEYCumKfQndo Gf7ta6gpFRL3bDtLo rahmFEhGQ3HuK7mw9 Ad23RJS4jqKEPyMkJ 3f62z7tWHzq5enmqB hk6bSsDMbaQ55Neva gFJD322Jp4ARNRQSD vrEeTAvtkRDhtWMCj o7pxJjMMmhzfdWC6Y DNWBaYb3eyL2KusCu y7rjWi47ZiruLYapu wSTCrfkTyr4BwbFpa GppYyjygwAuXB2B87 2d6QRABuYAXy5iTb6 yNyWDiWaNsDxWedt9 w8itiRM4TLTr62BLg RGt3P5etSXqYASPor YwNbpgh34hmRjRNN9 DQw795Ry5avADgaRa
```
