import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import React, { useState, useEffect } from 'react';
import { Link, Route, Switch, useHistory } from 'react-router-dom';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { moment } from 'meteor/momentjs:moment';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { Grid, Typography } from '@material-ui/core';
import List from '../../api/list/list';
import {
  getListAcademie,
  getListCommission,
  getListExamen,
  getCandidatList,
  getConcours,
  getAcademie,
  getAcademieFromCentre,
  getCentre,
  getNumCommission,
} from '../utils';
import Select from './select';
import CandidatListComponent from './candidateslist';
import SaisieCandidat from './saisiecandidat';
import SaisieCentre from './saisiecentre';
import SaisieCommission from './saisiecommission';
import SaisieConcours from './saisieconcours';
import SaisieAcademie from './saisieacademie';
import AdminCandidats from './admincandidats';
import AdminCentres from './admincentres';
import AdminCommissions from './admincommissions';
import AdminConcours from './adminconcours';
import AdminAcademies from './adminacademies';
import AdminLogs from './adminlogs';
import AdminUsers from './adminusers';
import AdminPurge from './adminpurge';
import Password from './Password';
import Spinner from '../components/Spinner';
import NotFound from './notfound';
import AdminImport from './adminimport';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  div: {
    padding: theme.spacing(3),
    position: 'relative',
    textAlign: 'center',
    backgroundColor: 'white',
    marginTop: theme.spacing(3),
  },
  space: {
    margin: theme.spacing(3),
  },
  grid: {
    justifyContent: 'center',
  },
  button: {
    width: '300px',
    height: '100px',
    fontSize: '20px',
  },
  disconnect: {
    width: '150px',
    height: '60px',
    fontSize: '15px',
  },
}));

function Home(props) {
  const classes = useStyles();
  const [menuAca, setMenuAca] = useState(null);
  const [academie, setAcademie] = useState('');
  const [formulars, setFormulars] = useState(null);
  const [userRoles, setUserRoles] = useState([]);
  const history = useHistory();
  const { list, currentUser, loggingIn, loading, roles } = props;

  useEffect(() => {
    if (currentUser) {
      const newRoles = Roles.getRolesForUser(currentUser._id);
      if (newRoles.length !== userRoles.length) {
        setUserRoles(newRoles);
      }
    }
  }, [currentUser, roles]);

  const disconnect = () => {
    Meteor.logout();
    history.push('/');
  };

  const changePassword = () => {
    history.push('/password');
  };

  const handleAcaClick = (event) => {
    setAcademie(event.currentTarget.id);
    setMenuAca(null);
  };

  const handleMenuAcaClick = (event) => {
    setMenuAca(event.currentTarget);
  };

  const handleMenuAcaClose = () => {
    setMenuAca(null);
  };

  const handleFormularClick = () => {
    setFormulars(null);
  };

  const handleMenuAdminClick = (event) => {
    setFormulars(event.currentTarget);
  };

  const handleFormularClose = () => {
    setFormulars(null);
  };

  const CandidateByList = ({ match }) => {
    return (
      <Switch>
        <Route exact path={match.url}>
          {match.path === '/commission/:idCom' ? (
            <CandidatListComponent
              candidatList={getCandidatList(list, match.params.idCom)}
              path="/commission"
              thirdColumnTitle="centre d'accueil du candidat"
              place={`pour le Concours ${getConcours(match.params.idCom)} - Commission ${getNumCommission(
                match.params.idCom,
              )} `}
              commission={match.params.idCom}
              match={match}
            />
          ) : (
            <CandidatListComponent
              candidatList={getCandidatList(list, match.params.idCenter)}
              path="/centredexamen"
              thirdColumnTitle="commission de jury"
              place={`du centre d'accueil ${getCentre(match.params.idCenter)} de l'académie ${getAcademieFromCentre(
                match.params.idCenter,
              )}`}
              match={match}
            />
          )}
        </Route>
      </Switch>
    );
  };

  CandidateByList.propTypes = {
    match: PropTypes.objectOf(PropTypes.element).isRequired,
  };

  const Commission = () => {
    return (
      <Switch>
        <Route exact path="/commission">
          <Select data={getListCommission()} title="Liste des commissions de jury" />
        </Route>
        <Route path="/commission/:idCom" component={CandidateByList} />
      </Switch>
    );
  };

  const CentreDexamen = () => {
    return (
      <Switch>
        <Route exact path="/centredexamen">
          <Select
            data={getListExamen(academie)}
            title={
              academie
                ? `Liste des centres d'accueil de l'académie de ${getAcademie(academie)}`
                : "Liste de tous les centres d'examens"
            }
          />
        </Route>
        <Route path="/centredexamen/:idCenter" component={CandidateByList} />
      </Switch>
    );
  };

  const AdminMenu = () => {
    return (
      <Menu id="admin-menu" anchorEl={formulars} keepMounted open={Boolean(formulars)} onClose={handleFormularClose}>
        {[
          {
            roles: ['saisie'],
            path: '/admincandidats',
            label: 'Candidats',
          },
          {
            roles: ['saisie'],
            path: '/admincommissions',
            label: 'Commissions',
          },
          {
            roles: ['saisie'],
            path: '/adminconcours',
            label: 'Concours',
          },
          {
            roles: ['saisie'],
            path: '/admincentres',
            label: 'Centres',
          },
          {
            roles: ['saisie'],
            path: '/adminacademies',
            label: 'Académies',
          },
          {
            roles: ['admin'],
            path: '/adminusers',
            label: 'Utilisateurs',
          },
          {
            roles: ['admin'],
            path: '/adminpurge',
            label: 'Purge base',
          },
          {
            roles: ['admin'],
            path: '/adminimport',
            label: 'Import données',
          },
          {
            roles: ['admin'],
            path: '/adminlogs',
            label: 'Consultation des logs',
          },
        ].map((a) => {
          let allowed = false;
          if (a.roles === undefined || userRoles.includes('admin')) {
            allowed = true;
          } else {
            // not admin and roles required, check that user has at least one of them
            a.roles.forEach((role) => {
              if (userRoles.includes(role)) allowed = true;
            });
          }
          if (allowed) {
            return (
              <MenuItem key={a.label} component={Link} to={a.path} onClick={handleFormularClick}>
                {a.label}
              </MenuItem>
            );
          }
          return null;
        })}
      </Menu>
    );
  };

  moment.locale('fr');
  return (
    <div>
      {!currentUser && !loggingIn ? (
        disconnect()
      ) : currentUser && !loading ? (
        <Switch>
          <Route exact path="/home">
            <div className={classes.root}>
              <div className={classes.space} />
              <div className={classes.div}>
                <Typography>
                  Utilisateur connecté : <b>{currentUser.username}</b>
                </Typography>
                <div className={classes.space} />
                <Button className={classes.button} color="primary" variant="outlined" component={Link} to="/commission">
                  jurys
                </Button>
                <div className={classes.space} />
                <Button
                  className={classes.button}
                  aria-controls="simple-menu"
                  aria-haspopup="true"
                  color="primary"
                  variant="outlined"
                  onClick={handleMenuAcaClick}
                >
                  candidats
                </Button>
                <Menu
                  id="simple-menu"
                  anchorEl={menuAca}
                  keepMounted
                  open={Boolean(menuAca)}
                  onClose={handleMenuAcaClose}
                >
                  {getListAcademie().map((acad) => (
                    <MenuItem key={acad[0]} component={Link} to="/centredexamen" id={acad[0]} onClick={handleAcaClick}>
                      {acad[1]}
                    </MenuItem>
                  ))}
                </Menu>
                <div className={classes.space} />
                {Roles.userIsInRole(currentUser._id, ['admin', 'saisie']) ? (
                  <>
                    <Button
                      className={classes.button}
                      aria-controls="admin-menu"
                      aria-haspopup="true"
                      color="primary"
                      variant="outlined"
                      onClick={handleMenuAdminClick}
                    >
                      Administration
                    </Button>
                    <AdminMenu />
                  </>
                ) : null}
                <div className={classes.space} />
                <Grid className={classes.grid} container spacing={2}>
                  {Meteor.settings.public.enableKeycloak ? null : (
                    <Grid item>
                      <Button
                        className={classes.disconnect}
                        aria-controls="simple-menu"
                        aria-haspopup="true"
                        color="secondary"
                        variant="outlined"
                        onClick={changePassword}
                        component={Link}
                        to="/password"
                      >
                        Changer de mot de passe
                      </Button>
                    </Grid>
                  )}
                  <Grid item>
                    <Button
                      className={classes.disconnect}
                      aria-controls="simple-menu"
                      aria-haspopup="true"
                      color="secondary"
                      variant="outlined"
                      onClick={disconnect}
                      component={Link}
                      to="/"
                    >
                      Déconnexion
                    </Button>
                  </Grid>
                </Grid>
              </div>
            </div>
          </Route>
          <Route path="/commission" component={Commission} />
          <Route path="/centredexamen" component={CentreDexamen} />
          <Route exact path="/password" component={Password} />
          <Route exact path="/admincandidats" component={AdminCandidats} />
          <Route exact path="/admincandidats/new" component={SaisieCandidat} />
          <Route exact path="/admincandidats/:id" component={SaisieCandidat} />
          <Route exact path="/admincentres" component={AdminCentres} />
          <Route exact path="/admincentres/new" component={SaisieCentre} />
          <Route exact path="/admincentres/:id" component={SaisieCentre} />
          <Route exact path="/admincommissions" component={AdminCommissions} />
          <Route exact path="/admincommissions/new" component={SaisieCommission} />
          <Route exact path="/admincommissions/:id" component={SaisieCommission} />
          <Route exact path="/adminconcours" component={AdminConcours} />
          <Route exact path="/adminconcours/new" component={SaisieConcours} />
          <Route exact path="/adminconcours/:id" component={SaisieConcours} />
          <Route exact path="/adminacademies" component={AdminAcademies} />
          <Route exact path="/adminacademies/new" component={SaisieAcademie} />
          <Route exact path="/adminacademies/:id" component={SaisieAcademie} />
          <Route exact path="/adminusers" component={AdminUsers} />
          <Route exact path="/adminpurge" component={AdminPurge} />
          <Route exact path="/adminimport" component={AdminImport} />
          <Route exact path="/adminlogs" component={AdminLogs} />
          <Route component={NotFound} />
        </Switch>
      ) : (
        <Spinner />
      )}
    </div>
  );
}

Home.defaultProps = {
  currentUser: null,
};

Home.propTypes = {
  loggingIn: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  currentUser: PropTypes.objectOf(PropTypes.element),
  list: PropTypes.arrayOf(PropTypes.element).isRequired,
  roles: PropTypes.arrayOf(PropTypes.element).isRequired,
};

const track = withTracker(() => {
  const loggingIn = Meteor.loggingIn();
  const subList = Meteor.subscribe('list.all');
  const subAca = Meteor.subscribe('academie.all');
  const subCentre = Meteor.subscribe('centre.all');
  const subConcours = Meteor.subscribe('concours.all');
  const subCommission = Meteor.subscribe('commission.all');
  const loading =
    !subCentre.ready() || !subCommission.ready() || !subConcours.ready() || !subAca.ready() || !subList.ready();
  return {
    loggingIn,
    loading,
    list: List.find({}).fetch(),
    currentUser: Meteor.user(),
    roles: Meteor.roleAssignment.find().fetch() || [],
  };
})(Home);

export default track;
