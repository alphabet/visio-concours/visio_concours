import React, { useState, useEffect } from 'react';
import { Meteor } from 'meteor/meteor';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data';
import { FormGroup, Paper, TextField, Button, Snackbar } from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import { useHistory } from 'react-router-dom';
import Concours from '../../api/concours/concours';

const useStyles = makeStyles((theme) => ({
  main: {
    display: 'flex',
  },
  paper: {
    marginTop: theme.spacing(3),
    margin: 'auto',
    maxWidth: 800,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  validate: {
    display: 'flex',
  },
  spaceBetween: {
    width: 10,
  },
  button: { width: 150, margin: theme.spacing(1) },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function SaisieConcours({ concours }) {
  const history = useHistory();
  const classes = useStyles();

  let infosConcours = {
    concoursName: '',
    concoursNum: '',
    concoursStart: new Date().toISOString().substring(0, 10),
    concoursEnd: new Date().toISOString().substring(0, 10),
  };
  const [state, setState] = useState(infosConcours);

  useEffect(() => {
    if (concours.name !== undefined) {
      infosConcours = {
        concoursName: concours.name,
        concoursNum: concours.numero,
        concoursStart: concours.start.toISOString().substring(0, 10),
        concoursEnd: concours.end.toISOString().substring(0, 10),
      };
      setState(infosConcours);
    }
  }, [concours]);

  const [openSnack, setOpenSnack] = React.useState(false);
  const [snackData, setSnackData] = React.useState({
    text: '',
    severity: 'success',
  });

  const showAlert = (text, severity = 'success') => {
    setSnackData({ text, severity });
    setOpenSnack(true);
  };

  const handleCloseSnack = (reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnack(false);
  };

  const handleChange = (event) => {
    const { name } = event.target;
    setState({
      ...state,
      [name]: event.target.value,
    });
  };

  const handleResult = (goBack, err) => {
    if (err) {
      let message = err.reason;
      if (err.error === 'validation-error') {
        // display first validation message
        message = err.details.map((info) => info.message).join('\n');
      }
      showAlert(message, 'error');
      if (typeof err.error === 'string' && err.error.includes('duplicateEntry')) {
        document.getElementById('concoursNum').focus();
      } else {
        document.getElementById('concoursName').focus();
      }
    } else if (goBack) {
      history.push('/adminconcours');
    } else if (concours.name === undefined) {
      showAlert('Le concours a été ajouté');
      setState({
        ...state,
        concoursName: '',
        concoursNum: '',
      });
      document.getElementById('concoursName').value = '';
      document.getElementById('concoursNum').value = '';
      document.getElementById('concoursNum').focus();
    } else {
      showAlert('Le concours a été mis à jour.');
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const goBack = event.type !== 'submit';
    const { concoursName, concoursNum, concoursStart, concoursEnd } = state;

    if (concoursName === '') {
      showAlert('Un nom de concours est nécessaire !', 'error');
      document.getElementById('concoursName').focus();
      return false;
    }

    if (concoursNum === '') {
      showAlert('Un numéro de concours est nécessaire !', 'error');
      document.getElementById('concoursNum').focus();
      return false;
    }

    if (concoursStart === '') {
      showAlert('Une date de début est nécessaire !', 'error');
      document.getElementById('concoursStart').focus();
      return false;
    }

    if (concoursEnd === '') {
      showAlert('Une date de fin est nécessaire !', 'error');
      document.getElementById('concoursEnd').focus();
      return false;
    }

    const concData = {
      name: concoursName,
      numero: concoursNum,
      start: concoursStart,
      end: concoursEnd,
    };
    if (concours.name === undefined) {
      Meteor.call('concours.createConcours', concData, (err) => handleResult(goBack, err));
    } else {
      Meteor.call(
        'concours.updateConcours',
        {
          concoursId: concours._id,
          data: concData,
        },
        (err) => handleResult(goBack, err),
      );
    }
    return false;
  };

  const renderTextfield = () => {
    return (
      <div className={classes.formControl}>
        <TextField
          name="concoursNum"
          margin="dense"
          id="concoursNum"
          label="Numéro du concours"
          value={state.concoursNum}
          fullWidth
          onChange={handleChange}
        />
        <TextField
          name="concoursName"
          margin="dense"
          id="concoursName"
          label="Nom du concours"
          value={state.concoursName}
          fullWidth
          onChange={handleChange}
        />
        <TextField
          name="concoursStart"
          margin="dense"
          id="concoursStart"
          label="Début du concours"
          type="date"
          value={state.concoursStart}
          fullWidth
          onChange={handleChange}
        />
        <TextField
          name="concoursEnd"
          margin="dense"
          id="concoursEnd"
          label="Fin du concours"
          type="date"
          value={state.concoursEnd}
          fullWidth
          onChange={handleChange}
        />
      </div>
    );
  };

  return (
    <div className={classes.main}>
      <Paper className={classes.paper}>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={openSnack}
          autoHideDuration={5000}
          onClose={handleCloseSnack}
        >
          <Alert onClose={handleCloseSnack} severity={snackData.severity}>
            {snackData.text}
          </Alert>
        </Snackbar>
        <form onSubmit={handleSubmit}>
          <FormGroup>{renderTextfield()}</FormGroup>
          <div className={classes.validate}>
            <Button
              className={classes.button}
              variant="outlined"
              color="secondary"
              onClick={() => history.push('/adminconcours')}
            >
              Retour
            </Button>
            <div className={classes.spaceBetween} />
            {concours.name === undefined ? (
              <>
                <Button type="submit" variant="outlined" color="primary" className={classes.button}>
                  Soumettre et continuer
                </Button>
                <Button
                  type="button"
                  variant="outlined"
                  color="primary"
                  className={classes.button}
                  onClick={handleSubmit}
                >
                  Soumettre et retour
                </Button>
              </>
            ) : (
              <Button
                type="button"
                variant="outlined"
                color="primary"
                className={classes.button}
                onClick={handleSubmit}
              >
                Modifier
              </Button>
            )}
          </div>
        </form>
      </Paper>
    </div>
  );
}

SaisieConcours.propTypes = {
  concours: PropTypes.objectOf(PropTypes.element).isRequired,
};

export default withTracker(
  ({
    match: {
      params: { id },
    },
  }) => {
    let concours = {};
    if (id !== 'new') {
      Meteor.subscribe('concours.one', { concoursId: id });
      concours = Concours.findOne({ _id: id }) || {};
    }
    return {
      concours,
    };
  },
)(SaisieConcours);
