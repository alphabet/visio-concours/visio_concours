import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import { _ } from 'meteor/underscore';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';

import Centre from './centre';
import Candidat from '../list/list';
import RegEx from '../regExp';

export const createCenter = new ValidatedMethod({
  name: 'center.createCenter',
  validate: Centre.schema.validator(),
  run({ name, academie, numCentre }) {
    if (!Roles.userIsInRole(this.userId, ['admin', 'saisie'])) {
      throw new Meteor.Error(
        'api.center.createCenter.notPermitted',
        'Vos droits sont insuffisants pour réaliser cette action',
      );
    }
    try {
      Centre.insert({
        name,
        numCentre,
        academie,
      });
    } catch (error) {
      if (error.code === 11000) {
        throw new Meteor.Error('api.center.createCenter.duplicateEntry', "Numéro de centre d'examen déjà existant");
      } else {
        throw error;
      }
    }
  },
});

export const updateCenter = new ValidatedMethod({
  name: 'center.updateCenter',
  validate: new SimpleSchema({
    centerId: {
      type: String,
      regEx: RegEx.Id,
      label: "Identifiant du Centre d'accueil",
    },
    data: Centre.schema,
  }).validator({ clean: true }),

  run({ data, centerId }) {
    if (!Roles.userIsInRole(this.userId, ['admin', 'saisie'])) {
      throw new Meteor.Error(
        'api.center.updateCenter.notPermitted',
        'Vos droits sont insuffisants pour réaliser cette action',
      );
    }
    // check center existence
    const centre = Centre.findOne(centerId);
    if (centre === undefined) {
      throw new Meteor.Error('api.center.updateCenter.unknownCenter', "Identifiant de centre d'accueil inconnu");
    }
    try {
      return Centre.update({ _id: centerId }, { $set: { ...data } });
    } catch (error) {
      if (error.code === 11000) {
        throw new Meteor.Error('api.center.updateCenter.duplicateEntry', "Numéro de centre d'examen déjà existant");
      } else {
        throw error;
      }
    }
  },
});

export const removeCenter = new ValidatedMethod({
  name: 'center.removeCenter',
  validate: new SimpleSchema({
    centerId: {
      type: String,
      regEx: RegEx.Id,
      label: "Identifiant du Centre d'accueil",
    },
  }).validator(),

  run({ centerId }) {
    if (!Roles.userIsInRole(this.userId, ['admin', 'saisie'])) {
      throw new Meteor.Error(
        'api.center.removeCenter.notPermitted',
        'Vos droits sont insuffisants pour réaliser cette action',
      );
    }
    // check center existence
    const center = Centre.findOne(centerId);
    if (center === undefined) {
      throw new Meteor.Error('api.center.removeCenter.unknownCenter', "Identifiant de centre d'accueil inconnu");
    }
    // check if current user has admin rights
    const authorized = !!this.userId;
    if (!authorized) {
      throw new Meteor.Error('api.center.removeCenter.notPermitted', 'Connexion requise');
    }
    if (Candidat.find({ centre: `${[centerId]}` }).count() === 0) {
      Centre.remove(centerId);
    } else {
      // Display error message on client
      throw new Meteor.Error(
        'api.center.removeCenter.relatedDataStillExist',
        'Des candidats utilisent encore ce centre',
      );
    }
  },
});

// Get list of all method names on User
const LISTS_METHODS = _.pluck([createCenter, updateCenter, removeCenter], 'name');

if (Meteor.isServer) {
  // Only allow 5 list operations per connection per second
  DDPRateLimiter.addRule(
    {
      name(name) {
        return _.contains(LISTS_METHODS, name);
      },

      // Rate limit per connection ID
      connectionId() {
        return true;
      },
    },
    5,
    1000,
  );
}
