import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { Snackbar } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import Paper from '@material-ui/core/Paper';
import { CSVReader } from 'react-papaparse';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
  titleBackgroud: {
    background: '#99ddff',
    borderRadius: '3px 3px 0 0',
    display: 'flex',
    justifyContent: 'space-between',
  },
  title: {
    margin: theme.spacing(3),
    fontSize: '120%',
    alignContent: 'flex-start',
  },
  titleRight: {
    alignContent: 'flex-end',
  },
  button: { width: 200, margin: theme.spacing(1) },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    margin: 'auto',
    marginTop: 20,
    marginBottom: 20,
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1) + 20,
    width: 232,
  },
  pre: {
    backgroundColor: 'black',
    color: 'whitesmoke',
    display: 'block',
    fontFamily: 'monospace',
    whiteSpace: 'pre',
    margin: '1em 2em',
    padding: '1em',
  },
}));

export default function AdminImport() {
  const history = useHistory();
  const classes = useStyles();
  const [csvfile, setCsvfile] = useState();
  const [importLog, setImportLog] = useState([]);

  const [openSnack, setOpenSnack] = React.useState(false);
  const [snackData, setSnackData] = React.useState({
    text: '',
    severity: 'success',
  });

  const handleCloseSnack = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnack(false);
  };

  const showAlert = (text, severity = 'success') => {
    setSnackData({ text, severity });
    setOpenSnack(true);
  };

  const handleDropFiles = (data) => {
    setCsvfile(data);
  };

  const handleOnError = (err) => {
    showAlert(err.reason, 'error');
  };

  const handleOnRemoveFile = () => {
    setCsvfile();
  };

  const handleImport = () => {
    if (csvfile !== undefined) {
      setImportLog([]);
      Meteor.call(
        'list.upload',
        {
          fileContent: csvfile,
        },
        (err, res) => {
          if (err) {
            showAlert(err.reason, 'error');
          } else if (res.allOK) {
            showAlert('Tout a été importé sans erreur');
          } else {
            showAlert('Il y a eu des erreurs !', 'warning');
            setImportLog(res.importLog);
          }
        },
      );
    } else {
      showAlert('Rien à importer !', 'warning');
    }
  };

  function showLog() {
    if (importLog.length > 0)
      return (
        <pre className={classes.pre}>
          <h3>Log</h3>
          {importLog.map((log) => {
            return <code key={log.slice(0, 10)}>{log}</code>;
          }) || ''}
        </pre>
      );
    return null;
  }

  return (
    <Paper className={classes.root}>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={openSnack}
        autoHideDuration={5000}
        onClose={handleCloseSnack}
      >
        <Alert onClose={handleCloseSnack} severity={snackData.severity}>
          {snackData.text}
        </Alert>
      </Snackbar>
      <div className={classes.titleBackgroud}>
        <p className={classes.title}>Importer des données</p>
        <div className={classes.titleRight} />
        <Button onClick={() => history.push('/home')} className={classes.button} variant="outlined" color="secondary">
          Retour
        </Button>
      </div>
      <p className={classes.container}>Permet d&apos;importer un fichier CSV Cyclades.</p>
      <form className={classes.container} noValidate>
        <CSVReader
          onDrop={handleDropFiles}
          onError={handleOnError}
          addRemoveButton
          onRemoveFile={handleOnRemoveFile}
          config={{
            delimiter: ';',
            quoteChar: '',
            header: true,
            dynamicTyping: false,
            encoding: '',
            worker: true,
            step: undefined,
            complete: undefined,
            error: undefined,
            skipEmptyLines: true,
            fastMode: undefined,
          }}
        >
          <span>Cliquer ou glisser un fichier CSV ici</span>
        </CSVReader>
        <Button className={classes.button} onClick={handleImport} color="primary">
          Importer
        </Button>
      </form>
      {showLog()}
    </Paper>
  );
}
