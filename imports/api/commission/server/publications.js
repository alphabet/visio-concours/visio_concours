import { Meteor } from 'meteor/meteor';
import Commission from '../commission';

// publish all exams
Meteor.publish('commission.all', function commissionAll() {
  if (!this.userId) {
    return this.ready();
  }
  return Commission.find({}, { sort: { name: 1 }, limit: 1000 });
});

Meteor.publish('commission.one', function commissionOne({ commissionId }) {
  if (!this.userId) {
    return this.ready();
  }
  return Commission.find({ _id: commissionId }, { fields: Commission.publicFields, sort: { name: 1 }, limit: 1 });
});
