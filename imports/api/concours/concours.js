import SimpleSchema from 'simpl-schema';
import { Random } from 'meteor/random';
import { Factory } from 'meteor/dburles:factory';

const Concours = new Mongo.Collection('concours');

// Deny all client-side updates since we will be using methods to manage this collection
Concours.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

Concours.schema = new SimpleSchema({
  name: {
    type: String,
    min: 1,
    index: true,
    unique: true,
    label: 'Intitulé du concours',
  },
  numero: {
    type: String,
    min: 1,
    index: true,
    unique: true,
    label: 'Numéro du concours',
  },
  start: {
    type: Date,
    label: 'Date de début du concours',
    defaultValue: new Date('2020-01-01'),
  },
  end: {
    type: Date,
    label: 'Date de fin du concours',
    defaultValue: new Date('2021-01-01'),
  },
});

Concours.attachSchema(Concours.schema);

Concours.publicFields = {
  _id: 1,
  name: 1,
  numero: 1,
  start: 1,
  end: 1,
};

Factory.define('concours', Concours, {
  name: () => Random.id(),
  numero: () => Random.id(),
});

export default Concours;
