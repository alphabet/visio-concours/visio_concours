/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */

import { PublicationCollector } from 'meteor/johanbrook:publication-collector';
import { chai, assert } from 'meteor/practicalmeteor:chai';
import { Meteor } from 'meteor/meteor';
import { _ } from 'meteor/underscore';
import faker from 'faker';
import { Random } from 'meteor/random';
import { Factory } from 'meteor/dburles:factory';
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';
import './publications';

import Commission from '../commission';
import { createCommission, updateCommission, removeCommission } from '../methods';
import Concours from '../../concours/concours';

describe('commission', function () {
  describe('mutators', function () {
    it('builds correctly from factory', function () {
      const newCommission = Factory.create('commission');
      assert.typeOf(newCommission, 'object');
    });
  });
  describe('publications', function () {
    let userId;
    before(function () {
      Meteor.users.remove({});
      userId = Accounts.createUser({
        username: faker.internet.email(),
        password: 'toto',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Meteor.users.update(userId, { $set: { isActive: true } });
      Commission.remove({});
      _.times(4, () => {
        Factory.create('commission');
      });
    });
    describe('commission.all', function () {
      it('sends all commissions', function (done) {
        const collector = new PublicationCollector({ userId });
        collector.collect('commission.all', (collections) => {
          chai.assert.equal(collections.commission.length, 4);
          done();
        });
      });
    });
  });
  describe('methods', function () {
    let userId;
    let saisieUserId;
    let adminUserId;
    beforeEach(function () {
      Meteor.users.remove({});
      Meteor.roles.remove({});
      Meteor.roleAssignment.remove({});
      Concours.remove({});
      Commission.remove({});
      const email = faker.internet.email();
      const email2 = faker.internet.email();
      adminUserId = Accounts.createUser({
        email: 'eole@ac-test.fr',
        username: 'eole',
        password: 'eole',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Roles.createRole('admin');
      Roles.addUsersToRoles(adminUserId, 'admin');
      Meteor.users.update(adminUserId, { $set: { isActive: true } });

      saisieUserId = Accounts.createUser({
        email2,
        username: email2,
        password: 'toto',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Roles.createRole('saisie');
      Roles.addUsersToRoles(saisieUserId, 'saisie');
      Meteor.users.update(saisieUserId, { $set: { isActive: true } });

      userId = Accounts.createUser({
        email,
        username: email,
        password: 'toto',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Meteor.users.update(userId, { $set: { isActive: true } });
    });
    describe('createCommission', function () {
      it('does user with admin role create a commission', function () {
        createCommission._execute(
          { userId: adminUserId },
          { concours: Random.id(), name: 'maComm', numCommission: '007' },
        );
        const reqComm = Commission.findOne({ name: 'maComm' });
        assert.typeOf(reqComm, 'object');
      });
      it('does user with saisie role create a commission', function () {
        createCommission._execute(
          { userId: saisieUserId },
          { concours: Random.id(), name: 'maComm2', numCommission: '008' },
        );
        const reqComm = Commission.findOne({ name: 'maComm2' });
        assert.typeOf(reqComm, 'object');
      });
      it('does not create a commission if user has no role', function () {
        assert.throws(
          () => {
            createCommission._execute({ userId }, { concours: Random.id(), name: 'maComm3', numCommission: '009' });
          },
          Meteor.Error,
          /api.commission.createCommission.notPermitted/,
        );
      });
      it('does not create a commission if no user is connected', function () {
        assert.throws(
          () => {
            createCommission._execute({}, { concours: Random.id(), name: 'maComm4', numCommission: '010' });
          },
          Meteor.Error,
          /api.commission.createCommission.notPermitted/,
        );
      });
      it('does not create a duplicate commission', function () {
        const concours = Factory.create('concours', { name: 'monConc' })._id;
        createCommission._execute({ userId: adminUserId }, { concours, name: 'maComm', numCommission: '007' });
        assert.throws(
          () => {
            createCommission._execute({ userId: adminUserId }, { concours, name: 'maComm', numCommission: '007' });
          },
          Meteor.Error,
          /api.commission.createCommission.duplicateEntry/,
        );
      });
    });
    describe('updateCommission', function () {
      it('does user with admin role update a commission', function () {
        const commissionId = Factory.create('commission', {
          concours: Random.id(),
          name: 'maComm',
          numCommission: '007',
        })._id;
        updateCommission._execute(
          { userId: adminUserId },
          { commissionId, data: { concours: Random.id(), name: 'updatedComm', numCommission: '007' } },
        );
        const reqComm = Commission.findOne({ _id: commissionId });
        assert.typeOf(reqComm, 'object');
        assert.equal(reqComm.name, 'updatedComm');
      });
      it('does user with saisie role update a commission', function () {
        const commissionId = Factory.create('commission', {
          concours: Random.id(),
          name: 'maComm2',
          numCommission: '008',
        })._id;
        updateCommission._execute(
          { userId: saisieUserId },
          { commissionId, data: { concours: Random.id(), name: 'updatedComm2', numCommission: '008' } },
        );
        const reqComm = Commission.findOne({ _id: commissionId });
        assert.typeOf(reqComm, 'object');
        assert.equal(reqComm.name, 'updatedComm2');
      });
      it('does not update a commission if user has no role', function () {
        const commissionId = Factory.create('commission', {
          concours: Random.id(),
          name: 'maComm3',
          numCommission: '009',
        })._id;
        assert.throws(
          () => {
            updateCommission._execute(
              { userId },
              { commissionId, data: { concours: Random.id(), name: 'updatedComm3', numCommission: '009' } },
            );
          },
          Meteor.Error,
          /api.commission.updateCommission.notPermitted/,
        );
      });
      it('does not update a commission if no user is connected', function () {
        const commissionId = Factory.create('commission', {
          concours: Random.id(),
          name: 'maComm4',
          numCommission: '010',
        })._id;
        assert.throws(
          () => {
            updateCommission._execute(
              {},
              { commissionId, data: { concours: Random.id(), name: 'updatedComm4', numCommission: '010' } },
            );
          },
          Meteor.Error,
          /api.commission.updateCommission.notPermitted/,
        );
      });
      it('does not update a not found commission', function () {
        assert.throws(
          () => {
            updateCommission._execute(
              { userId: adminUserId },
              { commissionId: Random.id(), data: { concours: Random.id(), name: 'noComm', numCommission: '007' } },
            );
          },
          Meteor.Error,
          /api.commission.updateCommission.unknownCommission/,
        );
      });
      it('does not update to an existing commission', function () {
        const concours = Random.id();
        Factory.create('commission', { concours, name: 'existingComm', numCommission: '007' });
        const commissionId = Factory.create('commission', {
          concours: Random.id(),
          name: 'maComm',
          numCommission: '000',
        })._id;
        assert.throws(
          () => {
            updateCommission._execute(
              { userId: adminUserId },
              { commissionId, data: { concours, name: 'existingComm', numCommission: '007' } },
            );
          },
          Meteor.Error,
          /api.commission.updateCommission.duplicateEntry/,
        );
      });
    });
    describe('removeCommission', function () {
      it('does user with admin role remove a commission', function () {
        const commissionId = Factory.create('commission', { name: 'maComm' })._id;
        assert.typeOf(Commission.findOne({ _id: commissionId }), 'object');
        removeCommission._execute({ userId: adminUserId }, { commissionId });
        assert.equal(Commission.findOne({ _id: commissionId }), undefined);
      });
      it('does user with saisie role remove a commission', function () {
        const commissionId = Factory.create('commission', { name: 'maComm' })._id;
        assert.typeOf(Commission.findOne({ _id: commissionId }), 'object');
        removeCommission._execute({ userId: saisieUserId }, { commissionId });
        assert.equal(Commission.findOne({ _id: commissionId }), undefined);
      });
      it('does not remove a commission if user has no role', function () {
        const commissionId = Factory.create('commission', { name: 'maComm' })._id;
        assert.typeOf(Commission.findOne({ _id: commissionId }), 'object');
        assert.throws(
          () => {
            removeCommission._execute({ userId }, { commissionId });
          },
          Meteor.Error,
          /api.commission.removeCommission.notPermitted/,
        );
      });
      it('does not remove a commission if no user is connected', function () {
        const commissionId = Factory.create('commission', { name: 'maComm' })._id;
        assert.typeOf(Commission.findOne({ _id: commissionId }), 'object');
        assert.throws(
          () => {
            removeCommission._execute({}, { commissionId });
          },
          Meteor.Error,
          /api.commission.removeCommission.notPermitted/,
        );
      });
      it('does not remove a not found commission', function () {
        const commissionId = Factory.create('commission', { name: 'maComm' })._id;
        assert.typeOf(Commission.findOne({ _id: commissionId }), 'object');
        assert.throws(
          () => {
            removeCommission._execute({ userId: adminUserId }, { commissionId: Random.id() });
          },
          Meteor.Error,
          /api.commission.removeCommission.unknownCommission/,
        );
      });
      it('does not remove a commission which still has candidat', function () {
        const commissionId = Factory.create('commission', { name: 'maComm' })._id;
        assert.typeOf(Commission.findOne({ _id: commissionId }), 'object');
        Factory.create('list', { commission: commissionId });
        assert.throws(
          () => {
            removeCommission._execute({ userId: adminUserId }, { commissionId });
          },
          Meteor.Error,
          /api.commission.removeCommission.relatedDataStillExist/,
        );
      });
    });
  });
});
