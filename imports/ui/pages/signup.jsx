import React, { useState, useEffect } from 'react';
import { Meteor } from 'meteor/meteor';
import { useHistory } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Snackbar from '@material-ui/core/Snackbar';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import validate from 'validate.js';
import { Typography } from '@material-ui/core';

validate.options = {
  fullMessages: false,
};

const styles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%', // Fix IE 11 issue.
  },
  titleBackgroud: {
    background: '#99ddff',
    borderRadius: '3px 3px 0 0',
    marginBottom: theme.spacing(2),
  },
  title: {
    padding: theme.spacing(3),
  },
  form: {
    backgroundColor: 'white',
    textAlign: 'center',
    marginTop: theme.spacing(10),
    margin: 'auto',
    width: 300,
  },
  button: {
    margin: theme.spacing(3),
    width: 250,
  },
  submit: {
    margin: theme.spacing(3),
    width: 200,
  },
}));

const schema = {
  email: {
    presence: { allowEmpty: false, message: 'Adresse électronique requise' },
    email: {
      message: 'Adresse électronique doit être une adresse valide',
    },
    length: {
      maximum: 64,
    },
  },
  password: {
    presence: { allowEmpty: false, message: 'Mot de passe requis' },
    length: {
      maximum: 64,
    },
  },
};

function SignUp() {
  const classes = styles();
  const history = useHistory();

  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    errors: {},
  });

  useEffect(() => {
    const errors = validate(formState.values, schema);
    setFormState(() => ({
      ...formState,
      isValid: !errors,
      errors: errors || {},
    }));
  }, [formState.values]);

  const [errMsg, setErrMsg] = useState('Erreur de création du compte');
  const [openError, setOpenError] = useState(false);

  const handleChange = (event) => {
    event.persist();

    setFormState(() => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]: event.target.value,
      },
    }));
  };

  const handleSignIn = (event) => {
    event.preventDefault();
    if (formState.isValid === true) {
      const { email, password } = formState.values;
      Meteor.call('user.addUser', { email, password }, (err) => {
        if (err) {
          let message = err.reason === 'Username already exists.' ? 'Cet utilisateur existe déjà' : err.reason;
          if (err.error === 'validation-error') {
            // display first validation message
            message = err.details.map((info) => info.message).join('\n');
          }
          setErrMsg(message);
          setOpenError(true);
        } else {
          alert('Le compte a bien été créé et doit être activé par un administrateur.');
          history.push('/');
        }
      });
    }
  };

  const handleErrorClose = (reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenError(false);
  };

  const hasError = (field) => !!formState.errors[field];
  return (
    <Grid container component="main" className={classes.container}>
      <Grid item xs={4} sm={8} md={12} elevation={6}>
        <form onSubmit={handleSignIn} className={classes.form}>
          <div className={classes.titleBackgroud}>
            <Typography className={classes.title} variant="h5">
              S&apos;enregistrer
            </Typography>
          </div>
          <TextField
            className={classes.button}
            autoFocus
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="email"
            label="Adresse électronique"
            type="text"
            id="email"
            autoComplete="current-user"
            error={hasError('email')}
            helperText={hasError('email') ? formState.errors.email[0] : null}
            onChange={handleChange}
            value={formState.values.email || ''}
          />
          <TextField
            className={classes.button}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Mot de passe"
            type="password"
            id="password"
            autoComplete="current-password"
            error={hasError('password')}
            helperText={hasError('password') ? formState.errors.password[0] : null}
            onChange={handleChange}
            value={formState.values.password || ''}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={!formState.isValid}
          >
            Créer un compte
          </Button>
        </form>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={openError}
          autoHideDuration={4000}
          onClose={handleErrorClose}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={errMsg}
        />
      </Grid>
    </Grid>
  );
}

export default SignUp;
