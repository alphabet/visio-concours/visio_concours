#
# meteor_contest_visio description:
  visio_conf with React, Meteor and Jitsi

# Only for development:

## Install on eolebase 2.7.1

  ```
  ouvre.firewall
  apt install -y curl python3-pip git
  curl https://install.meteor.com/ | sh
  git clone git@gitlab.mim.ovh:edouard.le-bourgeois/meteor_contest_visio.git && cd meteor_contest_visio
  meteor npm install
  METEOR_ALLOW_SUPERUSER=1 meteor --settings settings-development.json
  ```

  users:

  ```
  name: eole
  pwd: eole21;
  ```

## Installation et éxécution de mongosh

  ```
  npm install mongosh
  npx mongosh mongodb://localhost:3003/meteor
  ```
  Le port le mongodb correspond au port de l'application + 1
  Si l'application est lancé sur le port 3002, mongodb sera lancé sur le port 3003
  
## Build and run project on eolebase 2.7.1

  You need to install the node version that is bundled into your meteor Installation.

  ```
  export NODE_VERSION=$(meteor node -v)
  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
  nvm install $NODE_VERSION
  nvm use $NODE_VERSION
  ```

  extract archive located in /path/to/build, install node_modules then launch application :

  ```
  cd {BUILD_EXTRACT_DIR}
  cd programs/server && npm install
  cd ../..
  BIND_IP=0.0.0.0 PORT=8080 MONGO_URL=mongodb://localhost:27017/visio ROOT_URL=http://eolebase.ac-test.fr:8080/ METEOR_SETTINGS='{"public": {"domain": "meet.jit.si" }}' node main.js
  ```

## Run with meteor

  ```
  METEOR_ALLOW_SUPERUSER=1 meteor --settings settings-development.json
  ```

## Architecture

  `settings-development.json` is the config file for development only.
  `settings-production.json` has been haded in the .gitignore file. For more information click this [link](https://blog.meteor.com/the-meteor-chef-making-use-of-settings-json-3ed5be2d0bad).

## useful links

  - [custom css for user-accounts](https://guide.meteor.com/accounts.html#useraccounts)
  - [Jitsi Meet API](https://github.com/jitsi/jitsi-meet/blob/master/doc/api.md)
  - [Installation de Jitsi Meet](https://framacloud.org/fr/cultiver-son-jardin/jitsi-meet.html)
  - [a slider for date range](https://www.npmjs.com/package/rc-slider)
  - [a tooltip for date range](https://www.npmjs.com/package/rc-tooltip)
  - [mongo shell](https://docs.mongodb.com/manual/reference/mongo-shell/)
  - [moment.js](https://momentjs.com/)
  - [meteor deployement](https://guide.meteor.com/deployment.html)
  - [meteor build and manual deployment](https://www.digitalocean.com/community/tutorials/how-to-deploy-a-meteor-js-application-on-ubuntu-14-04-with-nginx)
