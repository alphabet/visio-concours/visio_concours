import SimpleSchema from 'simpl-schema';
import { Random } from 'meteor/random';
import { Factory } from 'meteor/dburles:factory';
import RegEx from '../regExp';

const List = new Mongo.Collection('list');

// Deny all client-side updates since we will be using methods to manage this collection
List.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

List.schema = new SimpleSchema({
  centre: {
    type: String,
    min: 1,
    regEx: RegEx.Id,
    label: "Identifiant du centre d'accueil",
  },
  numInscription: {
    type: String,
    index: true,
    unique: true,
    min: 1,
    label: "Numéro d'inscription du candidat",
  },
  candidat: {
    type: String,
    min: 1,
    label: 'Nom du candidat',
  },
  date: {
    type: Date,
    label: 'Date de convocation',
    defaultValue: new Date(),
  },
  heure: {
    type: String,
    min: 1,
    label: 'Heure de passage',
  },
  commission: {
    type: String,
    regEx: RegEx.Id,
    min: 1,
    label: 'Identifiant de la commission',
  },
  color: {
    type: Boolean,
    defaultValue: false,
    label: 'Visioconférence démarrée',
  },
  domain: {
    type: String,
    defaultValue: '',
    label: 'Adresse du serveur Jitsi',
  },
});

List.attachSchema(List.schema);

List.publicFields = {
  _id: 1,
  centre: 1,
  numInscription: 1,
  candidat: 1,
  date: 1,
  heure: 1,
  commission: 1,
  color: 1,
  domain: 1,
};

Factory.define('list', List, {
  centre: () => Random.id(),
  numInscription: () => Random.id(),
  candidat: () => `Bob_${Random.id()}`,
  date: () => new Date(),
  heure: '09:00',
  commission: () => Random.id(),
  color: false,
  domain: '',
});

export default List;
