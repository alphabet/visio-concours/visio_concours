import React, { useEffect, useState, forwardRef } from 'react';

import { withTracker } from 'meteor/react-meteor-data';
import { moment } from 'meteor/momentjs:moment';
import PropTypes from 'prop-types';
import MuiAlert from '@material-ui/lab/Alert';
import { Paper, Button, Snackbar } from '@material-ui/core';
import MaterialTable from 'material-table';
import { useHistory } from 'react-router-dom';
import ControlPointIcon from '@material-ui/icons/ControlPoint';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import { makeStyles } from '@material-ui/core/styles';
import Concours from '../../api/concours/concours';
import Commission from '../../api/commission/commission';
import Centre from '../../api/centre/centre';
import Academie from '../../api/academie/academie';
import List from '../../api/list/list';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
  titleBackgroud: {
    background: '#99ddff',
    borderRadius: '3px 3px 0 0',
    display: 'flex',
    justifyContent: 'space-between',
  },
  title: {
    margin: theme.spacing(3),
    fontSize: '120%',
    alignContent: 'flex-start',
  },
  titleRight: {
    alignContent: 'flex-end',
  },
  button: { width: 200, margin: theme.spacing(1) },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const AdminCandidats = ({ candidats, loading }) => {
  const history = useHistory();
  const classes = useStyles();
  const [data, setData] = useState([]);
  const [openSnack, setOpenSnack] = useState(false);
  const [snackData, setSnackData] = useState({
    text: '',
    severity: 'success',
  });

  const showAlert = (text, severity = 'success') => {
    setSnackData({ text, severity });
    setOpenSnack(true);
  };

  const handleCloseSnack = (reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnack(false);
  };

  useEffect(() => {
    if (loading === false) {
      const allData = candidats.map((row) => {
        let academie = '';
        let centre = '';
        let concours = '';
        let commission = '';
        const cexam = Centre.findOne({ _id: row.centre });
        if (cexam) {
          const aca = Academie.findOne({ _id: cexam.academie });
          if (aca) {
            academie = aca.name;
          }
          centre = cexam.name;
        }
        const comm = Commission.findOne({ _id: row.commission });
        if (comm) {
          const conc = Concours.findOne({ _id: comm.concours });
          if (conc) {
            concours = `${conc.numero}-${conc.name}`;
          }
          commission = `${comm.numCommission}-${comm.name}`;
        }
        return {
          ...row,
          academie,
          centre,
          concours,
          commission,
        };
      });
      setData(allData);
    }
  }, [loading, candidats]);

  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  const localization = {
    toolbar: {
      searchPlaceholder: 'Rechercher un candidat',
      searchTooltip: 'Rechercher un candidat',
    },
    pagination: {
      labelRowsSelect: 'Candidats par page',
      firstAriaLabel: 'Première page',
      firstTooltip: 'Première page',
      previousAriaLabel: 'Page précédente',
      previousTooltip: 'Page précédente',
      nextAriaLabel: 'Page suivante',
      nextTooltip: 'Page suivante',
      lastAriaLabel: 'Dernière page',
      lastTooltip: 'Dernière page',
    },
    body: {
      emptyDataSourceMessage: 'Aucun candidat trouvé',
      addTooltip: 'Ajouter un candidat',
      editTooltip: 'Modifier ce candidat',
      deleteTooltip: 'Supprimer ce candidat',
      editRow: {
        deleteText: 'Supprimer vraiment ce candidat ?',
        cancelTooltip: 'Annuler',
        saveTooltip: 'Enregistrer les changements',
      },
    },
  };

  const options = {
    pageSize: 10,
    pageSizeOptions: [10, 20, 50, 100],
    paginationType: 'stepped',
    actionsColumnIndex: 8,
    addRowPosition: 'first',
    emptyRowsWhenPaging: false,
  };

  const actions = [
    {
      icon: ControlPointIcon,
      isFreeAction: true,
      tooltip: 'Ajouter un candidat',
      onClick: () => history.push('/admincandidats/new'),
    },
    {
      icon: Edit,
      tooltip: 'Modifier ce candidat',
      onClick: (event, rowData) => {
        history.push(`/admincandidats/${rowData._id}`);
      },
    },
  ];

  return (
    <Paper className={classes.root}>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={openSnack}
        autoHideDuration={5000}
        onClose={handleCloseSnack}
      >
        <Alert onClose={handleCloseSnack} severity={snackData.severity}>
          {snackData.text}
        </Alert>
      </Snackbar>
      <div className={classes.titleBackgroud}>
        <p className={classes.title}>Liste de tous les candidats</p>
        <div className={classes.titleRight} />
        <Button onClick={() => history.push('/home')} className={classes.button} variant="outlined" color="secondary">
          Retour
        </Button>
      </div>

      <MaterialTable
        columns={[
          {
            title: 'N° Inscription',
            field: 'numInscription',
            defaultSort: 'asc',
          },
          { title: 'Nom', field: 'candidat' },
          { title: 'Académie', field: 'academie' },
          { title: "Centre d'accueil", field: 'centre' },
          { title: 'Concours (numéro + intitulé)', field: 'concours' },
          { title: 'Commission (numéro + intitulé)', field: 'commission' },
          {
            title: 'Date',
            field: 'date',
            type: 'date',
            render: (rowData) => moment(rowData.date).utc().format('DD-MM-YYYY'),
          },
          { title: 'Heure', field: 'heure' },
        ]}
        data={data}
        title=""
        options={options}
        localization={localization}
        icons={tableIcons}
        actions={actions}
        editable={{
          onRowDelete: (oldData) =>
            new Promise((resolve, reject) => {
              Meteor.call(
                'list.removeCandidat',
                {
                  candidatId: oldData._id,
                },
                (err, res) => {
                  if (err) {
                    showAlert(err.reason, 'error');
                    reject(err);
                  } else {
                    resolve(res);
                  }
                },
              );
            }),
        }}
      />
    </Paper>
  );
};

AdminCandidats.propTypes = {
  candidats: PropTypes.arrayOf(PropTypes.element).isRequired,
  loading: PropTypes.bool.isRequired,
};

export default withTracker(() => {
  const subAca = Meteor.subscribe('academie.all');
  const subCentre = Meteor.subscribe('centre.all');
  const subConcours = Meteor.subscribe('concours.all');
  const subComs = Meteor.subscribe('commission.all');
  const subList = Meteor.subscribe('list.all');
  const candidats = List.find().fetch() || [];
  return {
    candidats,
    loading: !subList.ready() || !subAca.ready() || !subCentre.ready() || !subComs.ready() || !subConcours.ready(),
  };
})(AdminCandidats);
