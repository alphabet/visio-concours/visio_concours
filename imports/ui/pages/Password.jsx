import React, { useState } from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import {
  InputAdornment,
  Button,
  IconButton,
  Snackbar,
  Grid,
  OutlinedInput,
  FormControl,
  InputLabel,
  FormHelperText,
  Typography,
} from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

const styles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  form: {
    padding: theme.spacing(3),
    position: 'relative',
    textAlign: 'center',
    backgroundColor: 'white',
    marginTop: theme.spacing(3),
    width: 400,
  },
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%', // Fix IE 11 issue.
  },
  submit: {
    margin: theme.spacing(2),
    width: 150,
  },
}));

function Password({ user }) {
  const classes = styles();

  const [redirect, setRedirect] = useState(false);
  const [formState, setFormState] = useState(false);
  const [password, setPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirm, setConfirm] = useState('');
  const [openError, setOpenError] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [match, setMatch] = useState(true);
  const [errMsg, setErrMsg] = useState('');

  const handlePassword = (event) => {
    setPassword(event.target.value);
    if (event.target.value) {
      if (newPassword !== '' && newPassword === confirm) setFormState(true);
    } else {
      setFormState(false);
    }
  };

  const handleNewPassword = (event) => {
    setNewPassword(event.target.value);
    if (confirm !== event.target.value) {
      setMatch(false);
      setFormState(false);
    } else {
      setMatch(true);
      if (newPassword !== '' && password !== '') {
        setFormState(true);
      }
    }
  };

  const handleConfirm = (event) => {
    setConfirm(event.target.value);
    if (newPassword !== event.target.value) {
      setMatch(false);
      setFormState(false);
    } else {
      setMatch(true);
      if (newPassword !== '' && password !== '') {
        setFormState(true);
      }
    }
  };

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (formState === true) {
      Accounts.changePassword(password, newPassword, (err) => {
        if (err) {
          setErrMsg(err.reason === 'Incorrect password' ? 'Mot de passe incorrect' : err.reason);
          setOpenError(true);
        } else {
          setRedirect(true);
        }
      });
    }
  };

  const handleErrorClose = (reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenError(false);
  };

  const renderRedirect = () => {
    if (redirect) {
      return <Redirect to="/home" />;
    }
    return null;
  };

  return (
    <div className={classes.root}>
      <div className={classes.form}>
        {renderRedirect()}
        <Grid container component="main" className={classes.container} spacing={2}>
          <Grid item xs={12}>
            <Typography variant="h6">Changement de mot de passe ({user.username})</Typography>
          </Grid>
          <Grid item xs={12} elevation={6}>
            <form onSubmit={handleSubmit}>
              <FormControl variant="outlined" margin="normal" fullWidth required>
                <InputLabel htmlFor="password">Mot de Passe actuel</InputLabel>
                <OutlinedInput
                  autoFocus
                  id="password"
                  name="password"
                  type={showPassword ? 'text' : 'password'}
                  value={password}
                  labelWidth={160}
                  onChange={handlePassword}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        title={showPassword ? 'Masquer le mot de passe' : 'Afficher le mot de passe'}
                        aria-label={showPassword ? 'Masquer le mot de passe' : 'Afficher le mot de passe'}
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  }
                />
              </FormControl>
              <FormControl variant="outlined" margin="normal" fullWidth required>
                <InputLabel htmlFor="newPassword">Nouveau Mot de Passe</InputLabel>
                <OutlinedInput
                  id="newPassword"
                  name="newPassword"
                  type={showPassword ? 'text' : 'password'}
                  autoComplete="new-password"
                  value={newPassword}
                  labelWidth={180}
                  onChange={handleNewPassword}
                />
              </FormControl>
              <FormControl variant="outlined" margin="normal" fullWidth required>
                <InputLabel htmlFor="confirm" className={match ? '' : 'Mui-error'}>
                  Confirmation
                </InputLabel>
                <OutlinedInput
                  id="confirm"
                  name="confirm"
                  type={showPassword ? 'text' : 'password'}
                  value={confirm}
                  error={!match}
                  labelWidth={100}
                  onChange={handleConfirm}
                />
                <FormHelperText className={match ? '' : 'Mui-error'}>
                  {match ? null : 'Les mots de passe ne correspondent pas'}
                </FormHelperText>
              </FormControl>
              <Button
                type="submit"
                fullWidth
                color="primary"
                variant="outlined"
                className={classes.submit}
                disabled={!formState}
              >
                Valider
              </Button>
              <Button className={classes.submit} onClick={() => setRedirect(true)} color="secondary" variant="outlined">
                Retour
              </Button>
            </form>
            <Snackbar
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              open={openError}
              autoHideDuration={4000}
              onClose={handleErrorClose}
              ContentProps={{
                'aria-describedby': 'message-id',
              }}
              message={`Erreur: ${errMsg}`}
            />
          </Grid>
        </Grid>
      </div>
    </div>
  );
}

export default withTracker(() => {
  return {
    user: Meteor.user(),
  };
})(Password);

Password.defaultProps = {
  user: {},
};

Password.propTypes = {
  user: PropTypes.objectOf(PropTypes.element),
};
