from sys import exit
from os.path import isfile
from datetime import datetime
from faker import Faker
from random import Random, choice
import csv
from utils import *


fake = Faker(['fr-FR'])
datas = []
dataCount = 0

academies_centres = {
    "Dijon": [("Côte-d'Or", "21000"), ("Nièvre", "21001"), ("Saône-et-Loire", "21002"), ("Yonne", "21003")],
    "Bordeaux": [("Dordogne", "33000"), ("Gironde", "33001"), ("Landes", "33002"), ("Lot-et-Garonne", "33003"), ("Pyrénées-Atlantiques", "33004")],
    "Normandie": [("Calvados", "16000"), ("Manche", "16001"), ("Orne", "16002"), ("Eure", "16003"), ("Seine-Maritime", "16004"), ("Saint-Pierre-et-Miquelon", "16005")],
    "Poitiers": [("Charente", "44000"), ("Charente-Maritime", "44001"), ("Deux-Sèvres", "44002"), ("Vienne", "44003")],
    "Versailles": [("Yvelines", "14000"), ("Essonne", "14001"), ("Hauts-de-Seine", "14002"), ("Val-d'Oise", "14003")]
}

academies_centres_et_codes_postaux = [[list(academies_centres.keys())[x], choice(list(academies_centres.values())[x])] for x in range(5) for n in range(5)]
concours_s_commissions_et_cles_commissions = [[fake.job(), "jury " + str(x), int('%d%d' % (Random().randint(1000, 9999), x))] for x in range(5)]
epreuves = [fake.job() for x in range(5)]
options = [fake.job() for x in range(5)]
heures = [x for x in range(8, 18)]
minutes = [str(x).zfill(2) for x in range(0, 60, 10)]

def generateData():
    global dataCount
    dataCount += 1

    username = fake.name().lower().replace(" ", "_").replace(".", "") + str(dataCount)
    mail = username + "@" + fake.free_email_domain()
    prenom = fake.first_name()
    nom = fake.last_name()

    epreuve = choice(epreuves)
    option = choice(options)
    heure = choice(heures)
    minute = choice(minutes)

    academie_centre_et_code_postale = choice(academies_centres_et_codes_postaux)
    academie = academie_centre_et_code_postale[0]
    centre = academie_centre_et_code_postale[1][0]
    code_postale = academie_centre_et_code_postale[1][1]

    concours_commission_et_cle_commission = choice(concours_s_commissions_et_cles_commissions)
    concours = concours_commission_et_cle_commission[0]
    commission = concours_commission_et_cle_commission[1]
    cle_commission = concours_commission_et_cle_commission[2]

    date_concours_debut_not_formated = fake.date_this_month()
    date_concours_debut = date_concours_debut_not_formated.strftime('%d/%m/%Y')
    date_concours_fin_not_formated = fake.date_between(date_concours_debut_not_formated, '+30d')
    date_concours_fin = date_concours_fin_not_formated.strftime('%d/%m/%Y')
    date_visio = fake.date_between(date_concours_debut_not_formated, date_concours_fin_not_formated).strftime('%d/%m/%Y')

    entry = {
        "NumeroCandidat": dataCount,
        "Nom": nom,
        "Prenom": prenom,
        "Commission": commission,
        "CleCommission": cle_commission,
        "CodeCentreEpreuve": generateID(4) + str(dataCount),
        "LibelleCentreEpreuve": centre,
        "CodePostalCentreEpreuve": code_postale,
        "VilleCentreEpreuve": centre,
        "CodeAcademieCentreEpreuve": generateID(8) + str(dataCount),
        "LibelleAcademieCentreEpreuve": academie,
        "CodeSalle": "room_" + generateID(4) + str(dataCount),
        "DateVisioDebut":  date_visio + " " + str(heure).zfill(2) + ":" + minute,
        "DateVisioFin": date_visio + " " + str(heure + 1).zfill(2) + ":" + minute,
        "Concours": concours,
        "Epreuve": epreuve,
        "Option": option,
        "Concours_Libelle": concours,
        "DateConcoursDebut": date_concours_debut,
        "DateConcoursFin": date_concours_fin,
        "DateVisioDebutLocale": date_visio + " " + str(heure).zfill(2) + ":" + minute,
        "DateVisioFinLocale": date_visio + " " + str(heure + 1).zfill(2) + ":" + minute
    }

    datas.append(entry)

    # print(dataCount)
    progress("Generate Users : ", dataCount, dataNumber)
    # print("=============================================")
    return

def generateCSVForData():
    print("[{}] Creation of CSV: datas.csv".format(datetime.now()))
    headers = [
        "NumeroCandidat",
        "Nom",
        "Prenom",
        "Commission",
        "CleCommission",
        "CodeCentreEpreuve",
        "LibelleCentreEpreuve",
        "CodePostalCentreEpreuve",
        "VilleCentreEpreuve",
        "CodeAcademieCentreEpreuve",
        "LibelleAcademieCentreEpreuve",
        "CodeSalle",
        "DateVisioDebut",
        "DateVisioFin",
        "Concours",
        "Epreuve",
        "Option",
        "Concours_Libelle",
        "DateConcoursDebut",
        "DateConcoursFin",
        "DateVisioDebutLocale",
        "DateVisioFinLocale"
    ]

    with open("datas.csv", "w", encoding="UTF8", newline="") as f:
        writer = csv.DictWriter(f, fieldnames=headers, delimiter=';')
        writer.writeheader()
        writer.writerows(datas)

    # print("=============================================")

if __name__ == "__main__":

    # Confirmation de l'écrasement des fichiers csv pré-existants
    if isfile("datas.csv"):
        confirm = input("Overwrites files datas.csv ? [yN] ")
        if confirm != "y":
            exit("\nExit script.")

    dataNumber = 100
    for n in range(dataNumber):
        generateData()

    generateCSVForData()


# generateData()
# print(datas)
