import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { withTracker } from 'meteor/react-meteor-data';

import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import MaterialTable from 'material-table';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import EditIcon from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import { Snackbar } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
  titleBackgroud: {
    background: '#99ddff',
    borderRadius: '3px 3px 0 0',
    display: 'flex',
    justifyContent: 'space-between',
  },
  title: {
    margin: theme.spacing(3),
    fontSize: '120%',
    alignContent: 'flex-start',
  },
  titleRight: {
    alignContent: 'flex-end',
  },
  button: { width: 200, margin: theme.spacing(1) },
}));

const AdminUsers = ({ users, loading }) => {
  const history = useHistory();
  const classes = useStyles();

  const [openSnack, setOpenSnack] = React.useState(false);
  const [snackData, setSnackData] = React.useState({
    text: '',
    severity: 'success',
  });

  const handleCloseSnack = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnack(false);
  };

  const showAlert = (text, severity = 'success') => {
    setSnackData({ text, severity });
    setOpenSnack(true);
  };

  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <VerifiedUserIcon {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  const localization = {
    toolbar: {
      searchPlaceholder: 'Rechercher un utilisateur',
      searchTooltip: 'Rechercher un utilisateur',
    },
    pagination: {
      labelRowsSelect: 'Utilisateurs par page',
      firstAriaLabel: 'Première page',
      firstTooltip: 'Première page',
      previousAriaLabel: 'Page précédente',
      previousTooltip: 'Page précédente',
      nextAriaLabel: 'Page suivante',
      nextTooltip: 'Page suivante',
      lastAriaLabel: 'Dernière page',
      lastTooltip: 'Dernière page',
    },
    body: {
      emptyDataSourceMessage: 'Aucun utilisateur trouvé',
      addTooltip: 'Ajouter un utilisateur',
      editTooltip: 'Activer/Désactiver cet utilisateur',
      deleteTooltip: 'Supprimer cet utilisateur',
      editRow: {
        deleteText: 'Supprimer vraiment cet utilisateur ?',
        cancelTooltip: 'Annuler',
        saveTooltip: 'Enregistrer les changements',
      },
    },
  };

  const options = {
    pageSize: 10,
    pageSizeOptions: [10, 20, 50, 100],
    paginationType: 'stepped',
    addRowPosition: 'first',
    emptyRowsWhenPaging: false,
    actionsColumnIndex: 3,
  };

  const actions = [
    {
      icon: VerifiedUserIcon,
      tooltip: 'Activer/Désactiver cet utilisateur',
      onClick: (event, rowData) => {
        const active = rowData.isActive !== true;
        Meteor.call(
          'user.setActive',
          {
            userId: rowData._id,
            active,
          },
          (err) => {
            if (err) {
              showAlert(err.reason, 'error');
            } else {
              showAlert(`utilisateur ${rowData.username} ${active === true ? 'activé' : 'désactivé'}`);
            }
          },
        );
      },
    },
    (userData) => ({
      icon: SupervisorAccountIcon,
      tooltip: "Ajouter/Supprimer les droits d'administrateur",
      disabled: Meteor.userId() === userData._id,
      onClick: (event, rowData) => {
        const hasAdmin = Roles.userIsInRole(rowData._id, 'admin');
        Meteor.call(
          hasAdmin ? 'user.removeRole' : 'user.addRole',
          {
            userId: rowData._id,
            role: 'admin',
          },
          (err) => {
            if (err) {
              showAlert(err.reason, 'error');
            } else {
              showAlert(
                `droits d'administration ${hasAdmin ? 'retirés' : 'donnés'} à l'utilisateur ${rowData.username}`,
              );
            }
          },
        );
      },
    }),
    {
      icon: EditIcon,
      tooltip: 'Ajouter/Supprimer les droits de saisie',
      onClick: (event, rowData) => {
        const canEdit = Roles.userIsInRole(rowData._id, 'saisie');
        Meteor.call(
          canEdit ? 'user.removeRole' : 'user.addRole',
          {
            userId: rowData._id,
            role: 'saisie',
          },
          (err) => {
            if (err) {
              showAlert(err.reason, 'error');
            } else {
              showAlert(`droits de saisie ${canEdit ? 'retirés' : 'donnés'} à l'utilisateur ${rowData.username}`);
            }
          },
        );
      },
    },
  ];

  return (
    <Paper className={classes.root}>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={openSnack}
        autoHideDuration={5000}
        onClose={handleCloseSnack}
      >
        <Alert onClose={handleCloseSnack} severity={snackData.severity}>
          {snackData.text}
        </Alert>
      </Snackbar>
      <div className={classes.titleBackgroud}>
        <p className={classes.title}>Liste des utilisateurs</p>
        <div className={classes.titleRight} />
        <Button onClick={() => history.push('/home')} className={classes.button} variant="outlined" color="secondary">
          Retour
        </Button>
      </div>

      <MaterialTable
        columns={[
          {
            title: "Nom de l'utilisateur",
            field: 'username',
            defaultSort: 'asc',
          },
          {
            title: 'Utilisateur activé',
            field: 'isActive',
            type: 'boolean',
            defaultSort: 'asc',
            searchable: false,
            render: (rowData) => {
              const { isActive } = rowData;
              return isActive === true ? <Check /> : <Clear />;
            },
          },
          {
            title: 'Rôles',
            field: '',
            type: 'string',
            defaultSort: 'asc',
            searchable: false,
            render: (rowData) => Roles.getRolesForUser(rowData._id).sort().join(','),
          },
        ]}
        data={users}
        isLoading={loading}
        title=""
        icons={tableIcons}
        localization={localization}
        options={options}
        actions={actions}
        editable={{
          onRowDelete: (oldData) =>
            new Promise((resolve, reject) => {
              Meteor.call(
                'user.removeUser',
                {
                  userId: oldData._id,
                },
                (err, res) => {
                  if (err) {
                    showAlert(err.reason, 'error');
                    reject(err);
                  } else {
                    showAlert(`utilisateur ${oldData.username} supprimé`);
                    resolve(res);
                  }
                },
              );
            }),
        }}
      />
    </Paper>
  );
};

AdminUsers.propTypes = {
  users: PropTypes.arrayOf(PropTypes.element).isRequired,
  loading: PropTypes.bool.isRequired,
};

export default withTracker(() => {
  const subUsers = Meteor.subscribe('user.all');
  const subRoles = Meteor.subscribe('roles.admin');
  const loading = !subUsers.ready() || !subRoles.ready();
  const users = Meteor.users.find().fetch() || [];
  return {
    users,
    loading,
  };
})(AdminUsers);
