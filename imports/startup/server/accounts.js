import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { ServiceConfiguration } from 'meteor/service-configuration';
import { Roles } from 'meteor/alanning:roles';

import AppRoles from '../../api/user/user';

Accounts.validateLoginAttempt((attempt) => {
  if (Meteor.settings.public && Meteor.settings.public.enableKeycloak === true) return true;
  if (attempt.user && attempt.user.isActive === false) {
    if (Meteor.settings.private && Meteor.settings.private.adminUsers.includes(attempt.user.username)) {
      // auto activate admin users to make sure at least one user can log in
      Meteor.users.update(attempt.user._id, { $set: { isActive: true } });
    } else {
      throw new Meteor.Error('login.validate.userNotActive', 'Votre compte est en attente de validation');
    }
  }
  return true;
});

// initialize Roles

const existingRoles = Roles.getAllRoles()
  .fetch()
  .map((role) => role._id);
AppRoles.forEach((role) => {
  if (existingRoles.indexOf(role) === -1) Roles.createRole(role);
});

// Manage Keycloak authentication

if (Meteor.settings.keycloak) {
  if (Meteor.settings.public && Meteor.settings.public.enableKeycloak === true) {
    ServiceConfiguration.configurations.upsert(
      { service: 'keycloak' },
      {
        $set: {
          loginStyle: 'redirect',
          serverUrl: Meteor.settings.public.keycloakUrl,
          realm: Meteor.settings.public.keycloakRealm,
          clientId: Meteor.settings.keycloak.client,
          realmPublicKey: Meteor.settings.keycloak.pubkey,
          bearerOnly: false,
        },
      },
    );
  }
} else {
  console.log('No Keycloak configuration. Please invoke meteor with a settings file.');
}
// server side login hook (updates user if keycloak, checks admin roles from config)
Accounts.onLogin((details) => {
  let loggedUser = details.user.username;
  if (details.type === 'keycloak') {
    // update user informations from keycloak service data
    const updateInfos = {};
    const kcEmail = details.user.services.keycloak.email;
    if (details.user.services.keycloak.given_name) {
      updateInfos.firstName = details.user.services.keycloak.given_name;
    }
    if (details.user.services.keycloak.family_name) {
      updateInfos.lastName = details.user.services.keycloak.family_name;
    }
    if (details.user.username === undefined || details.user.username !== kcEmail) {
      // use email as username if no username yet or if email has changed on Keycloak
      if (details.user.username !== undefined) {
        console.log(`* Keycloak login : username changed from ${details.user.username} to ${kcEmail}`);
      }
      loggedUser = kcEmail;
      updateInfos.username = kcEmail;
    }
    if (details.user.isActive === false) {
      // auto activate user in keycloak mode
      // XXX FIXME : auto-validate based on email domain or if email in adminUsers ?
      updateInfos.isActive = true;
    }
    if (Object.keys(updateInfos).length > 0) {
      Meteor.users.update({ _id: details.user._id }, { $set: updateInfos });
    }
    // Manage email change
    let addEmail = false;
    if (details.user.emails && details.user.emails.length !== 0) {
      const currentEmail = details.user.emails[0].address;
      if (currentEmail !== kcEmail) {
        Accounts.removeEmail(details.user._id, currentEmail);
        addEmail = true;
      }
    } else {
      addEmail = true;
    }
    if (addEmail) {
      Accounts.addEmail(details.user._id, kcEmail, true);
    }
  }
  // check if user is defined as admin in settings
  if (Meteor.settings.private && Meteor.settings.private.adminUsers.includes(loggedUser)) {
    if (!Roles.userIsInRole(details.user._id, 'admin')) {
      Roles.addUsersToRoles(details.user._id, 'admin');
      console.log(`- rôle admin donné à ${loggedUser}`);
    }
  }
});

if (Meteor.settings.private && Meteor.settings.private.forbidClientAccountCreation === true) {
  Accounts.config({
    forbidClientAccountCreation: true,
  });
}
