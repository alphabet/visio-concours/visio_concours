import React, { useEffect } from 'react';
import { Meteor } from 'meteor/meteor';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import SignIn from '../pages/signin';
import SignUp from '../pages/signup';
import Home from '../pages/home';
import GenConnexion from '../pages/connexion';

function App() {
  useEffect(() => {
    // Load Jeetsi external API
    // see this conversation : https://stackoverflow.com/questions/34424845/adding-script-tag-to-react-jsx
    const script = document.createElement('script');
    script.src = `https://${Meteor.settings.public.domain}/external_api.js`;
    script.async = true;
    document.head.appendChild(script);
    return () => {
      document.head.removeChild(script);
    };
  }, []);

  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={SignIn} />
        <Route exact path="/signup" component={SignUp} />
        <Route exact path="/commission/:numcom/:candidat/:domain" component={GenConnexion} />
        <Route exact path="/commission/:numcom/:candidat" component={GenConnexion} />
        <Route exact path="/centredexamen/:numcentre/:candidat/:domain" component={GenConnexion} />
        <Route exact path="/centredexamen/:numcentre/:candidat" component={GenConnexion} />
        <Route component={Home} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
