import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';

// publish all list for admin
Meteor.publish('user.all', function listAll() {
  if (!Roles.userIsInRole(this.userId, 'admin')) {
    return this.ready();
  }
  return Meteor.users.find(
    {},
    {
      fields: Meteor.users.adminFields,
      sort: { name: 1 },
      limit: 10000,
    },
  );
});

// automatically publish all existing roles and current user's assignments
Meteor.publish(null, function publishRoles() {
  if (this.userId) {
    return Meteor.roles.find({});
  }
  return this.ready();
});
Meteor.publish(null, function publishAssignments() {
  if (this.userId) {
    return Meteor.roleAssignment.find({ 'user._id': this.userId });
  }
  return this.ready();
});

// publish all user assignments (admin)
Meteor.publish('roles.admin', function publishAdmins() {
  if (!Roles.userIsInRole(this.userId, 'admin')) {
    return this.ready();
  }
  return Meteor.roleAssignment.find(
    {},
    {
      sort: { _id: 1 },
      limit: 100000,
    },
  );
});
