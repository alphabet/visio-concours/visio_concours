import { Meteor } from 'meteor/meteor';
import { publishComposite } from 'meteor/reywood:publish-composite';
import SimpleSchema from 'simpl-schema';

import List from '../list';
import Commission from '../../commission/commission';
import Concours from '../../concours/concours';
import Centre from '../../centre/centre';
import RegEx from '../../regExp';

// publish all list
Meteor.publish('list.all', function listAll() {
  if (!this.userId) {
    return this.ready();
  }
  return List.find({}, { fields: List.publicFields, sort: { name: 1 }, limit: 1000 });
});

// publish a specific candidate
Meteor.publish('list.one', function listOne({ candidatId }) {
  if (!this.userId) {
    return this.ready();
  }
  return List.find({ _id: candidatId }, { fields: List.publicFields, sort: { name: 1 }, limit: 1 });
});

publishComposite('list.connexion', (candidatId) => {
  try {
    new SimpleSchema({
      candidatId: {
        type: String,
        regEx: RegEx.Id,
      },
    }).validate({ candidatId });
  } catch (err) {
    console.log(`publish list.connexion : ${err}`);
    throw Meteor.Error(`publish list.connexion : ${err}`);
  }
  return {
    find() {
      // Find matching candidate
      return List.find({ _id: candidatId }, { fields: List.publicFields, sort: { name: 1 }, limit: 1 });
    },
    children: [
      {
        find({ commission }) {
          // Find commission associated to candidate
          return Commission.find(
            { _id: commission },
            { fields: Commission.publicFields, sort: { numCommission: 1 }, limit: 1 },
          );
        },
        children: [
          {
            find({ concours }) {
              // find concours associated to commission
              return Concours.find({ _id: concours }, { fields: Concours.publicFields, sort: { name: 1 }, limit: 1 });
            },
          },
        ],
      },
      {
        find({ centre }) {
          // Find centre associated to candidate
          return Centre.find({ _id: centre }, { fields: Centre.publicFields, sort: { name: 1 }, limit: 1 });
        },
      },
    ],
  };
});
