import React, { useState, useEffect } from 'react';
import { Meteor } from 'meteor/meteor';
import { makeStyles } from '@material-ui/core/styles';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import { FormGroup, Paper, Select, FormControl, InputLabel, TextField, Button, Snackbar } from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import { useHistory } from 'react-router-dom';
import Centre from '../../api/centre/centre';
import Academie from '../../api/academie/academie';

const useStyles = makeStyles((theme) => ({
  main: {
    display: 'flex',
  },
  paper: {
    marginTop: theme.spacing(3),
    margin: 'auto',
    maxWidth: 800,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  validate: {
    display: 'flex',
  },
  spaceBetween: {
    width: 10,
  },
  button: { width: 150, margin: theme.spacing(1) },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function SaisieCentre({ centre, academies }) {
  const history = useHistory();
  const classes = useStyles();

  let infosCenter = {
    centreId: '',
    academie: '',
    numCentre: '',
    name: '',
  };

  const [state, setState] = useState(infosCenter);

  useEffect(() => {
    if (centre.name !== undefined) {
      const cen = Centre.findOne({ name: centre.name }) || '';

      infosCenter = {
        centreId: cen._id,
        academie: cen.academie,
        numCentre: cen.numCentre,
        name: cen.name,
      };
      setState(infosCenter);
    }
  }, [centre]);

  const [openSnack, setOpenSnack] = React.useState(false);
  const [snackData, setSnackData] = React.useState({
    text: '',
    severity: 'success',
  });

  const showAlert = (text, severity = 'success') => {
    setSnackData({ text, severity });
    setOpenSnack(true);
  };

  const handleCloseSnack = (reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnack(false);
  };

  const handleChange = (event) => {
    const { name } = event.target;
    setState({
      ...state,
      [name]: event.target.value,
    });
  };

  const handleResult = (goBack, err) => {
    if (err) {
      let message = err.reason;
      if (err.error === 'validation-error') {
        // display first validation message
        message = err.details.map((info) => info.message).join('\n');
      }
      showAlert(message, 'error');
      if (typeof err.error === 'string' && err.error.includes('duplicateEntry')) {
        document.getElementById('numCentre').focus();
      } else {
        document.getElementById('name').focus();
      }
    } else if (goBack) {
      history.push('/admincentres');
    } else if (centre.name === undefined) {
      showAlert('Le centre a été ajouté');
      setState({
        ...state,
        name: '',
        numCentre: '',
      });
      document.getElementById('name').value = '';
      document.getElementById('numCentre').value = '';
      document.getElementById('numCentre').focus();
    } else {
      showAlert('Le centre a été mis à jour.');
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const goBack = event.type !== 'submit';
    const { academie, numCentre, name } = state;

    if (academie === '') {
      showAlert('Une academie est nécessaire !', 'error');
      document.getElementById('academie').focus();
      return false;
    }

    if (numCentre === '') {
      showAlert('Un numéro de Centre est nécessaire !', 'error');
      document.getElementById('numCentre').focus();
      return false;
    }

    if (name === '') {
      showAlert("Un centre d'accueil est nécessaire !", 'error');
      document.getElementById('name').focus();
      return false;
    }

    const _centre = {
      academie,
      numCentre,
      name,
    };
    if (centre.name === undefined) {
      Meteor.call('center.createCenter', _centre, (err) => handleResult(goBack, err));
    } else {
      Meteor.call(
        'center.updateCenter',
        {
          centerId: centre._id,
          data: _centre,
        },
        (err) => handleResult(goBack, err),
      );
    }
    return false;
  };

  const renderAcademies = () => {
    return academies
      .sort((a, b) => {
        return a.name > b.name ? 1 : a.name < b.name ? -1 : 0;
      })
      .map((el) => {
        return (
          <option value={el._id} key={el._id}>
            {el.name}
          </option>
        );
      });
  };

  const renderTextfield = () => {
    return (
      <div className={classes.formControl}>
        <TextField
          autoFocus
          name="numCentre"
          margin="dense"
          id="numCentre"
          label="N° Centre"
          value={state.numCentre}
          fullWidth
          onChange={handleChange}
        />
        <TextField
          name="name"
          margin="dense"
          id="name"
          label="Centre d'accueil"
          value={state.name}
          fullWidth
          onChange={handleChange}
        />
      </div>
    );
  };

  return (
    <div className={classes.main}>
      <Paper className={classes.paper}>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={openSnack}
          autoHideDuration={5000}
          onClose={handleCloseSnack}
        >
          <Alert onClose={handleCloseSnack} severity={snackData.severity}>
            {snackData.text}
          </Alert>
        </Snackbar>
        <form onSubmit={handleSubmit}>
          <FormGroup>
            {' '}
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel htmlFor="filled-aca-native-simple">Académie</InputLabel>
              <Select
                native
                value={state.academie}
                onChange={handleChange}
                label="Académie"
                inputProps={{
                  name: 'academie',
                  id: 'filled-aca-native-simple',
                }}
              >
                <option aria-label="None" value="" />
                {renderAcademies()}
              </Select>
            </FormControl>
            {renderTextfield()}
          </FormGroup>
          <div className={classes.validate}>
            <Button
              className={classes.button}
              variant="outlined"
              color="secondary"
              onClick={() => history.push('/admincentres')}
            >
              Retour
            </Button>
            <div className={classes.spaceBetween} />
            {centre.name === undefined ? (
              <>
                <Button type="submit" variant="outlined" color="primary" className={classes.button}>
                  Soumettre et continuer
                </Button>
                <Button
                  type="button"
                  variant="outlined"
                  color="primary"
                  className={classes.button}
                  onClick={handleSubmit}
                >
                  Soumettre et retour
                </Button>
              </>
            ) : (
              <Button
                type="button"
                variant="outlined"
                color="primary"
                className={classes.button}
                onClick={handleSubmit}
              >
                Modifier
              </Button>
            )}
          </div>
        </form>
      </Paper>
    </div>
  );
}

SaisieCentre.propTypes = {
  centre: PropTypes.objectOf(PropTypes.element).isRequired,
  academies: PropTypes.arrayOf(PropTypes.element).isRequired,
};

export default withTracker(
  ({
    match: {
      params: { id },
    },
  }) => {
    let centre = {};
    if (id !== 'new') {
      Meteor.subscribe('centre.one', { centreId: id });
      centre = Centre.findOne({ _id: id }) || {};
    }
    Meteor.subscribe('academie.all');
    const academies = Academie.find().fetch() || [];
    return { academies, centre };
  },
)(SaisieCentre);
