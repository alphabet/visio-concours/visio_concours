import React, { useState, forwardRef } from 'react';
import { Meteor } from 'meteor/meteor';

import MuiAlert from '@material-ui/lab/Alert';
import { Paper, Button, Snackbar } from '@material-ui/core';
import MaterialTable from 'material-table';
import { useHistory } from 'react-router-dom';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
  titleBackgroud: {
    background: '#99ddff',
    borderRadius: '3px 3px 0 0',
    display: 'flex',
    justifyContent: 'space-between',
  },
  title: {
    margin: theme.spacing(3),
    fontSize: '120%',
    alignContent: 'flex-start',
  },
  titleRight: {
    alignContent: 'flex-end',
  },
  button: { width: 200, margin: theme.spacing(1) },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const AdminLogs = () => {
  const history = useHistory();
  const classes = useStyles();
  const [openSnack, setOpenSnack] = useState(false);
  const [snackData, setSnackData] = useState({
    text: '',
    severity: 'success',
  });

  const showAlert = (text, severity = 'success') => {
    setSnackData({ text, severity });
    setOpenSnack(true);
  };

  const handleCloseSnack = (reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnack(false);
  };

  const tableIcons = {
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  const localization = {
    toolbar: {
      searchPlaceholder: 'Rechercher une conférence',
      searchTooltip: 'Rechercher une conférence',
    },
    pagination: {
      labelRowsSelect: 'Entrées par page',
      firstAriaLabel: 'Première page',
      firstTooltip: 'Première page',
      previousAriaLabel: 'Page précédente',
      previousTooltip: 'Page précédente',
      nextAriaLabel: 'Page suivante',
      nextTooltip: 'Page suivante',
      lastAriaLabel: 'Dernière page',
      lastTooltip: 'Dernière page',
    },
    body: {
      emptyDataSourceMessage: 'Aucune conférence trouvée',
    },
  };

  const options = {
    pageSize: 20,
    pageSizeOptions: [10, 20, 50, 100],
    paginationType: 'stepped',
    emptyRowsWhenPaging: false,
  };

  return (
    <Paper className={classes.root}>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={openSnack}
        autoHideDuration={5000}
        onClose={handleCloseSnack}
      >
        <Alert onClose={handleCloseSnack} severity={snackData.severity}>
          {snackData.text}
        </Alert>
      </Snackbar>
      <div className={classes.titleBackgroud}>
        <p className={classes.title}>Historique des visio-conférences</p>
        <div className={classes.titleRight} />
        <Button onClick={() => history.push('/home')} className={classes.button} variant="outlined" color="secondary">
          Retour
        </Button>
      </div>

      <MaterialTable
        columns={[
          {
            title: 'Début',
            field: 'dateStart',
            type: 'datetime',
            defaultSort: 'desc',
          },
          {
            title: 'Fin',
            field: 'dateEnd',
            type: 'datetime',
          },
          { title: 'Nom', field: 'candidat' },
          { title: 'Académie', field: 'academie' },
          { title: "Centre d'accueil", field: 'centre' },
          {
            title: 'N° Inscription',
            field: 'numInscription',
          },
          {
            title: 'Concours (numéro + intitulé)',
            field: 'concoursNumero',
            render: (rowData) => `${rowData.concoursNumero} ${rowData.concoursName}`,
          },
          { title: 'Commission', field: 'commission' },
          { title: 'Serveur', field: 'domain' },
          { title: 'Rôle', field: 'isJury', render: (rowData) => (rowData.isJury ? 'Jury' : 'Candidat') },
        ]}
        data={(query) =>
          new Promise((resolve) => {
            const sort = {};
            if (query.orderBy && query.orderDirection) {
              sort[query.orderBy.field] = query.orderDirection === 'asc' ? 1 : -1;
            }
            Meteor.call(
              'log.getLogs',
              { pageSize: query.pageSize, page: query.page + 1, filter: query.search, sort },
              (err, res) => {
                if (err) {
                  showAlert(err.reason, 'error');
                  resolve({ data: [], page: 0, totalCount: 0 });
                } else {
                  // prepare your data and then call resolve like this:
                  resolve({
                    data: res.data,
                    page: res.page - 1,
                    totalCount: res.totalCount,
                  });
                }
              },
            );
          })
        }
        title=""
        options={options}
        localization={localization}
        icons={tableIcons}
      />
    </Paper>
  );
};

AdminLogs.propTypes = {};

export default AdminLogs;
