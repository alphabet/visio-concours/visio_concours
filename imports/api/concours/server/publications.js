import { Meteor } from 'meteor/meteor';
import Concours from '../concours';

// publish all exams
Meteor.publish('concours.all', function concoursAll() {
  if (!this.userId) {
    return this.ready();
  }
  return Concours.find({}, { sort: { name: 1 }, limit: 1000 });
});

Meteor.publish('concours.one', function concoursOne({ concoursId }) {
  if (!this.userId) {
    return this.ready();
  }
  return Concours.find({ _id: concoursId }, { fields: Concours.publicFields, sort: { name: 1 }, limit: 1 });
});
