#!/bin/bash

echo "METEOR_SETTINGS=$(cat settings-development.json | jq -c .)" > .env
docker-compose -f docker-compose-dev.yml up -d
