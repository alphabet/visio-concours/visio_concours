import { Meteor } from 'meteor/meteor';
import React, { useEffect, useState } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import { Button, Snackbar } from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';

import { expandCandidat } from '../utils';
import List from '../../api/list/list';
import Spinner from '../components/Spinner';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  cell: {
    textTransform: 'uppercase',
  },
  space: {
    margin: theme.spacing(2),
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    height: window.screen.height - 100,
  },
  table: {
    width: '1000px',
  },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function Connexion({ candidat, domain, loading, loggingIn, currentUser }) {
  const classes = useStyles();
  const history = useHistory();

  /// XXX needs candidat to be loaded !!!
  const [candidatData, setCandidatData] = useState({ domain: null });
  const [api, setApi] = useState(null);
  const [jitsiDomain, setJitsiDomain] = useState('');
  const [openSnack, setOpenSnack] = React.useState(false);
  const [snackData, setSnackData] = React.useState({
    text: '',
    severity: 'success',
  });

  const showAlert = (text, severity = 'success') => {
    setSnackData({ text, severity });
    setOpenSnack(true);
  };

  const handleCloseSnack = (reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnack(false);
  };

  const disconnect = () => {
    Meteor.logout();
    history.push('/');
  };

  const logEnd = (forcedId = null) => {
    // use forcedId if given (window unload), else api.logId if available
    let logId = forcedId;
    if (logId === null) {
      logId = api && api.logId ? api.logId : null;
    }
    if (logId) {
      Meteor.call('log.logEnd', { logId }, (err) => {
        if (err) console.log(`Error in end log (${logId})`, err);
      });
    }
  };

  const initApi = () => {
    let defaultLocalDisplayName = candidatData.candidat;
    let isJury = false;
    if (history.location.pathname.startsWith('/commission')) {
      isJury = true;
      defaultLocalDisplayName = `(${candidatData.numCom}) ${candidatData.nomConcours}`;
    }
    // if domain is set as prop, update domain in database for this candidate
    if (domain && candidatData.domain !== domain) {
      Meteor.call('list.updateDomain', { candidatId: candidatData._id, domain }, (err) => {
        if (err) console.log(err);
      });
    }
    // if no domain specified as prop, use the one provided for this candidate
    const newDomain = domain || candidatData.domain || Meteor.settings.public.domain;
    setJitsiDomain(newDomain);
    const options = {
      roomName: candidatData._id,
      noSLL: false,
      width: window.screen.width,
      height: window.screen.height - 160,
      parentNode: document.querySelector('#meet'),
      configOverwrite: {
        defaultLanguage: 'fr',
      },
      interfaceConfigOverwrite: {
        // filmStripOnly: true,
        DEFAULT_LOCAL_DISPLAY_NAME: '',
        TOOLBAR_BUTTONS: [],
      },
    };
    // global JitsiMeetExternalAPI
    // We need to attach JitsiMeetExternalAPI to window as it is attached to global exports object
    // see: https://github.com/jitsi/jitsi-meet/issues/4671
    window.JitsiMeetExternalAPI = window.JitsiMeetExternalAPI || window.exports.JitsiMeetExternalAPI;
    const jApi = new window.JitsiMeetExternalAPI(newDomain, options);
    jApi.executeCommands({
      displayName: defaultLocalDisplayName,
    });
    jApi.removeEventListeners(['incomingMessage', 'outgoingMessageListener']);
    jApi.addEventListener('readyToClose', function closeApi() {
      history.goBack();
    });
    // log call start
    jApi.logId = null;
    Meteor.call('log.logBegin', { candidatId: candidatData._id, isJury, domain: newDomain }, (err, res) => {
      if (err) {
        console.log('Error adding new log : ', err);
      } else {
        window.addEventListener(
          'beforeunload',
          () => {
            logEnd(res);
          },
          false,
        );
        jApi.logId = res;
      }
    });
    setApi(jApi);
  };

  useEffect(() => {
    if (!loading) {
      const newData = expandCandidat(candidat);
      setCandidatData(newData);
    }
  }, [candidat]);

  useEffect(() => {
    const newDomain = domain || candidatData.domain || Meteor.settings.public.domain;
    if (candidatData.domain !== null && newDomain !== jitsiDomain) {
      if (api) {
        logEnd();
        api.executeCommand('hangup');
        api.dispose();
      }
      // call room creation API if necessary (Renater)
      if (newDomain.endsWith('rendez-vous.renater.fr')) {
        Meteor.call('list.initRoom', { roomName: candidatData._id, domainRdv: newDomain }, (err, res) => {
          if (err) {
            showAlert('Erreur rencontrée lors de la création du salon', 'error');
            console.log(err);
          } else if (res === true) {
            initApi();
          } else showAlert('Erreur rencontrée lors de la création du salon', 'error');
        });
      } else {
        initApi();
      }
    }
    return () => {
      // cleanup api on component dismount
      if (api) api.dispose();
    };
  }, [candidatData.domain]);

  const goBack = () => {
    logEnd();
    Meteor.call('list.updateColor', { candidatId: candidat._id, color: true });
    history.goBack();
  };

  return (
    <div className={classes.root}>
      {!currentUser && !loggingIn ? (
        disconnect()
      ) : currentUser && !loading ? (
        <>
          <Snackbar
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
            open={openSnack}
            autoHideDuration={5000}
            onClose={handleCloseSnack}
          >
            <Alert onClose={handleCloseSnack} severity={snackData.severity}>
              {snackData.text}
            </Alert>
          </Snackbar>
          <Table className={classes.table} data={candidatData._id}>
            <TableBody>
              <TableRow>
                <TableCell>
                  nom : <b>{candidatData.candidat}</b>
                </TableCell>
                <TableCell>
                  concours : <b>{candidatData.nomConcours}</b>
                </TableCell>
                <TableCell>
                  centre d&apos;accueil : <b>{candidatData.nomCentre}</b>
                </TableCell>
                <TableCell>
                  <Button className={classes.space} onClick={goBack} color="secondary" variant="outlined">
                    Deconnexion
                  </Button>
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </>
      ) : (
        <Spinner />
      )}
      <div id="meet" className={classes.paper} />
    </div>
  );
}

Connexion.defaultProps = {
  domain: '',
  currentUser: null,
};

Connexion.propTypes = {
  loading: PropTypes.bool.isRequired,
  loggingIn: PropTypes.bool.isRequired,
  currentUser: PropTypes.objectOf(PropTypes.element),
  candidat: PropTypes.objectOf(PropTypes.element).isRequired,
  domain: PropTypes.string,
};

const ConnexionTrack = withTracker(({ candidatId, domain }) => {
  const loggingIn = Meteor.loggingIn();
  const subList = Meteor.subscribe('list.connexion', candidatId);
  const loading = !subList.ready();
  return {
    loading,
    candidat: List.findOne({ _id: candidatId }) || {},
    domain,
    loggingIn,
    currentUser: Meteor.user(),
  };
})(Connexion);

const GenConnexion = ({ match }) => {
  const { domain, backupDomain } = Meteor.settings.public;
  return match.params.domain ? (
    <ConnexionTrack
      candidatId={match.params.candidat}
      domain={match.params.domain === 'backup' ? backupDomain : domain}
    />
  ) : (
    <ConnexionTrack candidatId={match.params.candidat} />
  );
};

GenConnexion.propTypes = {
  match: PropTypes.objectOf(PropTypes.element).isRequired,
};

export default GenConnexion;
