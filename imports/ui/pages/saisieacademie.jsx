import React, { useState, useEffect } from 'react';
import { Meteor } from 'meteor/meteor';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data';
import { FormGroup, Paper, TextField, Button, Snackbar } from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import { useHistory } from 'react-router-dom';
import Academie from '../../api/academie/academie';

const useStyles = makeStyles((theme) => ({
  main: {
    display: 'flex',
  },
  paper: {
    marginTop: theme.spacing(3),
    margin: 'auto',
    maxWidth: 800,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  validate: {
    display: 'flex',
  },
  spaceBetween: {
    width: 10,
  },
  button: { width: 150, margin: theme.spacing(1) },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function SaisieAcademie({ academie }) {
  const history = useHistory();
  const classes = useStyles();
  let infosAca = { name: '' };

  const [state, setState] = useState(infosAca);
  useEffect(() => {
    if (academie.name !== undefined) {
      const aca = Academie.findOne({ name: academie.name }) || '';
      if (aca !== undefined) {
        infosAca = { name: aca.name };
      }
      setState(infosAca);
    }
  }, [academie]);

  const [openSnack, setOpenSnack] = useState(false);
  const [snackData, setSnackData] = useState({
    text: '',
    severity: 'success',
  });

  const showAlert = (text, severity = 'success') => {
    setSnackData({ text, severity });
    setOpenSnack(true);
  };

  const handleCloseSnack = (reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnack(false);
  };

  const handleChange = (event) => {
    const { name } = event.target;
    setState({
      ...state,
      [name]: event.target.value,
    });
  };

  const handleResult = (goBack, err) => {
    if (err) {
      let message = err.reason;
      if (err.error === 'validation-error') {
        // display first validation message
        message = err.details.map((info) => info.message).join('\n');
      }
      showAlert(message, 'error');
      if (typeof err.error === 'string' && err.error.includes('duplicateEntry')) {
        document.getElementById('academieField').focus();
      }
    } else if (goBack) {
      history.push('/adminacademies');
    } else if (academie.name === undefined) {
      showAlert('Cette academie a été ajoutée');
      setState({ nom: '' });
      document.getElementById('academieField').value = '';
      document.getElementById('academieField').focus();
    } else {
      showAlert('Cette academie a été mise à jour.');
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const goBack = event.type !== 'submit';

    if (state.name === '') {
      showAlert('Une academie est nécessaire !', 'error');
      document.getElementById('academieField').focus();
      return false;
    }

    const _academie = {
      name: state.name,
    };

    if (academie.name === undefined) {
      // When route is "adminacademies/new" => Create Aca
      Meteor.call('academie.createAcademie', _academie, (err) => handleResult(goBack, err));
    } else {
      // When route is "adminacademies/:id" => Update Aca
      Meteor.call(
        'academie.updateAcademie',
        {
          academieId: academie._id,
          data: _academie,
        },
        (err) => handleResult(goBack, err),
      );
    }
    return false;
  };

  const renderTextfield = () => {
    return (
      <div className={classes.formControl}>
        <TextField
          name="name"
          margin="dense"
          autoFocus
          id="academieField"
          label="Académie"
          value={state.name}
          fullWidth
          onChange={handleChange}
        />
      </div>
    );
  };

  return (
    <div className={classes.main}>
      <Paper className={classes.paper}>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={openSnack}
          autoHideDuration={5000}
          onClose={handleCloseSnack}
        >
          <Alert onClose={handleCloseSnack} severity={snackData.severity}>
            {snackData.text}
          </Alert>
        </Snackbar>
        <form onSubmit={handleSubmit}>
          <FormGroup>{renderTextfield()}</FormGroup>
          <div className={classes.validate}>
            <Button
              className={classes.button}
              variant="outlined"
              color="secondary"
              onClick={() => history.push('/adminacademies')}
            >
              Retour
            </Button>
            <div className={classes.spaceBetween} />
            {academie.name === undefined ? (
              <>
                <Button type="submit" variant="outlined" color="primary" className={classes.button}>
                  Soumettre et continuer
                </Button>
                <Button
                  type="button"
                  variant="outlined"
                  color="primary"
                  className={classes.button}
                  onClick={handleSubmit}
                >
                  Soumettre et retour
                </Button>
              </>
            ) : (
              <Button
                type="button"
                variant="outlined"
                color="primary"
                className={classes.button}
                onClick={handleSubmit}
              >
                Modifier
              </Button>
            )}
          </div>
        </form>
      </Paper>
    </div>
  );
}

SaisieAcademie.propTypes = {
  academie: PropTypes.objectOf(PropTypes.element).isRequired,
};

export default withTracker(
  ({
    match: {
      params: { id },
    },
  }) => {
    let academie = {};
    if (id !== 'new') {
      Meteor.subscribe('academie.all', { academieId: id });
      academie = Academie.findOne({ _id: id }) || {};
    }
    return { academie };
  },
)(SaisieAcademie);
