import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import { _ } from 'meteor/underscore';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';
import axios from 'axios';

import List from './list';
import RegEx from '../regExp';

export const updateColor = new ValidatedMethod({
  name: 'list.updateColor',
  validate: new SimpleSchema({
    candidatId: {
      type: String,
      regEx: RegEx.Id,
      label: 'Identifiant du candidat',
    },
    color: {
      type: Boolean,
    },
  }).validator(),
  run({ candidatId, color }) {
    const authorized = !!this.userId;
    if (!authorized) {
      throw new Meteor.Error('api.list.updateColor.notPermitted', 'Connexion requise');
    }
    List.update(candidatId, { $set: { color } });
  },
});

export const updateDomain = new ValidatedMethod({
  name: 'list.updateDomain',
  validate: new SimpleSchema({
    candidatId: {
      type: String,
      regEx: RegEx.Id,
      label: 'Identifiant du candidat',
    },
    domain: {
      type: String,
    },
  }).validator(),
  run({ candidatId, domain }) {
    const authorized = !!this.userId;
    if (!authorized) {
      throw new Meteor.Error('api.list.updateDomain.notPermitted', 'Connexion requise');
    }
    List.update(candidatId, { $set: { domain } });
  },
});

export const initRoom = new ValidatedMethod({
  name: 'list.initRoom',
  validate: new SimpleSchema({
    roomName: {
      type: String,
      min: 1,
    },
    domainRdv: {
      type: String,
      min: 1,
    },
  }).validator(),
  run({ roomName, domainRdv }) {
    const authorized = !!this.userId;
    if (!authorized) {
      throw new Meteor.Error('api.list.initRoom.notPermitted', 'Connexion requise');
    }
    return axios
      .get('https://gouv1.rendez-vous.renater.fr/home/rest.php/Room', {
        params: {
          roomName,
          domain: domainRdv,
        },
        headers: {
          Accept: 'application/json',
        },
      })
      .then((response) => {
        if (response.data.status === 'succeed') return true;
        console.log('Error in api.list.initRoom: ', response.data.status);
        return false;
      })
      .catch((error) => {
        console.log(
          'Error in api.list.initRoom: ',
          error.response && error.response.data ? error.response.data : error,
        );
        return false;
      });
  },
});

export const createCandidat = new ValidatedMethod({
  name: 'list.createCandidat',
  validate: List.schema.validator({ clean: true }),
  run(data) {
    if (!Roles.userIsInRole(this.userId, ['admin', 'saisie'])) {
      throw new Meteor.Error(
        'api.list.createCandidat.notPermitted',
        'Vos droits sont insuffisants pour réaliser cette action',
      );
    }
    try {
      List.insert({ ...data });
    } catch (error) {
      if (error.code === 11000) {
        throw new Meteor.Error(
          'api.list.createCandidat.duplicateNumInscription',
          "Ce numéro d'inscription existe déjà",
        );
      } else {
        throw error;
      }
    }
  },
});

export const updateCandidat = new ValidatedMethod({
  name: 'list.updateCandidat',
  validate: new SimpleSchema({
    candidatId: {
      type: String,
      regEx: RegEx.Id,
      label: 'Identifiant du candidat',
    },
    data: List.schema.omit('color', 'domain'),
  }).validator({ clean: true }),

  run({ data, candidatId }) {
    if (!Roles.userIsInRole(this.userId, ['admin', 'saisie'])) {
      throw new Meteor.Error(
        'api.list.updateCandidat.notPermitted',
        'Vos droits sont insuffisants pour réaliser cette action',
      );
    }
    // check concours existence
    const candidat = List.findOne(candidatId);
    if (candidat === undefined) {
      throw new Meteor.Error('api.list.updateCandidat.unknownCandidat', 'Identifiant de candidat inconnu');
    }
    try {
      return List.update({ _id: candidatId }, { $set: { ...data } });
    } catch (error) {
      if (error.code === 11000) {
        throw new Meteor.Error(
          'api.list.updateCandidat.duplicateNumInscription',
          "Ce numéro d'inscription existe déjà",
        );
      } else {
        throw error;
      }
    }
  },
});

export const removeCandidat = new ValidatedMethod({
  name: 'list.removeCandidat',
  validate: new SimpleSchema({
    candidatId: {
      type: String,
      regEx: RegEx.Id,
      label: 'Identifiant du candidat',
    },
  }).validator(),

  run({ candidatId }) {
    if (!Roles.userIsInRole(this.userId, ['admin', 'saisie'])) {
      throw new Meteor.Error(
        'api.list.removeCandidat.notPermitted',
        'Vos droits sont insuffisants pour réaliser cette action',
      );
    }
    // check candidat existence
    const candidat = List.findOne(candidatId);
    if (candidat === undefined) {
      throw new Meteor.Error('api.list.removeCandidat.unknownCandidat', 'Identifiant de candidat inconnu');
    }
    List.remove(candidatId);
  },
});

export const checkInscription = new ValidatedMethod({
  name: 'list.checkInscription',
  validate: new SimpleSchema({
    numInscription: {
      type: String,
      min: 1,
      label: "Numéro d'inscription du candidat",
    },
    ignoreEntry: {
      type: String,
      defaultValue: '',
    },
  }).validator({ clean: true }),

  run({ numInscription, ignoreEntry }) {
    // check that user is logged in
    if (!this.userId) {
      throw new Meteor.Error('api.list.checkInscription', 'Connexion requise');
    }
    // return false if another candidate as the same number
    let query = { numInscription };
    if (ignoreEntry !== '') {
      query = { ...query, _id: { $ne: ignoreEntry } };
    }
    const existing = List.find(query).count();
    if (existing > 0) return false;
    return true;
  },
});

// Get list of all method names on User
const LISTS_METHODS = _.pluck(
  [updateColor, updateDomain, initRoom, createCandidat, updateCandidat, removeCandidat],
  'name',
);

if (Meteor.isServer) {
  // Only allow 5 list operations per connection per second
  DDPRateLimiter.addRule(
    {
      name(name) {
        return _.contains(LISTS_METHODS, name);
      },

      // Rate limit per connection ID
      connectionId() {
        return true;
      },
    },
    5,
    1000,
  );
}
