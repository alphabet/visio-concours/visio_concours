/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */

import { PublicationCollector } from 'meteor/johanbrook:publication-collector';
import { chai, assert } from 'meteor/practicalmeteor:chai';
import { Meteor } from 'meteor/meteor';
import { _ } from 'meteor/underscore';
import faker from 'faker';
import { Factory } from 'meteor/dburles:factory';
import { Random } from 'meteor/random';
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';
import './publications';

import Academie from '../academie';
import { createAcademie, updateAcademie, removeAcademie } from '../methods';

describe('academie', function () {
  describe('mutators', function () {
    it('builds correctly from factory', function () {
      const newAcademie = Factory.create('academie');
      assert.typeOf(newAcademie, 'object');
    });
  });
  describe('publications', function () {
    let userId;
    before(function () {
      Meteor.users.remove({});
      userId = Accounts.createUser({
        username: faker.internet.email(),
        password: 'toto',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Meteor.users.update(userId, { $set: { isActive: true } });
      Academie.remove({});
      _.times(4, () => {
        Factory.create('academie');
      });
    });
    describe('academie.all', function () {
      it('sends all academies', function (done) {
        const collector = new PublicationCollector({ userId });
        collector.collect('academie.all', (collections) => {
          chai.assert.equal(collections.academie.length, 4);
          done();
        });
      });
    });
  });
  describe('methods', function () {
    let userId;
    let saisieUserId;
    let adminUserId;
    beforeEach(function () {
      Meteor.users.remove({});
      Meteor.roles.remove({});
      Meteor.roleAssignment.remove({});
      Academie.remove({});
      const email = faker.internet.email();
      const email2 = faker.internet.email();
      adminUserId = Accounts.createUser({
        email: 'eole@ac-test.fr',
        username: 'eole',
        password: 'eole',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Roles.createRole('admin');
      Roles.addUsersToRoles(adminUserId, 'admin');
      Meteor.users.update(adminUserId, { $set: { isActive: true } });

      saisieUserId = Accounts.createUser({
        email2,
        username: email2,
        password: 'toto',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Roles.createRole('saisie');
      Roles.addUsersToRoles(saisieUserId, 'saisie');
      Meteor.users.update(saisieUserId, { $set: { isActive: true } });

      userId = Accounts.createUser({
        email,
        username: email,
        password: 'toto',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Meteor.users.update(userId, { $set: { isActive: true } });
    });
    describe('createAcademie', function () {
      it('does user with admin role create an academie', function () {
        createAcademie._execute({ userId: adminUserId }, { name: 'monAcad' });
        const reqAcad = Academie.findOne({ name: 'monAcad' });
        assert.typeOf(reqAcad, 'object');
      });
      it('does user with saisie role create an academie', function () {
        createAcademie._execute({ userId: saisieUserId }, { name: 'monAcad2' });
        const reqAcad = Academie.findOne({ name: 'monAcad2' });
        assert.typeOf(reqAcad, 'object');
      });
      it('does not create an academie if user has no role', function () {
        assert.throws(
          () => {
            createAcademie._execute({ userId }, { name: 'monAcad3' });
          },
          Meteor.Error,
          /api.academie.createAcademie.notPermitted/,
        );
      });
      it('does not create an academie if no user is connected', function () {
        assert.throws(
          () => {
            createAcademie._execute({}, { name: 'monAcad4' });
          },
          Meteor.Error,
          /api.academie.createAcademie.notPermitted/,
        );
      });
      it('does not create a duplicate academie', function () {
        createAcademie._execute({ userId: adminUserId }, { name: 'monAcad' });
        assert.throws(
          () => {
            createAcademie._execute({ userId: adminUserId }, { name: 'monAcad' });
          },
          Meteor.Error,
          /api.academie.createAcademie.duplicateEntry/,
        );
      });
    });
    describe('updateAcademie', function () {
      it('does user with admin role update an academie', function () {
        const acadId = Factory.create('academie', { name: 'monAcad' })._id;
        updateAcademie._execute({ userId: adminUserId }, { academieId: acadId, data: { name: 'updatedAcad' } });
        const reqAcad = Academie.findOne({ _id: acadId });
        assert.typeOf(reqAcad, 'object');
        assert.equal(reqAcad.name, 'updatedAcad');
      });
      it('does user with saisie role update an academie', function () {
        const acadId = Factory.create('academie', { name: 'monAcad' })._id;
        updateAcademie._execute({ userId: saisieUserId }, { academieId: acadId, data: { name: 'updatedAcad2' } });
        const reqAcad = Academie.findOne({ _id: acadId });
        assert.typeOf(reqAcad, 'object');
        assert.equal(reqAcad.name, 'updatedAcad2');
      });
      it('does not update an academie if user has no role', function () {
        const acadId = Factory.create('academie', { name: 'monAcad' })._id;
        assert.throws(
          () => {
            updateAcademie._execute({ userId }, { academieId: acadId, data: { name: 'updatedAcad3' } });
          },
          Meteor.Error,
          /api.academie.updateAcademie.notPermitted/,
        );
      });
      it('does not update an academie if no user is connected', function () {
        const acadId = Factory.create('academie', { name: 'monAcad' })._id;
        assert.throws(
          () => {
            updateAcademie._execute({}, { academieId: acadId, data: { name: 'updatedAcad4' } });
          },
          Meteor.Error,
          /api.academie.updateAcademie.notPermitted/,
        );
      });
      it('does not update a not found academie', function () {
        assert.throws(
          () => {
            updateAcademie._execute({ userId: adminUserId }, { academieId: Random.id(), data: { name: 'noAcad' } });
          },
          Meteor.Error,
          /api.academie.updateAcademie.unknownAcademie/,
        );
      });
      it('does not update to an existing academie', function () {
        Factory.create('academie', { name: 'existingAcad' });
        const acadId = Factory.create('academie', { name: 'monAcad' })._id;
        assert.throws(
          () => {
            updateAcademie._execute({ userId: adminUserId }, { academieId: acadId, data: { name: 'existingAcad' } });
          },
          Meteor.Error,
          /api.academie.updateAcademie.duplicateEntry/,
        );
      });
    });
    describe('removeAcademie', function () {
      it('does user with admin role remove an academie', function () {
        const acadId = Factory.create('academie', { name: 'monAcad' })._id;
        assert.typeOf(Academie.findOne({ _id: acadId }), 'object');
        removeAcademie._execute({ userId: adminUserId }, { academieId: acadId });
        assert.equal(Academie.findOne({ _id: acadId }), undefined);
      });
      it('does user with saisie role remove an academie', function () {
        const acadId = Factory.create('academie', { name: 'monAcad' })._id;
        assert.typeOf(Academie.findOne({ _id: acadId }), 'object');
        removeAcademie._execute({ userId: saisieUserId }, { academieId: acadId });
        assert.equal(Academie.findOne({ _id: acadId }), undefined);
      });
      it('does not remove an academie if user has no role', function () {
        const acadId = Factory.create('academie', { name: 'monAcad' })._id;
        assert.typeOf(Academie.findOne({ _id: acadId }), 'object');
        assert.throws(
          () => {
            removeAcademie._execute({ userId }, { academieId: acadId });
          },
          Meteor.Error,
          /api.academie.removeAcademie.notPermitted/,
        );
      });
      it('does not remove an academie if no user is connected', function () {
        const acadId = Factory.create('academie', { name: 'monAcad' })._id;
        assert.typeOf(Academie.findOne({ _id: acadId }), 'object');
        assert.throws(
          () => {
            removeAcademie._execute({}, { academieId: acadId });
          },
          Meteor.Error,
          /api.academie.removeAcademie.notPermitted/,
        );
      });
      it('does not remove a not found academie', function () {
        const acadId = Factory.create('academie', { name: 'monAcad' })._id;
        assert.typeOf(Academie.findOne({ _id: acadId }), 'object');
        assert.throws(
          () => {
            removeAcademie._execute({ userId: adminUserId }, { academieId: Random.id() });
          },
          Meteor.Error,
          /api.academie.removeAcademie.unknownAcademie/,
        );
      });
      it('does not remove an academie which still has centre', function () {
        const acadId = Factory.create('academie', { name: 'monAcad' })._id;
        assert.typeOf(Academie.findOne({ _id: acadId }), 'object');
        Factory.create('centre', { academie: acadId });
        assert.throws(
          () => {
            removeAcademie._execute({ userId: adminUserId }, { academieId: acadId });
          },
          Meteor.Error,
          /api.academie.removeAcademie.relatedDataStillExist/,
        );
      });
    });
  });
});
