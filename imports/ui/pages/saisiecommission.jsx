import React, { useState, useEffect } from 'react';
import { Meteor } from 'meteor/meteor';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data';
import { FormGroup, FormControl, InputLabel, Select, Paper, TextField, Button, Snackbar } from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import { useHistory } from 'react-router-dom';
import Commission from '../../api/commission/commission';
import Concours from '../../api/concours/concours';

const useStyles = makeStyles((theme) => ({
  main: {
    display: 'flex',
  },
  paper: {
    marginTop: theme.spacing(3),
    margin: 'auto',
    maxWidth: 800,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  validate: {
    display: 'flex',
  },
  spaceBetween: {
    width: 10,
  },
  button: { width: 150, margin: theme.spacing(1) },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function SaisieCommission({ commission, concours }) {
  const history = useHistory();
  const classes = useStyles();

  let infosCommission = {
    commissionId: '',
    commissionName: '',
    numCommission: '',
    concours: '',
    codeJury: '',
  };

  const [state, setState] = useState(infosCommission);

  useEffect(() => {
    if (commission.name !== undefined) {
      infosCommission = {
        commissionId: commission._id,
        commissionName: commission.name,
        numCommission: commission.numCommission,
        concours: commission.concours,
        codeJury: commission.codeJury,
      };
      setState(infosCommission);
    }
  }, [commission]);

  const [openSnack, setOpenSnack] = React.useState(false);
  const [snackData, setSnackData] = React.useState({
    text: '',
    severity: 'success',
  });

  const showAlert = (text, severity = 'success') => {
    setSnackData({ text, severity });
    setOpenSnack(true);
  };

  const handleCloseSnack = (reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnack(false);
  };

  const handleChange = (event) => {
    const { name } = event.target;
    setState({
      ...state,
      [name]: event.target.value,
    });
  };

  const handleResult = (goBack, err) => {
    if (err) {
      let message = err.reason;
      if (err.error === 'validation-error') {
        // display first validation message
        message = err.details.map((info) => info.message).join('\n');
      }
      showAlert(message, 'error');
      if (typeof err.error === 'string' && err.error.includes('duplicateEntry')) {
        document.getElementById('numCommission').focus();
      } else {
        document.getElementById('commissionName').focus();
      }
    } else if (goBack) {
      history.push('/admincommissions');
    } else if (commission.name === undefined) {
      showAlert('La commission a été ajouté');
      setState({
        ...state,
        commissionName: '',
        numCommission: '',
        codeJury: '',
      });
      document.getElementById('commissionName').value = '';
      document.getElementById('numCommission').value = '';
      document.getElementById('numCommission').focus();
      document.getElementById('codeJury').value = '';
    } else {
      showAlert('La commission a été mis à jour.');
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const goBack = event.type !== 'submit';
    const { commissionName, numCommission, concours: concoursST, codeJury } = state;

    if (commissionName === '') {
      showAlert('Un nom de commission est nécessaire !', 'error');
      document.getElementById('commissionName').focus();
      return false;
    }

    if (numCommission === '') {
      showAlert('Un numéro de commission est nécessaire !', 'error');
      document.getElementById('numCommission').focus();
      return false;
    }

    if (concoursST === '') {
      showAlert('Un concours est nécessaire !', 'error');
      document.getElementById('concours').focus();
      return false;
    }

    const comm = {
      name: commissionName,
      numCommission,
      concours: concoursST,
      codeJury,
    };
    if (commission.name === undefined) {
      Meteor.call('commission.createCommission', comm, (err) => handleResult(goBack, err));
    } else {
      Meteor.call(
        'commission.updateCommission',
        {
          commissionId: commission._id,
          data: comm,
        },
        (err) => handleResult(goBack, err),
      );
    }
    return false;
  };

  const renderTextfield = () => {
    return (
      <div className={classes.formControl}>
        <TextField
          autoFocus
          name="numCommission"
          margin="dense"
          id="numCommission"
          label="Numéro de commission"
          value={state.numCommission}
          fullWidth
          onChange={handleChange}
        />
        <TextField
          name="commissionName"
          margin="dense"
          id="commissionName"
          label="Nom de la commission"
          value={state.commissionName}
          fullWidth
          onChange={handleChange}
        />
        <TextField
          name="codeJury"
          margin="dense"
          id="codeJury"
          label="Code d'accès à la commission"
          value={state.codeJury}
          fullWidth
          onChange={handleChange}
        />
      </div>
    );
  };

  const renderConcours = () => {
    return concours
      .sort((a, b) => {
        return a.numero > b.numero ? 1 : a.numero < b.numero ? -1 : 0;
      })
      .map((el) => {
        return (
          <option name={el.numero} value={el._id} key={el._id}>
            {`${el.numero}-${el.name}`}
          </option>
        );
      });
  };

  return (
    <div className={classes.main}>
      <Paper className={classes.paper}>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={openSnack}
          autoHideDuration={5000}
          onClose={handleCloseSnack}
        >
          <Alert onClose={handleCloseSnack} severity={snackData.severity}>
            {snackData.text}
          </Alert>
        </Snackbar>
        <form onSubmit={handleSubmit}>
          <FormGroup>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel htmlFor="concours">Concours (numéro + intitulé)</InputLabel>
              <Select
                native
                value={state.concours}
                onChange={handleChange}
                inputProps={{
                  name: 'concours',
                  id: 'concours',
                }}
              >
                <option aria-label="None" value="" />
                {renderConcours()}
              </Select>
            </FormControl>
            {renderTextfield()}
          </FormGroup>
          <div className={classes.validate}>
            <Button
              className={classes.button}
              variant="outlined"
              color="secondary"
              onClick={() => history.push('/admincommissions')}
            >
              Retour
            </Button>
            <div className={classes.spaceBetween} />
            {commission.name === undefined ? (
              <>
                <Button type="submit" variant="outlined" color="primary" className={classes.button}>
                  Soumettre et continuer
                </Button>
                <Button
                  type="button"
                  variant="outlined"
                  color="primary"
                  className={classes.button}
                  onClick={handleSubmit}
                >
                  Soumettre et retour
                </Button>
              </>
            ) : (
              <Button
                type="button"
                variant="outlined"
                color="primary"
                className={classes.button}
                onClick={handleSubmit}
              >
                Modifier
              </Button>
            )}
          </div>
        </form>
      </Paper>
    </div>
  );
}

SaisieCommission.propTypes = {
  commission: PropTypes.objectOf(PropTypes.element).isRequired,
  concours: PropTypes.arrayOf(PropTypes.element).isRequired,
};

export default withTracker(
  ({
    match: {
      params: { id },
    },
  }) => {
    let commission = {};
    if (id !== 'new') {
      Meteor.subscribe('commission.one', { commissionId: id });
      commission = Commission.findOne({ _id: id }) || {};
    }
    Meteor.subscribe('concours.all');
    const concours = Concours.find().fetch() || [];
    return {
      commission,
      concours,
    };
  },
)(SaisieCommission);
