/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */

import { assert } from 'meteor/practicalmeteor:chai';
import { Meteor } from 'meteor/meteor';
import faker from 'faker';
import { Factory } from 'meteor/dburles:factory';
import { Random } from 'meteor/random';
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';

import Log from '../log';
import { logBegin, logEnd, getLogs } from '../methods';
import Commission from '../../commission/commission';
import Concours from '../../concours/concours';
import Academie from '../../academie/academie';
import Centre from '../../centre/centre';
import List from '../../list/list';

describe('log', function () {
  describe('mutators', function () {
    it('builds correctly from factory', function () {
      const newLog = Factory.create('log');
      assert.typeOf(newLog, 'object');
    });
  });
  describe('methods', function () {
    let userId;
    let adminUserId;
    let concours;
    let commission;
    let academie;
    let centre;
    let candidat;
    let candidat2;
    let expectedData;
    beforeEach(function () {
      Meteor.users.remove({});
      Meteor.roles.remove({});
      Meteor.roleAssignment.remove({});
      Log.remove({});
      List.remove({});
      Commission.remove({});
      Concours.remove({});
      Centre.remove({});
      Academie.remove({});
      const email = faker.internet.email();
      adminUserId = Accounts.createUser({
        email: 'eole@ac-test.fr',
        username: 'eole',
        password: 'eole',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Roles.createRole('admin');
      Roles.addUsersToRoles(adminUserId, 'admin');
      Meteor.users.update(adminUserId, { $set: { isActive: true } });
      userId = Accounts.createUser({
        email,
        username: email,
        password: 'toto',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Meteor.users.update(userId, { $set: { isActive: true } });
      // create a candidate with associated data (concours, academie, ...)
      concours = Factory.create('concours');
      commission = Factory.create('commission', { concours: concours._id });
      academie = Factory.create('academie');
      centre = Factory.create('centre', { academie: academie._id });
      candidat = Factory.create('list', { centre: centre._id, commission: commission._id });
      candidat2 = Factory.create('list', { centre: centre._id, commission: commission._id });
      expectedData = {
        centre: centre.name,
        academie: academie.name,
        commission: commission.name,
        concoursName: concours.name,
        concoursNumero: concours.numero,
        numInscription: candidat.numInscription,
        candidat: candidat.candidat,
        isJury: true,
        dateEnd: null,
        domain: 'visio.libre-communaute.fr',
      };
    });
    describe('logBegin', function () {
      it('does create a log entry with startDate', function () {
        logBegin._execute({ userId }, { candidatId: candidat._id, isJury: true, domain: 'visio.libre-communaute.fr' });
        const newLog = Log.findOne(expectedData);
        assert.typeOf(newLog, 'object');
      });
      it('does not create a log entry if no user is connected', function () {
        assert.throws(
          () => {
            logBegin._execute({}, { candidatId: candidat._id, isJury: true, domain: 'visio.libre-communaute.fr' });
          },
          Meteor.Error,
          /api.log.logBegin.notPermitted/,
        );
      });
    });
    describe('logEnd', function () {
      it('does update a log entry endDate', function () {
        logBegin._execute({ userId }, { candidatId: candidat._id, isJury: true, domain: 'visio.libre-communaute.fr' });
        let newLog = Log.findOne(expectedData);
        assert.typeOf(newLog, 'object');
        logEnd._execute({ userId }, { logId: newLog._id });
        newLog = Log.findOne({ _id: newLog._id });
        assert.notEqual(newLog.dateEnd, null);
      });
      it('does not create a log entry if no user is connected', function () {
        assert.throws(
          () => {
            logEnd._execute({}, { logId: Random.id() });
          },
          Meteor.Error,
          /api.log.logEnd.notPermitted/,
        );
      });
    });
    describe('getLogs', function () {
      beforeEach(function () {
        const candidat3 = Factory.create('list', {
          candidat: 'totolharicot',
          centre: centre._id,
          commission: commission._id,
        });
        function sleep(milliseconds) {
          const date = Date.now();
          let currentDate = null;
          do {
            currentDate = Date.now();
          } while (currentDate - date < milliseconds);
        }
        const candidat4 = Factory.create('list', { centre: centre._id, commission: commission._id });
        // create log entries for all 4 candidates
        [candidat, candidat2, candidat3, candidat4].forEach((cand) => {
          // wait 50 ms to make sure log entries have different start date
          sleep(50);
          Factory.create('log', {
            centre: centre.name,
            academie: academie.name,
            numInscription: cand.numInscription,
            candidat: cand.candidat,
            dateStart: () => new Date(),
            dateEnd: () => new Date(),
            concoursName: concours.name,
            concoursNumero: concours.numero,
            commission: () => commission.name,
          });
        });
      });
      it('does return requested page of log entries with specified sort', function () {
        // request candidates with default params
        const res = getLogs._execute({ userId: adminUserId }, {});
        assert.equal(res.data.length, 4);
        assert.equal(res.totalCount, 4);
        assert.equal(res.data[0].dateStart > res.data[3].dateStart, true);
        // request candidates with reversed dateStart order
        const res2 = getLogs._execute({ userId: adminUserId }, { sort: { dateStart: 1 } });
        assert.equal(res2.data.length, 4);
        assert.equal(res2.totalCount, 4);
        // check (partially) that date order is reversed
        assert.equal(res2.data[0].dateStart < res2.data[3].dateStart, true);
        assert.equal(res2.data[0]._id, res.data[3]._id);
        assert.equal(res2.data[3]._id, res.data[0]._id);
      });
      it('does return requested page of log entries with specified pageSize', function () {
        // request candidates with pageSize of 3
        let res = getLogs._execute({ userId: adminUserId }, { pageSize: 3, page: 1 });
        assert.equal(res.data.length, 3);
        assert.equal(res.totalCount, 4);
        res = getLogs._execute({ userId: adminUserId }, { pageSize: 3, page: 2 });
        assert.equal(res.data.length, 1);
        assert.equal(res.totalCount, 4);
      });
      it('does return requested page of log entries with search filter', function () {
        // request logs for candidate 'totolharicot'
        const res = getLogs._execute({ userId: adminUserId }, { filter: 'totolharicot' });
        assert.equal(res.data.length, 1);
        assert.equal(res.totalCount, 1);
        assert.equal(res.data[0].candidat, 'totolharicot');
      });
      it('does not send logs if user is not admin', function () {
        assert.throws(
          () => {
            getLogs._execute({ userId }, { logId: Random.id() });
          },
          Meteor.Error,
          /api.log.getLogs.notPermitted/,
        );
      });
    });
  });
});
