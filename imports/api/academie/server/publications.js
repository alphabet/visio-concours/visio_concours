import { Meteor } from 'meteor/meteor';
import Academie from '../academie';

// publish all academie
Meteor.publish('academie.all', function academieAll() {
  if (!this.userId) {
    return this.ready();
  }
  return Academie.find({}, { sort: { name: 1 }, limit: 1000 });
});
