import { Migrations } from 'meteor/percolate:migrations';
import List from './list/list';

Migrations.add({
  version: 1,
  name: 'Add domain field to list',
  up: () => {
    List.update({ domain: null }, { $set: { domain: '' } }, { multi: true, removeEmptyStrings: false });
  },
  down: () => {
    List.rawCollection().updateMany({}, { $unset: { domain: true } });
  },
});
