# Run local visio app in container :

```
docker-compose build

./run.sh

docker-compose exec mongo /bin/bash

mongo meteor

db.users.insert({
  _id: '1',
  createdAt: new Date(),
  services: {
    password: {
      bcrypt: '$2a$10$w91o/mi5lRyuPdDKSUaZZOdcSXw1NfLRXY4LV7WajsqsVs5.OAw2u',
    },
  },
  username: 'eole',
});
```

Browse to localhost url with user 'eole' and password 'eole'.


# Run visio with the latest image from [docker-hub](https://hub.docker.com/repository/docker/eoleteam/visio) :

```
echo "METEOR_SETTINGS=$(cat settings-development.json | jq -c .)" > .env

docker-compose -f docker-compose-hub.yml up -d
```

# Run visio with the latest image from development :

```
sudo ./run.sh
```
