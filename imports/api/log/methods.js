import { Meteor } from 'meteor/meteor';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import { _ } from 'meteor/underscore';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';
import { Roles } from 'meteor/alanning:roles';

import Log from './log';
import List from '../list/list';
import Academie from '../academie/academie';
import Centre from '../centre/centre';
import Concours from '../concours/concours';
import Commission from '../commission/commission';
import RegEx from '../regExp';

export const logBegin = new ValidatedMethod({
  name: 'log.logBegin',
  validate: new SimpleSchema({
    candidatId: {
      type: String,
      regEx: RegEx.Id,
      label: 'Identifiant du candidat',
    },
    isJury: {
      type: Boolean,
      label: 'Connexion du jury',
    },
    // data: centre, academie, numInscription, candidat(name), concoursName, concoursNumero, commission(id), domain
    domain: {
      type: String,
      defaultValue: '',
      label: 'Adresse du serveur Jitsi',
    },
  }).validator(),
  run({ candidatId, isJury, domain }) {
    const authorized = !!this.userId;
    if (!authorized) {
      throw new Meteor.Error('api.log.logBegin.notPermitted', 'Connexion requise');
    }
    // check candidate existence
    const candidat = List.findOne(candidatId);
    if (candidat === undefined) {
      throw new Meteor.Error('api.log.logBegin.unknownCandidate', 'Identifiant de candidat inconnu');
    }
    const centre = Centre.findOne(candidat.centre);
    const academie = Academie.findOne(centre.academie);
    const commission = Commission.findOne(candidat.commission);
    const concours = Concours.findOne(commission.concours);
    return Log.insert({
      // _id: candidatId,
      centre: centre.name,
      academie: academie.name,
      numInscription: candidat.numInscription,
      candidat: candidat.candidat,
      concoursName: concours.name,
      concoursNumero: concours.numero,
      commission: commission.name,
      isJury,
      domain,
      dateStart: new Date(),
      dateEnd: null,
    });
  },
});

export const logEnd = new ValidatedMethod({
  name: 'log.logEnd',
  validate: new SimpleSchema({
    logId: {
      type: String,
      regEx: RegEx.Id,
      label: 'Identifiant du log',
    },
  }).validator(),
  run({ logId }) {
    const authorized = !!this.userId;
    if (!authorized) {
      throw new Meteor.Error('api.log.logEnd.notPermitted', 'Connexion requise');
    }
    // check candidate existence
    const log = Log.findOne(logId);
    if (log === undefined) {
      throw new Meteor.Error('api.log.logEnd.unkownEntry', 'log non retrouvé');
    }
    Log.update({ _id: logId }, { $set: { dateEnd: new Date() } });
  },
});

export const getLogs = new ValidatedMethod({
  name: 'log.getLogs',
  validate: new SimpleSchema({
    page: {
      type: SimpleSchema.Integer,
      min: 1,
      defaultValue: 1,
      optional: true,
      label: 'Numéro de page',
    },
    pageSize: {
      type: SimpleSchema.Integer,
      min: 1,
      defaultValue: 10,
      optional: true,
      label: 'Taille de la page',
    },
    filter: {
      type: String,
      defaultValue: '',
      optional: true,
      label: 'Filtre de recherche',
    },
    sort: {
      type: Object,
      blackbox: true,
      defaultValue: { dateStart: -1 },
      label: 'Options de tri',
    },
  }).validator({ clean: true }),
  run({ page, pageSize, filter, sort }) {
    if (!Roles.userIsInRole(this.userId, ['admin'])) {
      throw new Meteor.Error('api.log.getLogs.notPermitted', 'Vos droits sont insuffisants pour accéder à ces données');
    }
    // calculate number of entries to skip
    const skip = (page - 1) * pageSize;
    let query = {};
    if (filter && filter.length > 0) {
      query = { $and: [] };
      const words = filter.split(' ');
      words.forEach((word) => {
        if (word.length > 0) {
          query.$and.push({
            $or: [
              {
                candidat: { $regex: `.*${word}.*`, $options: 'i' },
              },
              {
                numInscription: { $regex: `.*${word}.*`, $options: 'i' },
              },
              {
                centre: { $regex: `.*${word}.*`, $options: 'i' },
              },
              {
                academie: { $regex: `.*${word}.*`, $options: 'i' },
              },
              {
                commission: { $regex: `.*${word}.*`, $options: 'i' },
              },
              {
                concoursName: { $regex: `.*${word}.*`, $options: 'i' },
              },
              {
                concoursNumero: { $regex: `.*${word}.*`, $options: 'i' },
              },
            ],
          });
        }
      });
    }
    const sortOpts = {};
    const allowedValues = [-1, 1];
    Object.keys(sort).forEach((key) => {
      // validate sort options
      if (Log.schema.objectKeys().includes(key) && allowedValues.includes(sort[key])) {
        sortOpts[key] = sort[key];
      }
    });
    const totalCount = Log.find(query).count();
    const data = Log.find(query, {
      fields: Log.publicFields,
      limit: pageSize,
      skip,
      sort: sortOpts,
    }).fetch();
    return { data, page, totalCount };
  },
});

// Get list of all method names on User
const LISTS_METHODS = _.pluck([logBegin, logEnd, getLogs], 'name');

if (Meteor.isServer) {
  // Only allow 5 list operations per connection per second
  DDPRateLimiter.addRule(
    {
      name(name) {
        return _.contains(LISTS_METHODS, name);
      },

      // Rate limit per connection ID
      connectionId() {
        return true;
      },
    },
    5,
    1000,
  );
}
