/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */

import { PublicationCollector } from 'meteor/johanbrook:publication-collector';
import { chai, assert } from 'meteor/practicalmeteor:chai';
import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import { _ } from 'meteor/underscore';
import faker from 'faker';
import { Factory } from 'meteor/dburles:factory';
import { Roles } from 'meteor/alanning:roles';
import { Accounts } from 'meteor/accounts-base';
import './publications';

import Concours from '../concours';
import { createConcours, updateConcours, removeConcours, purgeConcours } from '../methods';

import Academie from '../../academie/academie';
import Centre from '../../centre/centre';
import Commission from '../../commission/commission';
import List from '../../list/list';

describe('concours', function () {
  describe('mutators', function () {
    it('builds correctly from factory', function () {
      const newConcours = Factory.create('concours');
      assert.typeOf(newConcours, 'object');
    });
  });
  describe('publications', function () {
    let userId;
    before(function () {
      Meteor.users.remove({});
      userId = Accounts.createUser({
        username: faker.internet.email(),
        password: 'toto',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Meteor.users.update(userId, { $set: { isActive: true } });
      Concours.remove({});
      _.times(4, () => {
        Factory.create('concours');
      });
    });
    describe('concours.all', function () {
      it('sends all concours', function (done) {
        const collector = new PublicationCollector({ userId });
        collector.collect('concours.all', (collections) => {
          chai.assert.equal(collections.concours.length, 4);
          done();
        });
      });
    });
  });
  describe('methods', function () {
    let userId;
    let saisieUserId;
    let adminUserId;
    beforeEach(function () {
      Meteor.users.remove({});
      Meteor.roles.remove({});
      Meteor.roleAssignment.remove({});
      Concours.remove({});
      const email = faker.internet.email();
      const email2 = faker.internet.email();
      adminUserId = Accounts.createUser({
        email: 'eole@ac-test.fr',
        username: 'eole',
        password: 'eole',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Roles.createRole('admin');
      Roles.addUsersToRoles(adminUserId, 'admin');
      Meteor.users.update(adminUserId, { $set: { isActive: true } });

      saisieUserId = Accounts.createUser({
        email2,
        username: email2,
        password: 'toto',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Roles.createRole('saisie');
      Roles.addUsersToRoles(saisieUserId, 'saisie');
      Meteor.users.update(saisieUserId, { $set: { isActive: true } });

      userId = Accounts.createUser({
        email,
        username: email,
        password: 'toto',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Meteor.users.update(userId, { $set: { isActive: true } });
    });
    describe('createConcours', function () {
      it('does user with admin role create a concours', function () {
        createConcours._execute({ userId: adminUserId }, { name: 'monConc', numero: '007' });
        const reqConc = Concours.findOne({ name: 'monConc' });
        assert.typeOf(reqConc, 'object');
      });
      it('does user with saisie role create a concours', function () {
        createConcours._execute({ userId: saisieUserId }, { name: 'monConc2', numero: '008' });
        const reqConc = Concours.findOne({ name: 'monConc2' });
        assert.typeOf(reqConc, 'object');
      });
      it('does not create a concours if user has no role', function () {
        assert.throws(
          () => {
            createConcours._execute({ userId }, { name: 'monConc3', numero: '009' });
          },
          Meteor.Error,
          /api.concours.createConcours.notPermitted/,
        );
      });
      it('does not create a concours if no user is connected', function () {
        assert.throws(
          () => {
            createConcours._execute({}, { name: 'monConc4', numero: '010' });
          },
          Meteor.Error,
          /api.concours.createConcours.notPermitted/,
        );
      });
      it('does not create a duplicate concours', function () {
        createConcours._execute({ userId: adminUserId }, { name: 'monConc', numero: '007' });
        assert.throws(
          () => {
            createConcours._execute({ userId: adminUserId }, { name: 'monConc', numero: '007' });
          },
          Meteor.Error,
          /api.concours.createConcours.duplicateEntry/,
        );
      });
      it('does not create a concours with inconsistent dates', function () {
        assert.throws(
          () => {
            createConcours._execute(
              { userId: adminUserId },
              { name: 'monConc', numero: '007', start: new Date('2021-01-01'), end: new Date('2020-01-01') },
            );
          },
          Meteor.Error,
          /api.concours.createConcours.dateMismatch/,
        );
      });
    });
    describe('updateConcours', function () {
      it('does user with admin role update a concours', function () {
        const concId = Factory.create('concours', { name: 'monConc' })._id;
        updateConcours._execute(
          { userId: adminUserId },
          { concoursId: concId, data: { name: 'updatedConc', numero: '007' } },
        );
        const reqConc = Concours.findOne({ _id: concId });
        assert.typeOf(reqConc, 'object');
        assert.equal(reqConc.name, 'updatedConc');
      });
      it('does user with saisie role update a concours', function () {
        const concId = Factory.create('concours', { name: 'monConc' })._id;
        updateConcours._execute(
          { userId: saisieUserId },
          { concoursId: concId, data: { name: 'updatedConc2', numero: '008' } },
        );
        const reqConc = Concours.findOne({ _id: concId });
        assert.typeOf(reqConc, 'object');
        assert.equal(reqConc.name, 'updatedConc2');
      });
      it('does not update a concours if user has no role', function () {
        const concId = Factory.create('concours', { name: 'monConc' })._id;
        assert.throws(
          () => {
            updateConcours._execute({ userId }, { concoursId: concId, data: { name: 'updatedConc3', numero: '009' } });
          },
          Meteor.Error,
          /api.concours.updateConcours.notPermitted/,
        );
      });
      it('does not update a concours if no user is connected', function () {
        const concId = Factory.create('concours', { name: 'monConc' })._id;
        assert.throws(
          () => {
            updateConcours._execute({}, { concoursId: concId, data: { name: 'updatedConc4', numero: '010' } });
          },
          Meteor.Error,
          /api.concours.updateConcours.notPermitted/,
        );
      });
      it('does not update a not found concours', function () {
        assert.throws(
          () => {
            updateConcours._execute(
              { userId: adminUserId },
              { concoursId: Random.id(), data: { name: 'noConc', numero: '0' } },
            );
          },
          Meteor.Error,
          /api.concours.updateConcours.unknownConcours/,
        );
      });
      it('does not update to an existing concours', function () {
        Factory.create('concours', { name: 'existingConc' });
        const concId = Factory.create('concours', { name: 'monConc' })._id;
        assert.throws(
          () => {
            updateConcours._execute(
              { userId: adminUserId },
              { concoursId: concId, data: { name: 'existingConc', numero: '0' } },
            );
          },
          Meteor.Error,
          /api.concours.updateConcours.duplicateEntry/,
        );
      });
      it('does not update a concours with inconsistent dates', function () {
        const concId = Factory.create('concours', { name: 'monConc' })._id;
        assert.throws(
          () => {
            updateConcours._execute(
              { userId: adminUserId },
              {
                concoursId: concId,
                data: { name: 'monConc', numero: '007', start: new Date('2021-01-01'), end: new Date('2020-01-01') },
              },
            );
          },
          Meteor.Error,
          /api.concours.updateConcours.dateMismatch/,
        );
      });
    });
    describe('removeConcours', function () {
      it('does user with admin role remove a concours', function () {
        const concId = Factory.create('concours', { name: 'monConc' })._id;
        assert.typeOf(Concours.findOne({ _id: concId }), 'object');
        removeConcours._execute({ userId: adminUserId }, { concoursId: concId });
        assert.equal(Concours.findOne({ _id: concId }), undefined);
      });
      it('does user with saisie role remove a concours', function () {
        const concId = Factory.create('concours', { name: 'monConc' })._id;
        assert.typeOf(Concours.findOne({ _id: concId }), 'object');
        removeConcours._execute({ userId: saisieUserId }, { concoursId: concId });
        assert.equal(Concours.findOne({ _id: concId }), undefined);
      });
      it('does not remove a concours if user has no role', function () {
        const concId = Factory.create('concours', { name: 'monConc' })._id;
        assert.typeOf(Concours.findOne({ _id: concId }), 'object');
        assert.throws(
          () => {
            removeConcours._execute({ userId }, { concoursId: concId });
          },
          Meteor.Error,
          /api.concours.removeConcours.notPermitted/,
        );
      });
      it('does not remove a concours if no user is connected', function () {
        const concId = Factory.create('concours', { name: 'monConc' })._id;
        assert.typeOf(Concours.findOne({ _id: concId }), 'object');
        assert.throws(
          () => {
            removeConcours._execute({}, { concoursId: concId });
          },
          Meteor.Error,
          /api.concours.removeConcours.notPermitted/,
        );
      });
      it('does not remove a not found concours', function () {
        const concId = Factory.create('concours', { name: 'monConc' })._id;
        assert.typeOf(Concours.findOne({ _id: concId }), 'object');
        assert.throws(
          () => {
            removeConcours._execute({ userId: adminUserId }, { concoursId: Random.id() });
          },
          Meteor.Error,
          /api.concours.removeConcours.unknownConcours/,
        );
      });
      it('does not remove a concours which still has centre', function () {
        const concId = Factory.create('concours', { name: 'monConc' })._id;
        assert.typeOf(Concours.findOne({ _id: concId }), 'object');
        Factory.create('commission', { concours: concId });
        assert.throws(
          () => {
            removeConcours._execute({ userId: adminUserId }, { concoursId: concId });
          },
          Meteor.Error,
          /api.concours.removeConcours.relatedDataStillExist/,
        );
      });
    });
    describe('purgeConcours', function () {
      it('does user with admin role purge a list of concours with all commissions and candidats related', function () {
        const acadId = Factory.create('academie')._id;
        assert.typeOf(Academie.findOne({ _id: acadId }), 'object');
        const centId = Factory.create('centre', { academie: acadId })._id;
        assert.typeOf(Centre.findOne({ _id: centId }), 'object');

        const concId0 = Factory.create('concours')._id;
        assert.typeOf(Concours.findOne({ _id: concId0 }), 'object');
        const commId0 = Factory.create('commission', { concours: concId0 })._id;
        assert.typeOf(Commission.findOne({ _id: commId0 }), 'object');

        const concId1 = Factory.create('concours')._id;
        assert.typeOf(Concours.findOne({ _id: concId1 }), 'object');
        const commId1 = Factory.create('commission', { concours: concId1 })._id;
        assert.typeOf(Commission.findOne({ _id: commId1 }), 'object');

        const concId2 = Factory.create('concours')._id;
        assert.typeOf(Concours.findOne({ _id: concId2 }), 'object');
        const commId2 = Factory.create('commission', { concours: concId2 })._id;
        assert.typeOf(Commission.findOne({ _id: commId2 }), 'object');

        const candidId0 = Factory.create('list')._id;
        assert.typeOf(List.findOne({ _id: candidId0 }), 'object');
        const candidId1 = Factory.create('list', { centre: centId, commission: commId1 })._id;
        assert.typeOf(List.findOne({ _id: candidId1 }), 'object');
        const candidId2 = Factory.create('list', { centre: centId, commission: commId1 })._id;
        assert.typeOf(List.findOne({ _id: candidId2 }), 'object');
        const candidId3 = Factory.create('list', { centre: centId, commission: commId2 })._id;
        assert.typeOf(List.findOne({ _id: candidId3 }), 'object');
        const candidId4 = Factory.create('list', { centre: centId, commission: commId2 })._id;
        assert.typeOf(List.findOne({ _id: candidId4 }), 'object');

        purgeConcours._execute({ userId: adminUserId }, { concoursIdList: [concId1, concId2] });

        assert.typeOf(Academie.findOne({ _id: acadId }), 'object');
        assert.typeOf(Centre.findOne({ _id: centId }), 'object');

        assert.typeOf(Concours.findOne({ _id: concId0 }), 'object');
        assert.typeOf(Commission.findOne({ _id: commId0 }), 'object');
        assert.typeOf(List.findOne({ _id: candidId0 }), 'object');

        assert.equal(Concours.findOne({ _id: concId1 }), undefined);
        assert.equal(Concours.findOne({ _id: concId2 }), undefined);

        assert.equal(Commission.findOne({ _id: commId1 }), undefined);
        assert.equal(Commission.findOne({ _id: commId2 }), undefined);

        assert.equal(List.findOne({ _id: candidId1 }), undefined);
        assert.equal(List.findOne({ _id: candidId2 }), undefined);
        assert.equal(List.findOne({ _id: candidId3 }), undefined);
        assert.equal(List.findOne({ _id: candidId4 }), undefined);
      });
      it('does not purge a concours if user has saisie role', function () {
        const concId = Factory.create('concours', { name: 'monConc' })._id;
        assert.throws(
          () => {
            purgeConcours._execute({ userId: saisieUserId }, { concoursIdList: [concId] });
          },
          Meteor.Error,
          /api.concours.purgeConcours.notPermitted/,
        );
      });
      it('does not purge a concours if user has no role', function () {
        const concId = Factory.create('concours', { name: 'monConc' })._id;
        assert.throws(
          () => {
            purgeConcours._execute({ userId }, { concoursIdList: [concId] });
          },
          Meteor.Error,
          /api.concours.purgeConcours.notPermitted/,
        );
      });
      it('does not purge a concours if no user is connected', function () {
        const concId = Factory.create('concours', { name: 'monConc' })._id;
        assert.throws(
          () => {
            purgeConcours._execute({}, { concoursIdList: [concId] });
          },
          Meteor.Error,
          /api.concours.purgeConcours.notPermitted/,
        );
      });
    });
  });
});
