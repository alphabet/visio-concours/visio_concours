import React from 'react';
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';
import { render } from 'react-dom';
import { Accounts } from 'meteor/accounts-base';

import { MuiThemeProvider } from '@material-ui/core';
import { createTheme } from '@material-ui/core/styles';
import purple from '@material-ui/core/colors/purple';
import App from '../../ui/layouts/app';

import { registerSchemaMessages } from '../../api/utils';

registerSchemaMessages();

const theme = createTheme({
  palette: {
    primary: {
      main: '#0097a7',
    },
    secondary: {
      main: '#f50057',
      purple: purple.A400,
    },
  },
});

Meteor.startup(() => {
  render(
    <MuiThemeProvider theme={theme}>
      <App />
    </MuiThemeProvider>,
    document.getElementById('root'),
  );
});

Accounts.onLogout(() => {
  // cleanup session on logout (codes_jury)
  Object.keys(Session.keys).forEach(function cleanupSession(key) {
    Session.set(key, undefined);
  });
  Session.keys = {};
});
