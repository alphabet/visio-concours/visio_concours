import React, { useState, useEffect } from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { useHistory } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Snackbar from '@material-ui/core/Snackbar';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import { Typography } from '@material-ui/core';

validate.options = {
  fullMessages: false,
};

const styles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%', // Fix IE 11 issue.
  },
  titleBackgroud: {
    background: '#99ddff',
    borderRadius: '3px 3px 0 0',
    marginBottom: theme.spacing(2),
  },
  title: {
    padding: theme.spacing(3),
  },
  form: {
    backgroundColor: 'white',
    textAlign: 'center',
    marginTop: theme.spacing(10),
    margin: 'auto',
    width: 300,
  },
  button: {
    margin: theme.spacing(3),
    width: 250,
  },
  submit: {
    margin: theme.spacing(3),
    width: 200,
  },
}));

const schema = {
  user: {
    presence: { allowEmpty: false, message: "Nom d'utilisateur requis" },
    length: {
      maximum: 64,
    },
  },
  password: {
    presence: { allowEmpty: false, message: 'Mot de passe requis' },
    length: {
      maximum: 64,
    },
  },
};

function SignIn({ loggingIn }) {
  const classes = styles();
  const history = useHistory();

  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    errors: {},
  });

  const [errMsg, setErrMsg] = useState('Erreur de connexion');

  useEffect(() => {
    const errors = validate(formState.values, schema);

    setFormState(() => ({
      ...formState,
      isValid: !errors,
      errors: errors || {},
    }));
  }, [formState.values]);

  const [openError, setOpenError] = useState(false);

  const handleChange = (event) => {
    event.persist();

    setFormState(() => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]: event.target.value,
      },
    }));
  };

  const handleSignIn = (event) => {
    event.preventDefault();
    if (formState.isValid === true) {
      const { user, password } = formState.values;
      Meteor.loginWithPassword(user, password, (err) => {
        if (err) {
          if (err.reason === 'Incorrect password') {
            setErrMsg('Mot de passe incorrect');
          } else if (err.reason === 'User not found') {
            setErrMsg('Utilisateur inconnu');
          } else {
            setErrMsg(err.reason);
          }
          setOpenError(true);
        } else {
          history.push('/home');
        }
      });
    }
  };

  const handleKeycloakAuth = () => {
    Meteor.loginWithKeycloak();
  };

  const handleErrorClose = (reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenError(false);
  };

  const hasError = (field) => !!formState.errors[field];
  const useKeycloak = Meteor.settings.public.enableKeycloak;
  return useKeycloak && loggingIn ? (
    <>{history.push('/home')}</>
  ) : (
    <Grid container component="main" className={classes.container}>
      <Grid item xs={4} sm={8} md={12} elevation={6}>
        <form onSubmit={handleSignIn} className={classes.form}>
          <div className={classes.titleBackgroud}>
            <Typography className={classes.title} variant="h5">
              Visio Concours
            </Typography>
          </div>
          {useKeycloak ? (
            <Button
              disabled={loggingIn}
              fullWidth
              variant="outlined"
              color="primary"
              className={classes.submit}
              onClick={handleKeycloakAuth}
            >
              connexion
            </Button>
          ) : (
            <>
              <TextField
                className={classes.button}
                autoFocus
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="user"
                label="Identifiant"
                type="text"
                id="user"
                autoComplete="current-user"
                error={hasError('user')}
                helperText={hasError('user') ? formState.errors.user[0] : null}
                onChange={handleChange}
                value={formState.values.user || ''}
                disabled={loggingIn}
              />
              <TextField
                className={classes.button}
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Mot de passe"
                type="password"
                id="password"
                autoComplete="current-password"
                error={hasError('password')}
                helperText={hasError('password') ? formState.errors.password[0] : null}
                onChange={handleChange}
                value={formState.values.password || ''}
                disabled={loggingIn}
              />
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                disabled={!formState.isValid || loggingIn}
              >
                Connexion
              </Button>
            </>
          )}
        </form>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={openError}
          autoHideDuration={4000}
          onClose={handleErrorClose}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={errMsg}
        />
      </Grid>
    </Grid>
  );
}

export default withTracker(() => {
  const loggingIn = Meteor.loggingIn();
  return {
    loggingIn,
  };
})(SignIn);

SignIn.defaultProps = {
  loggingIn: false,
};

SignIn.propTypes = {
  loggingIn: PropTypes.bool,
};
