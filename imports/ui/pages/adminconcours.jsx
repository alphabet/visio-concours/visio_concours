import React, { forwardRef, useState } from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { withTracker } from 'meteor/react-meteor-data';

import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import MaterialTable from 'material-table';
import ControlPointIcon from '@material-ui/icons/ControlPoint';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import MuiAlert from '@material-ui/lab/Alert';
import { Snackbar } from '@material-ui/core';
import Concours from '../../api/concours/concours';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
  titleBackgroud: {
    background: '#99ddff',
    borderRadius: '3px 3px 0 0',
    display: 'flex',
    justifyContent: 'space-between',
  },
  title: {
    margin: theme.spacing(3),
    fontSize: '120%',
    alignContent: 'flex-start',
  },
  titleRight: {
    alignContent: 'flex-end',
  },
  button: { width: 200, margin: theme.spacing(1) },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const AdminConcours = ({ concours }) => {
  const history = useHistory();
  const classes = useStyles();
  const [openSnack, setOpenSnack] = useState(false);
  const [snackData, setSnackData] = useState({
    text: '',
    severity: 'success',
  });

  const showAlert = (text, severity = 'success') => {
    setSnackData({ text, severity });
    setOpenSnack(true);
  };

  const handleCloseSnack = (reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnack(false);
  };

  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  const localization = {
    toolbar: {
      searchPlaceholder: 'Rechercher un concours',
      searchTooltip: 'Rechercher un concours',
    },
    pagination: {
      labelRowsSelect: 'Concours par page',
      firstAriaLabel: 'Première page',
      firstTooltip: 'Première page',
      previousAriaLabel: 'Page précédente',
      previousTooltip: 'Page précédente',
      nextAriaLabel: 'Page suivante',
      nextTooltip: 'Page suivante',
      lastAriaLabel: 'Dernière page',
      lastTooltip: 'Dernière page',
    },
    body: {
      emptyDataSourceMessage: 'Aucun concours trouvé',
      addTooltip: 'Ajouter un concours',
      editTooltip: 'Modifier ce concours',
      deleteTooltip: 'Supprimer ce concours',
      editRow: {
        deleteText: 'Supprimer vraiment ce concours ?',
        cancelTooltip: 'Annuler',
        saveTooltip: 'Enregistrer les changements',
      },
    },
  };

  const options = {
    pageSize: 10,
    pageSizeOptions: [10, 20, 50, 100],
    paginationType: 'stepped',
    addRowPosition: 'first',
    emptyRowsWhenPaging: false,
    actionsColumnIndex: 7,
  };

  const actions = [
    {
      icon: ControlPointIcon,
      isFreeAction: true,
      tooltip: 'Ajouter un concours',
      onClick: () => history.push('/adminconcours/new'),
    },
    {
      icon: Edit,
      tooltip: 'Modifier ce concours',
      onClick: (event, rowData) => {
        history.push(`/adminconcours/${rowData._id}`);
      },
    },
  ];

  return (
    <Paper className={classes.root}>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={openSnack}
        autoHideDuration={5000}
        onClose={handleCloseSnack}
      >
        <Alert onClose={handleCloseSnack} severity={snackData.severity}>
          {snackData.text}
        </Alert>
      </Snackbar>
      <div className={classes.titleBackgroud}>
        <p className={classes.title}>Liste des concours</p>
        <div className={classes.titleRight} />
        <Button onClick={() => history.push('/home')} className={classes.button} variant="outlined" color="secondary">
          Retour
        </Button>
      </div>

      <MaterialTable
        columns={[
          { title: 'N° Concours', field: 'numero', defaultSort: 'asc' },
          { title: 'Concours', field: 'name' },
          { title: 'Début du Concours', field: 'start', type: 'date' },
          { title: 'Fin du Concours', field: 'end', type: 'date' },
        ]}
        data={concours}
        title=""
        icons={tableIcons}
        localization={localization}
        options={options}
        actions={actions}
        editable={{
          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              Meteor.call(
                'concours.removeConcours',
                {
                  concoursId: oldData._id,
                },
                (err, res) => {
                  if (err) {
                    showAlert(err.reason, 'error');
                    // reject(err);
                    resolve(); // close the confirmation message
                  } else {
                    resolve(res);
                  }
                },
              );
            }),
        }}
      />
    </Paper>
  );
};

AdminConcours.propTypes = {
  concours: PropTypes.arrayOf(PropTypes.element).isRequired,
};

export default withTracker(() => {
  Meteor.subscribe('concours.all');
  const concours = Concours.find().fetch() || [];
  return {
    concours,
  };
})(AdminConcours);
