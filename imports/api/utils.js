import RegEx from './regExp';

export function registerSchemaMessages() {
  const dataTypes = {
    String: 'Chaine de caractères',
    Number: 'Numérique',
    Integer: 'Entier',
    Boolean: 'Booléen',
    Array: 'Tableau',
    Object: 'Objet',
  };
  const regExpMessages = [
    {
      exp: RegEx.Email,
      msg: 'doit être une adresse e-mail valide',
    },
    {
      exp: RegEx.EmailWithTLD,
      msg: 'doit être une adresse e-mail valide',
    },
    { exp: RegEx.Domain, msg: 'doit être un domaine valide' },
    {
      exp: RegEx.WeakDomain,
      msg: 'doit être un domaine valide',
    },
    {
      exp: RegEx.IP,
      msg: 'doit être une adresse IPv4/IPv6 valide',
    },
    { exp: RegEx.IPv4, msg: 'doit être une adresse IPv4 valide' },
    { exp: RegEx.IPv6, msg: 'doit être une adresse IPv6 valide' },
    { exp: RegEx.Url, msg: 'doit être une URL valide' },
    {
      exp: RegEx.Id,
      msg: 'doit être un identifiant alphanumérique valide',
    },
    { exp: RegEx.ZipCode, msg: 'doit être un code postal valide' },
    {
      exp: RegEx.Phone,
      msg: 'doit être un numéro de téléphone valide',
    },
  ];
  const customMessages = {
    required: (_, label) => {
      return `${label} est requis`;
    },
    minString: (ctx, label) => {
      return `${label} doit faire au moins ${ctx.min} caractères`;
    },
    maxString: (ctx, label) => {
      return `${label} est limité à ${ctx.max} caractères`;
    },
    minNumber: (ctx, label) => {
      return `${label} doit être supérieur ou égal à ${ctx.min}`;
    },
    maxNumber: (ctx, label) => {
      return `${label} doit être inférieur ou égal à ${ctx.max}`;
    },
    minNumberExclusive: (ctx, label) => {
      return `${label} doit être supérieur à ${ctx.min}`;
    },
    maxNumberExclusive: (ctx, label) => {
      return `${label} doit être inférieur à ${ctx.max}`;
    },
    minDate: (ctx, label) => {
      return `${label} doit être postérieur à ${ctx.min}`;
    },
    maxDate: (ctx, label) => {
      return `${label} doit être antérieur à ${ctx.max}`;
    },
    badDate: (_, label) => {
      return `${label} n'est pas une tâche valide`;
    },
    minCount: (ctx) => {
      return `Vous devez saisir plus de ${ctx.minCount} valeurs`;
    },
    maxCount: (ctx) => {
      return `Vous devez saisir moins de ${ctx.maxCount} valeurs`;
    },
    noDecimal: (_, label) => {
      return `${label} doit être un entier`;
    },
    notAllowed: (ctx) => {
      return `${ctx.value} n'est pas une valeur acceptée`;
    },
    keyNotInSchema: (ctx) => {
      return `${ctx.name} n'est pas autorisé par le schéma`;
    },
    expectedType: (ctx, label) => {
      const finalCtx = { ...ctx, label };
      const typeTranslated = dataTypes[finalCtx.dataType] || finalCtx.dataType;
      return `${finalCtx.label} doit être du type ${typeTranslated}`;
    },
    regEx({ ctx, label }) {
      // See if there's one where exp matches this expression
      let msgObj;
      if (ctx.regExp) {
        msgObj = regExpMessages.find((o) => o.exp && o.exp.toString() === ctx.regExp);
      }
      const regExpMessage = msgObj ? msgObj.msg : "Echec de validation de l'expression régulière";
      return `${label} ${regExpMessage}`;
    },
  };
  globalThis.simpleSchemaGlobalConfig = {
    getErrorMessage(error, label) {
      if (typeof customMessages[error.type] === 'function') {
        return customMessages[error.type](error, label);
      }
      return undefined;
    },
  };
}

// checks if the domain part of an email address matches whitelisted domains
export function checkDomain(email) {
  let res = false;
  const domainMail = email.split('@')[1];
  const whiteDomains = Meteor.settings.private.whiteDomains || [];
  whiteDomains.forEach((whiteDomain) => {
    if (new RegExp(whiteDomain).test(domainMail)) {
      res = true;
    }
  });
  return res;
}
