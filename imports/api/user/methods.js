import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import { _ } from 'meteor/underscore';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';
import { checkDomain } from '../utils';
import AppRoles from './user';
import RegEx from '../regExp';

export const addUser = new ValidatedMethod({
  name: 'user.addUser',
  validate: new SimpleSchema({
    email: {
      type: String,
      regEx: RegEx.Email,
      label: 'Adresse électronique',
    },
    password: {
      type: String,
      label: 'Mot de passe',
    },
  }).validator({ clean: true }),
  run({ email, password }) {
    // uncomment if only admin can create users
    // if (!Roles.userIsInRole(this.userId, 'admin')) {
    //   throw new Meteor.Error(
    //     'api.user.addUser.notPermitted',
    //     'Cette action est réservée aux administrateurs'
    //   );
    // }

    // check email against domain whitelist if configured
    if (Meteor.settings.private && Meteor.settings.private.whiteDomains.length > 1) {
      if (checkDomain(email.toLowerCase()) === false) {
        throw new Meteor.Error(
          'api.user.addUser.unauthorizedEmail',
          "Le domaine de l'adresse électronique n'est pas autorisé",
        );
      }
    }
    Accounts.createUser({
      username: email,
      email,
      password,
    });
  },
});

export const removeUser = new ValidatedMethod({
  name: 'user.removeUser',
  validate: new SimpleSchema({
    userId: {
      type: String,
      label: "Id de l'utilisateur",
    },
  }).validator({ clean: true }),
  run({ userId }) {
    if (!Roles.userIsInRole(this.userId, 'admin')) {
      throw new Meteor.Error('api.user.removeUser.notPermitted', 'Cette action est réservée aux administrateurs');
    }
    const user = Meteor.users.findOne(userId);
    if (user === undefined) {
      throw new Meteor.Error('api.user.setActive.unknownUser', "Nom d'utilisateur inconnu");
    }
    // Do not allow to delete admin users
    if (Meteor.settings.private.adminUsers.includes(user.username)) {
      throw new Meteor.Error(
        'api.user.removeUser.cannotRemoveAdmin',
        'Les administrateurs ne peuvent pas être supprimés',
      );
    }
    Meteor.users.remove({ _id: userId });
  },
});

export const setActive = new ValidatedMethod({
  name: 'user.setActive',
  validate: new SimpleSchema({
    userId: {
      type: String,
      label: "Id de l'utilisateur",
    },
    active: {
      type: Boolean,
      defaultValue: true,
      label: "état de l'activation (booléen)",
    },
  }).validator({ clean: true }),
  run({ userId, active }) {
    if (!Roles.userIsInRole(this.userId, 'admin')) {
      throw new Meteor.Error('api.user.setActive.notPermitted', 'Cette action est réservée aux administrateurs');
    }
    const user = Meteor.users.findOne(userId);
    if (user === undefined) {
      throw new Meteor.Error('api.user.setActive.unknownUser', "Nom d'utilisateur inconnu");
    }
    // Do not allow to disable admin users
    if (Meteor.settings.private.adminUsers.includes(user.username) && active === false) {
      throw new Meteor.Error(
        'api.user.setActive.cannotDisableAdmin',
        'Les administrateurs ne peuvent pas être désactivés',
      );
    }
    Meteor.users.update(userId, { $set: { isActive: active } });
  },
});

export const addRole = new ValidatedMethod({
  name: 'user.addRole',
  validate: new SimpleSchema({
    userId: {
      type: String,
      label: "Id de l'utilisateur",
    },
    role: {
      type: String,
      allowedValues: AppRoles,
      label: 'nom du rôle',
    },
  }).validator(),
  run({ userId, role }) {
    if (!Roles.userIsInRole(this.userId, 'admin')) {
      throw new Meteor.Error('api.user.addRole.notPermitted', 'Cette action est réservée aux administrateurs');
    }
    const user = Meteor.users.findOne(userId);
    if (user === undefined) {
      throw new Meteor.Error('api.user.addRole.unknownUser', "Nom d'utilisateur inconnu");
    }
    if (!Roles.userIsInRole(userId, role)) {
      Roles.addUsersToRoles(userId, role);
    }
  },
});

export const removeRole = new ValidatedMethod({
  name: 'user.removeRole',
  validate: new SimpleSchema({
    userId: {
      type: String,
      label: "Id de l'utilisateur",
    },
    role: {
      type: String,
      allowedValues: AppRoles,
      label: 'nom du rôle',
    },
  }).validator(),
  run({ userId, role }) {
    if (!Roles.userIsInRole(this.userId, 'admin')) {
      throw new Meteor.Error('api.user.removeRole.notPermitted', 'Cette action est réservée aux administrateurs');
    }
    const user = Meteor.users.findOne(userId);
    if (user === undefined) {
      throw new Meteor.Error('api.user.removeRole.unknownUser', "Nom d'utilisateur inconnu");
    }
    if (Roles.userIsInRole(userId, role)) {
      Roles.removeUsersFromRoles(userId, role);
    }
  },
});

// Get list of all method names on User
const LISTS_METHODS = _.pluck([addUser, setActive, addRole, removeRole, removeUser], 'name');

if (Meteor.isServer) {
  // Only allow 5 list operations per connection per second
  DDPRateLimiter.addRule(
    {
      name(name) {
        return _.contains(LISTS_METHODS, name);
      },

      // Rate limit per connection ID
      connectionId() {
        return true;
      },
    },
    5,
    1000,
  );
}
