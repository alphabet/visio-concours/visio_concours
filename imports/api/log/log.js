import SimpleSchema from 'simpl-schema';
import { Random } from 'meteor/random';
import { Factory } from 'meteor/dburles:factory';

const Log = new Mongo.Collection('log');

// Deny all client-side updates since we will be using methods to manage this collection
Log.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

Log.schema = new SimpleSchema({
  centre: {
    type: String,
    min: 1,
    label: "nom du centre d'accueil",
  },
  academie: {
    type: String,
    min: 1,
    label: "Intitulé de l'académie",
  },
  numInscription: {
    type: String,
    min: 1,
    label: "Numéro d'inscription du candidat",
  },
  candidat: {
    type: String,
    min: 1,
    label: 'Nom du candidat',
  },
  isJury: {
    type: Boolean,
    label: 'Connexion du jury',
  },
  dateStart: {
    type: Date,
    label: 'Date de début',
    defaultValue: new Date(),
  },
  dateEnd: {
    type: Date,
    optional: true,
    label: 'Date de fin',
    defaultValue: null,
  },
  concoursName: {
    type: String,
    min: 1,
    label: 'Intitulé du concours',
  },
  concoursNumero: {
    type: String,
    min: 1,
    label: 'Numéro du concours',
  },
  commission: {
    type: String,
    min: 1,
    label: 'Intitulé de la commission',
  },
  domain: {
    type: String,
    min: 1,
    label: 'Adresse du serveur Jitsi',
  },
});

Log.attachSchema(Log.schema);

Log.publicFields = {
  _id: 1,
  centre: 1,
  academie: 1,
  numInscription: 1,
  candidat: 1,
  isJury: 1,
  dateStart: 1,
  dateEnd: 1,
  concoursName: 1,
  concoursNumero: 1,
  commission: 1,
  domain: 1,
};

Factory.define('log', Log, {
  centre: () => Random.id(),
  academie: 'Dijon',
  numInscription: () => Random.id(),
  candidat: () => `Bob_${Random.id()}`,
  dateStart: () => new Date(),
  dateEnd: () => new Date(),
  concoursName: 'concours 1',
  concoursNumero: () => Random.id(),
  commission: () => Random.id(),
  domain: 'meet.jit.si',
  isJury: false,
});

export default Log;
