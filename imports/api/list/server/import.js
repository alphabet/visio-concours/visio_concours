/* eslint-disable no-console, func-names, camelcase */
import { Meteor } from 'meteor/meteor';
import { moment } from 'meteor/momentjs:moment';
import SimpleSchema from 'simpl-schema';
import Academie from '../../academie/academie';
import Concours from '../../concours/concours';
import Centre from '../../centre/centre';
import Commission from '../../commission/commission';
import List from '../list';

// Validation d'une ligne du fichier d'import
const validationContext = new SimpleSchema({
  NumeroCandidat: String, // candidat : numInscription partie 1
  Nom: String, // candidat : candidat
  Prenom: String, // candidat : candidat
  Commission: String, // commission : name
  CleCommission: String, // commission : numCommission
  CodeCentreEpreuve: String,
  LibelleCentreEpreuve: String, // centre : name
  CodePostalCentreEpreuve: String, // centre : numCentre
  VilleCentreEpreuve: String,
  CodeAcademieCentreEpreuve: String,
  LibelleAcademieCentreEpreuve: String, // academie : name
  CodeSalle: String,
  DateVisioDebut: String, //  candidat : date+heure
  DateVisioFin: String,
  Concours: String, // concours : numero
  Epreuve: String, // candidat : numInscription partie 2
  Option: String,
  Concours_Libelle: String, // concours : name
  DateConcoursDebut: String, // concours : start
  DateConcoursFin: String, // concours : end
  DateVisioDebutLocale: String, // candidat : heure locale (ajoutée à l'heure de convocation)
  DateVisioFinLocale: String,
}).newContext();

function createOrGetAcad(name) {
  // console.log('createOrGetAcad', name);
  const academie = Academie.findOne({ name });
  if (academie !== undefined) {
    return academie._id;
  }
  console.log('Create Académie : ', name);
  try {
    return Academie.insert({ name });
  } catch {
    return '';
  }
}

function createOrGetConcours(name, numero, start, end) {
  // console.log('createOrGetConcours', name, numero, start, end);
  const concours = Concours.findOne({ name, numero });
  if (concours !== undefined) {
    return concours._id;
  }
  console.log('Create Concours : ', name, numero);
  try {
    return Concours.insert({ name, numero, start, end });
  } catch {
    return '';
  }
}

function createOrGetCentre(academie, name, numCentre) {
  // console.log('createOrGetCentre', academie, name, numCentre);
  const centre = Centre.findOne({ academie, name, numCentre });
  if (centre !== undefined) {
    return centre._id;
  }
  console.log('Create Centre : ', name, numCentre);
  try {
    return Centre.insert({ academie, name, numCentre });
  } catch {
    return '';
  }
}

function createOrGetCommission(concours, name, numCommission, codeJury) {
  // console.log('createOrGetCommission', concours, name, numCommission, codeJury);
  const commission = Commission.findOne({ concours, name, numCommission });
  if (commission !== undefined) {
    return commission._id;
  }
  console.log('Create Commission : ', name, numCommission);
  try {
    return Commission.insert({ concours, name, numCommission, codeJury });
  } catch {
    return '';
  }
}

function createCandidat(centre, commission, numInscription, candidat, date, heure) {
  // console.log('createCandidat', centre, commission, numInscription, candidat, date, heure);
  const candid = List.findOne({ centre, commission, numInscription, candidat });
  if (candid !== undefined) {
    return candid._id;
  }
  console.log('Create List : ', numInscription, candidat);
  try {
    return List.insert({ centre, commission, numInscription, candidat, date, heure });
  } catch {
    return '';
  }
}

function batchImportData(row) {
  // retourne un message d'erreur ou "" si tout est ok
  // console.log('batchImportData', row);

  validationContext.validate(row);
  if (!validationContext.isValid()) {
    return `${validationContext.validationErrors()[0].name} : ${validationContext.validationErrors()[0].type}`;
  }

  // Récupération / création de l'académie
  const acadId = createOrGetAcad(row.LibelleAcademieCentreEpreuve.trim());
  if (!acadId) {
    return `Problème avec l'académie "${row.LibelleAcademieCentreEpreuve.trim()}"`;
  }

  // Récupération / création du concours
  const debutConc = moment(row.DateConcoursDebut.slice(0, 10), 'DD-MM-YYYY').toDate();
  const finConc = moment(row.DateConcoursFin.slice(0, 10), 'DD-MM-YYYY').toDate();
  const concId = createOrGetConcours(row.Concours_Libelle.trim(), row.Concours.trim(), debutConc, finConc);
  if (!concId) {
    return `Problème avec le concours "${row.Concours_Libelle.trim()}-${row.Concours.trim()}"`;
  }

  // Récupération / création du centre
  const centreId = createOrGetCentre(acadId, row.LibelleCentreEpreuve.trim(), row.CodePostalCentreEpreuve.trim());
  if (!centreId) {
    return `Problème avec le centre "${row.LibelleCentreEpreuve.trim()}-${row.CodePostalCentreEpreuve.trim()}"`;
  }

  // Récupération / création de la commission
  const commId = createOrGetCommission(concId, row.Commission.trim(), row.CleCommission.trim(), ''); // le codeJury est laissé vide
  if (!commId) {
    return `Problème avec la commission "${row.Commission.trim()}-${row.CleCommission.trim()}"`;
  }

  // Création du candidat
  const dateConvoc = moment(row.DateVisioDebut.slice(0, 10), 'DD-MM-YYYY').toDate();
  const heureConvoc = row.DateVisioDebut.slice(11, 16);

  let heureStr;
  if (row.DateVisioDebutLocale.length > 5 && row.DateVisioDebutLocale !== row.DateVisioDebut) {
    heureStr = `${heureConvoc} (Paris) / ${row.DateVisioDebutLocale.slice(11, 16)} (locale)`;
  } else {
    heureStr = heureConvoc;
  }

  const res = createCandidat(
    centreId,
    commId,
    `${row.NumeroCandidat.trim()}-${row.Epreuve.trim()}`,
    `${row.Nom.trim()} ${row.Prenom.trim()}`,
    dateConvoc,
    heureStr,
  );
  if (!res) {
    // eslint-disable-next-line max-len
    return `Problème avec le candidat "${row.Nom.trim()} ${row.Prenom.trim()}-${row.NumeroCandidat.trim()}-${row.Epreuve.trim()}"`;
  }

  return ''; // tout est ok
}

const csvImport = Meteor.methods({
  'list.upload': function ({ fileContent }) {
    // console.log('start import');
    let allOK = true;
    const importLog = [];
    fileContent.forEach((line) => {
      const ret = batchImportData(line.data);
      if (ret) {
        allOK = false;
        importLog.push(`${line.data.NumeroCandidat}-${line.data.Nom}\t\t=>\t${ret}\n`);
      }
    });
    // console.log('completed', allOK, importLog);
    return { allOK, importLog };
  },
});

export default csvImport;
