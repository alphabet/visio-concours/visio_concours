/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */

import { PublicationCollector } from 'meteor/johanbrook:publication-collector';
import { chai, assert } from 'meteor/practicalmeteor:chai';
import { Meteor } from 'meteor/meteor';
import { _ } from 'meteor/underscore';
import faker from 'faker';
import { Random } from 'meteor/random';
import { Factory } from 'meteor/dburles:factory';
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';
import './publications';

import Centre from '../centre';
import { createCenter, updateCenter, removeCenter } from '../methods';

describe('centre', function () {
  describe('mutators', function () {
    it('builds correctly from factory', function () {
      const newCentre = Factory.create('centre');
      assert.typeOf(newCentre, 'object');
    });
  });
  describe('publications', function () {
    let userId;
    before(function () {
      Meteor.users.remove({});
      userId = Accounts.createUser({
        username: faker.internet.email(),
        password: 'toto',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Meteor.users.update(userId, { $set: { isActive: true } });
      Centre.remove({});
      _.times(4, () => {
        Factory.create('centre');
      });
    });
    describe('centre.all', function () {
      it('sends all centres', function (done) {
        const collector = new PublicationCollector({ userId });
        collector.collect('centre.all', (collections) => {
          chai.assert.equal(collections.centre.length, 4);
          done();
        });
      });
    });
  });
  describe('methods', function () {
    let userId;
    let saisieUserId;
    let adminUserId;
    beforeEach(function () {
      Meteor.users.remove({});
      Meteor.roles.remove({});
      Meteor.roleAssignment.remove({});
      Centre.remove({});
      const email = faker.internet.email();
      const email2 = faker.internet.email();
      adminUserId = Accounts.createUser({
        email: 'eole@ac-test.fr',
        username: 'eole',
        password: 'eole',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Roles.createRole('admin');
      Roles.addUsersToRoles(adminUserId, 'admin');
      Meteor.users.update(adminUserId, { $set: { isActive: true } });

      saisieUserId = Accounts.createUser({
        email2,
        username: email2,
        password: 'toto',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Roles.createRole('saisie');
      Roles.addUsersToRoles(saisieUserId, 'saisie');
      Meteor.users.update(saisieUserId, { $set: { isActive: true } });

      userId = Accounts.createUser({
        email,
        username: email,
        password: 'toto',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Meteor.users.update(userId, { $set: { isActive: true } });
    });
    describe('createCenter', function () {
      it('does user with admin role create a centre', function () {
        createCenter._execute({ userId: adminUserId }, { academie: Random.id(), name: 'monCent', numCentre: '007' });
        const reqCent = Centre.findOne({ name: 'monCent' });
        assert.typeOf(reqCent, 'object');
      });
      it('does user with saisie role create a centre', function () {
        createCenter._execute({ userId: saisieUserId }, { academie: Random.id(), name: 'monCent2', numCentre: '008' });
        const reqCent = Centre.findOne({ name: 'monCent2' });
        assert.typeOf(reqCent, 'object');
      });
      it('does not create a centre if user has no role', function () {
        assert.throws(
          () => {
            createCenter._execute({ userId }, { academie: Random.id(), name: 'monCent3', numCentre: '009' });
          },
          Meteor.Error,
          /api.center.createCenter.notPermitted/,
        );
      });
      it('does not create a centre if no user is connected', function () {
        assert.throws(
          () => {
            createCenter._execute({}, { academie: Random.id(), name: 'monCent4', numCentre: '010' });
          },
          Meteor.Error,
          /api.center.createCenter.notPermitted/,
        );
      });
      it('does not create a duplicate centre', function () {
        createCenter._execute({ userId: adminUserId }, { academie: Random.id(), name: 'monCent', numCentre: '007' });
        assert.throws(
          () => {
            createCenter._execute(
              { userId: adminUserId },
              { academie: Random.id(), name: 'monCent', numCentre: '007' },
            );
          },
          Meteor.Error,
          /api.center.createCenter.duplicateEntry/,
        );
      });
    });
    describe('updateCenter', function () {
      it('does user with admin role update a centre', function () {
        const centerId = Factory.create('centre', { academie: Random.id(), name: 'monCent', numCentre: '007' })._id;
        updateCenter._execute(
          { userId: adminUserId },
          { centerId, data: { academie: Random.id(), name: 'updatedCent', numCentre: '007' } },
        );
        const reqCent = Centre.findOne({ _id: centerId });
        assert.typeOf(reqCent, 'object');
        assert.equal(reqCent.name, 'updatedCent');
      });
      it('does user with saisie role update a centre', function () {
        const centerId = Factory.create('centre', { academie: Random.id(), name: 'monCent2', numCentre: '008' })._id;
        updateCenter._execute(
          { userId: saisieUserId },
          { centerId, data: { academie: Random.id(), name: 'updatedCent2', numCentre: '008' } },
        );
        const reqCent = Centre.findOne({ _id: centerId });
        assert.typeOf(reqCent, 'object');
        assert.equal(reqCent.name, 'updatedCent2');
      });
      it('does not update a centre if user has no role', function () {
        const centerId = Factory.create('centre', { academie: Random.id(), name: 'monCent3', numCentre: '009' })._id;
        assert.throws(
          () => {
            updateCenter._execute(
              { userId },
              { centerId, data: { academie: Random.id(), name: 'updatedCent3', numCentre: '009' } },
            );
          },
          Meteor.Error,
          /api.center.updateCenter.notPermitted/,
        );
      });
      it('does not update a centre if no user is connected', function () {
        const centerId = Factory.create('centre', { academie: Random.id(), name: 'monCent4', numCentre: '010' })._id;
        assert.throws(
          () => {
            updateCenter._execute(
              {},
              { centerId, data: { academie: Random.id(), name: 'updatedCent4', numCentre: '010' } },
            );
          },
          Meteor.Error,
          /api.center.updateCenter.notPermitted/,
        );
      });
      it('does not update a not found centre', function () {
        assert.throws(
          () => {
            updateCenter._execute(
              { userId: adminUserId },
              { centerId: Random.id(), data: { academie: Random.id(), name: 'noCent', numCentre: '007' } },
            );
          },
          Meteor.Error,
          /api.center.updateCenter.unknownCenter/,
        );
      });
      it('does not update to an existing centre', function () {
        const academie = Random.id();
        Factory.create('centre', { academie, name: 'existingCent', numCentre: '007' });
        const centerId = Factory.create('centre', { academie: Random.id(), name: 'monCent', numCentre: '000' })._id;
        assert.throws(
          () => {
            updateCenter._execute(
              { userId: adminUserId },
              { centerId, data: { academie, name: 'existingCent', numCentre: '007' } },
            );
          },
          Meteor.Error,
          /api.center.updateCenter.duplicateEntry/,
        );
      });
    });
    describe('removeCenter', function () {
      it('does user with admin role remove a centre', function () {
        const centerId = Factory.create('centre', { name: 'monCent' })._id;
        assert.typeOf(Centre.findOne({ _id: centerId }), 'object');
        removeCenter._execute({ userId: adminUserId }, { centerId });
        assert.equal(Centre.findOne({ _id: centerId }), undefined);
      });
      it('does user with saisie role remove a centre', function () {
        const centerId = Factory.create('centre', { name: 'monCent' })._id;
        assert.typeOf(Centre.findOne({ _id: centerId }), 'object');
        removeCenter._execute({ userId: saisieUserId }, { centerId });
        assert.equal(Centre.findOne({ _id: centerId }), undefined);
      });
      it('does not remove a centre if user has no role', function () {
        const centerId = Factory.create('centre', { name: 'monCent' })._id;
        assert.typeOf(Centre.findOne({ _id: centerId }), 'object');
        assert.throws(
          () => {
            removeCenter._execute({ userId }, { centerId });
          },
          Meteor.Error,
          /api.center.removeCenter.notPermitted/,
        );
      });
      it('does not remove a centre if no user is connected', function () {
        const centerId = Factory.create('centre', { name: 'monCent' })._id;
        assert.typeOf(Centre.findOne({ _id: centerId }), 'object');
        assert.throws(
          () => {
            removeCenter._execute({}, { centerId });
          },
          Meteor.Error,
          /api.center.removeCenter.notPermitted/,
        );
      });
      it('does not remove a not found centre', function () {
        const centerId = Factory.create('centre', { name: 'monCent' })._id;
        assert.typeOf(Centre.findOne({ _id: centerId }), 'object');
        assert.throws(
          () => {
            removeCenter._execute({ userId: adminUserId }, { centerId: Random.id() });
          },
          Meteor.Error,
          /api.center.removeCenter.unknownCenter/,
        );
      });
      it('does not remove a centre which still has candidat', function () {
        const centerId = Factory.create('centre', { name: 'monCent' })._id;
        assert.typeOf(Centre.findOne({ _id: centerId }), 'object');
        Factory.create('list', { centre: centerId });
        assert.throws(
          () => {
            removeCenter._execute({ userId: adminUserId }, { centerId });
          },
          Meteor.Error,
          /api.center.removeCenter.relatedDataStillExist/,
        );
      });
    });
  });
});
