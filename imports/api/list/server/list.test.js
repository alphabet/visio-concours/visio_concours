/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */

import { PublicationCollector } from 'meteor/johanbrook:publication-collector';
import { chai, assert } from 'meteor/practicalmeteor:chai';
import { Meteor } from 'meteor/meteor';
import { _ } from 'meteor/underscore';
import faker from 'faker';
import { Random } from 'meteor/random';
import { Factory } from 'meteor/dburles:factory';
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';
import './publications';

import List from '../list';
import { createCandidat, updateCandidat, removeCandidat, updateColor, updateDomain } from '../methods';
import Concours from '../../concours/concours';
import Candidission from '../../commission/commission';

describe('list', function () {
  describe('mutators', function () {
    it('builds correctly from factory', function () {
      const newList = Factory.create('list');
      assert.typeOf(newList, 'object');
    });
  });
  describe('publications', function () {
    let userId;
    before(function () {
      Meteor.users.remove({});
      userId = Accounts.createUser({
        username: faker.internet.email(),
        password: 'toto',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Meteor.users.update(userId, { $set: { isActive: true } });
      List.remove({});
      _.times(4, () => {
        Factory.create('list');
      });
    });
    describe('list.all', function () {
      it('sends all lists', function (done) {
        const collector = new PublicationCollector({ userId });
        collector.collect('list.all', (collections) => {
          chai.assert.equal(collections.list.length, 4);
          done();
        });
      });
    });
  });
  describe('methods', function () {
    let userId;
    let saisieUserId;
    let adminUserId;
    beforeEach(function () {
      Meteor.users.remove({});
      Meteor.roles.remove({});
      Meteor.roleAssignment.remove({});
      Concours.remove({});
      Candidission.remove({});
      List.remove({});
      const email = faker.internet.email();
      const email2 = faker.internet.email();
      adminUserId = Accounts.createUser({
        email: 'eole@ac-test.fr',
        username: 'eole',
        password: 'eole',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Roles.createRole('admin');
      Roles.addUsersToRoles(adminUserId, 'admin');
      Meteor.users.update(adminUserId, { $set: { isActive: true } });

      saisieUserId = Accounts.createUser({
        email2,
        username: email2,
        password: 'toto',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Roles.createRole('saisie');
      Roles.addUsersToRoles(saisieUserId, 'saisie');
      Meteor.users.update(saisieUserId, { $set: { isActive: true } });

      userId = Accounts.createUser({
        email,
        username: email,
        password: 'toto',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Meteor.users.update(userId, { $set: { isActive: true } });
    });
    describe('createCandidat', function () {
      it('does user with admin role create a candidat', function () {
        createCandidat._execute(
          { userId: adminUserId },
          {
            commission: Random.id(),
            centre: Random.id(),
            heure: '10h00',
            candidat: 'monCandid',
            numInscription: '007',
          },
        );
        const reqCandid = List.findOne({ candidat: 'monCandid' });
        assert.typeOf(reqCandid, 'object');
      });
      it('does user with saisie role create a candidat', function () {
        createCandidat._execute(
          { userId: saisieUserId },
          {
            commission: Random.id(),
            centre: Random.id(),
            heure: '10h00',
            candidat: 'monCandid2',
            numInscription: '008',
          },
        );
        const reqCandid = List.findOne({ candidat: 'monCandid2' });
        assert.typeOf(reqCandid, 'object');
      });
      it('does not create a candidat if user has no role', function () {
        assert.throws(
          () => {
            createCandidat._execute(
              { userId },
              {
                commission: Random.id(),
                centre: Random.id(),
                heure: '10h00',
                candidat: 'monCandid3',
                numInscription: '009',
              },
            );
          },
          Meteor.Error,
          /api.list.createCandidat.notPermitted/,
        );
      });
      it('does not create a candidat if no user is connected', function () {
        assert.throws(
          () => {
            createCandidat._execute(
              {},
              {
                commission: Random.id(),
                centre: Random.id(),
                heure: '10h00',
                candidat: 'monCandid4',
                numInscription: '010',
              },
            );
          },
          Meteor.Error,
          /api.list.createCandidat.notPermitted/,
        );
      });
      it('does not create a duplicate candidat', function () {
        createCandidat._execute(
          { userId: adminUserId },
          {
            commission: Random.id(),
            centre: Random.id(),
            heure: '10h00',
            candidat: 'monCandid5',
            numInscription: '011',
          },
        );
        assert.throws(
          () => {
            createCandidat._execute(
              { userId: adminUserId },
              {
                commission: Random.id(),
                centre: Random.id(),
                heure: '10h00',
                candidat: 'monCandid5',
                numInscription: '011',
              },
            );
          },
          Meteor.Error,
          /api.list.createCandidat.duplicateNumInscription/,
        );
      });
    });
    describe('updateCandidat', function () {
      it('does user with admin role update a candidat', function () {
        const candidatId = Factory.create('list')._id;
        updateCandidat._execute(
          { userId: adminUserId },
          {
            candidatId,
            data: {
              commission: Random.id(),
              centre: Random.id(),
              heure: '10h00',
              candidat: 'updatedCandid',
              numInscription: '007',
            },
          },
        );
        const reqCandid = List.findOne({ _id: candidatId });
        assert.typeOf(reqCandid, 'object');
        assert.equal(reqCandid.candidat, 'updatedCandid');
      });
      it('does user with saisie role update a candidat', function () {
        const candidatId = Factory.create('list')._id;
        updateCandidat._execute(
          { userId: saisieUserId },
          {
            candidatId,
            data: {
              commission: Random.id(),
              centre: Random.id(),
              heure: '10h00',
              candidat: 'updatedCandid2',
              numInscription: '008',
            },
          },
        );
        const reqCandid = List.findOne({ _id: candidatId });
        assert.typeOf(reqCandid, 'object');
        assert.equal(reqCandid.candidat, 'updatedCandid2');
      });
      it('does not update a candidat if user has no role', function () {
        const candidatId = Factory.create('list')._id;
        assert.throws(
          () => {
            updateCandidat._execute(
              { userId },
              {
                candidatId,
                data: {
                  commission: Random.id(),
                  centre: Random.id(),
                  heure: '10h00',
                  candidat: 'updatedCandid3',
                  numInscription: '009',
                },
              },
            );
          },
          Meteor.Error,
          /api.list.updateCandidat.notPermitted/,
        );
      });
      it('does not update a candidat if no user is connected', function () {
        const candidatId = Factory.create('list')._id;
        assert.throws(
          () => {
            updateCandidat._execute(
              {},
              {
                candidatId,
                data: {
                  commission: Random.id(),
                  centre: Random.id(),
                  heure: '10h00',
                  candidat: 'updatedCandid4',
                  numInscription: '010',
                },
              },
            );
          },
          Meteor.Error,
          /api.list.updateCandidat.notPermitted/,
        );
      });
      it('does not update a not found candidat', function () {
        assert.throws(
          () => {
            updateCandidat._execute(
              { userId: adminUserId },
              {
                candidatId: Random.id(),
                data: {
                  commission: Random.id(),
                  centre: Random.id(),
                  heure: '10h00',
                  candidat: 'noCandid',
                  numInscription: '007',
                },
              },
            );
          },
          Meteor.Error,
          /api.list.updateCandidat.unknownCandidat/,
        );
      });
      it('does not update to an existing candidat', function () {
        Factory.create('list', {
          candidat: 'existingCandid',
          numInscription: '007',
        });
        const candidatId = Factory.create('list', {
          candidat: 'monCandid',
          numInscription: '000',
        })._id;
        assert.throws(
          () => {
            updateCandidat._execute(
              { userId: adminUserId },
              {
                candidatId,
                data: {
                  commission: Random.id(),
                  centre: Random.id(),
                  heure: '10h00',
                  candidat: 'existingCandid',
                  numInscription: '007',
                },
              },
            );
          },
          Meteor.Error,
          /api.list.updateCandidat.duplicateNumInscription/,
        );
      });
    });
    describe('removeCandidat', function () {
      it('does user with admin role remove a candidat', function () {
        const candidatId = Factory.create('list', { candidat: 'monCandid' })._id;
        assert.typeOf(List.findOne({ _id: candidatId }), 'object');
        removeCandidat._execute({ userId: adminUserId }, { candidatId });
        assert.equal(List.findOne({ _id: candidatId }), undefined);
      });
      it('does user with saisie role remove a candidat', function () {
        const candidatId = Factory.create('list', { candidat: 'monCandid' })._id;
        assert.typeOf(List.findOne({ _id: candidatId }), 'object');
        removeCandidat._execute({ userId: saisieUserId }, { candidatId });
        assert.equal(List.findOne({ _id: candidatId }), undefined);
      });
      it('does not remove a candidat if user has no role', function () {
        const candidatId = Factory.create('list', { candidat: 'monCandid' })._id;
        assert.typeOf(List.findOne({ _id: candidatId }), 'object');
        assert.throws(
          () => {
            removeCandidat._execute({ userId }, { candidatId });
          },
          Meteor.Error,
          /api.list.removeCandidat.notPermitted/,
        );
      });
      it('does not remove a candidat if no user is connected', function () {
        const candidatId = Factory.create('list', { candidat: 'monCandid' })._id;
        assert.typeOf(List.findOne({ _id: candidatId }), 'object');
        assert.throws(
          () => {
            removeCandidat._execute({}, { candidatId });
          },
          Meteor.Error,
          /api.list.removeCandidat.notPermitted/,
        );
      });
      it('does not remove a not found candidat', function () {
        const candidatId = Factory.create('list', { candidat: 'monCandid' })._id;
        assert.typeOf(List.findOne({ _id: candidatId }), 'object');
        assert.throws(
          () => {
            removeCandidat._execute({ userId: adminUserId }, { candidatId: Random.id() });
          },
          Meteor.Error,
          /api.list.removeCandidat.unknownCandidat/,
        );
      });
    });
    describe('updateColor', function () {
      it('does user update color of a candidat', function () {
        const candidatId = Factory.create('list')._id;
        let reqCandid = List.findOne({ _id: candidatId });
        assert.typeOf(reqCandid, 'object');
        assert.equal(reqCandid.color, false);
        updateColor._execute({ userId }, { candidatId, color: true });
        reqCandid = List.findOne({ _id: candidatId });
        assert.equal(reqCandid.color, true);
      });
      it('does not update color of a candidat if no user is connected', function () {
        const candidatId = Factory.create('list')._id;
        const reqCandid = List.findOne({ _id: candidatId });
        assert.typeOf(reqCandid, 'object');
        assert.equal(reqCandid.color, false);
        assert.throws(
          () => {
            updateColor._execute({}, { candidatId, color: true });
          },
          Meteor.Error,
          /api.list.updateColor.notPermitted/,
        );
      });
    });
    describe('updateDomain', function () {
      it('does user update domain of a candidat', function () {
        const candidatId = Factory.create('list')._id;
        let reqCandid = List.findOne({ _id: candidatId });
        assert.typeOf(reqCandid, 'object');
        assert.equal(reqCandid.domain, '');
        updateDomain._execute({ userId }, { candidatId, domain: 'yo' });
        reqCandid = List.findOne({ _id: candidatId });
        assert.equal(reqCandid.domain, 'yo');
      });
      it('does not update domain of a candidat if no user is connected', function () {
        const candidatId = Factory.create('list')._id;
        const reqCandid = List.findOne({ _id: candidatId });
        assert.typeOf(reqCandid, 'object');
        assert.equal(reqCandid.domain, '');
        assert.throws(
          () => {
            updateDomain._execute({}, { candidatId, domain: 'yo' });
          },
          Meteor.Error,
          /api.list.updateDomain.notPermitted/,
        );
      });
    });
  });
});
