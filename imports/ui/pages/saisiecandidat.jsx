import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { FormGroup, Select, FormControl, InputLabel, Paper, TextField, Button, Snackbar } from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import List from '../../api/list/list';
import Commission from '../../api/commission/commission';
import Concours from '../../api/concours/concours';
import Centre from '../../api/centre/centre';
import { getAcademie } from '../utils';

const useStyles = makeStyles((theme) => ({
  main: {
    display: 'flex',
  },
  paper: {
    marginTop: theme.spacing(3),
    margin: 'auto',
    maxWidth: 800,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  validate: {
    display: 'flex',
  },
  spaceBetween: {
    width: 10,
  },
  button: { width: 150, margin: theme.spacing(1) },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function SaisieCandidat({ candidat }) {
  const history = useHistory();
  const classes = useStyles();
  let infosCandidat = {
    commission: '',
    centredexamen: '',
    nom: '',
    numInscription: '',
    date_: new Date().toISOString().substring(0, 10),
    heure: '08:00',
    heureLocale: '',
  };

  const [state, setState] = useState(infosCandidat);

  useEffect(() => {
    if (candidat.candidat !== undefined) {
      infosCandidat = {
        commission: candidat.commission,
        centredexamen: candidat.centre,
        nom: candidat.candidat,
        numInscription: candidat.numInscription,
        date_: candidat.date.toISOString().substring(0, 10),
        heure: candidat.heure.slice(0, 5),
        heureLocale: candidat.heure.length > 8 ? candidat.heure.slice(16, 21) : '',
      };
      setState(infosCandidat);
    }
  }, [candidat]);

  const [openSnack, setOpenSnack] = React.useState(false);
  const [snackData, setSnackData] = React.useState({
    text: '',
    severity: 'success',
  });

  const showAlert = (text, severity = 'success') => {
    setSnackData({ text, severity });
    setOpenSnack(true);
  };

  const handleCloseSnack = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnack(false);
  };

  const handleChange = (event) => {
    const { name } = event.target;

    setState({
      ...state,
      [name]: event.target.value,
    });
  };

  const getConcoursNum = (concoursID) => {
    const conc = Concours.findOne({ _id: concoursID }) || '';
    return conc.numero || '';
  };

  const renderCommissions = () => {
    const commissions = Commission.find().fetch() || [];
    return commissions
      .map((el) => {
        return {
          label: `${getConcoursNum(el.concours)} - ${el.numCommission} - ${el.name}`,
          value: el._id,
        };
      })
      .sort((a, b) => {
        return a.label > b.label ? 1 : a.label < b.label ? -1 : 0;
      })
      .map((el) => {
        return (
          <option value={el.value} key={el.value}>
            {el.label}
          </option>
        );
      });
  };

  const renderCentreDexamen = () => {
    // generate options with a custom sort on dynamic values
    const centres = Centre.find().fetch() || [];
    return centres
      .map((el) => {
        return {
          label: `${getAcademie(el.academie)} - ${el.name}`,
          value: el._id,
        };
      })
      .sort((a, b) => {
        return a.label > b.label ? 1 : a.label < b.label ? -1 : 0;
      })
      .map((el) => {
        return (
          <option value={el.value} key={el.value}>
            {el.label}
          </option>
        );
      });
  };

  const handleResult = (goBack, err) => {
    if (err) {
      let message = err.reason;
      if (err.error === 'validation-error') {
        // display first validation message
        message = err.details.map((info) => info.message).join('\n');
      }
      showAlert(message, 'error');
      if (typeof err.error === 'string' && err.error.includes('duplicateNumInscription')) {
        document.getElementById('numInscription').focus();
      } else {
        document.getElementById('nom').focus();
      }
    } else if (goBack) {
      history.push('/admincandidats');
    } else if (candidat.candidat === undefined) {
      showAlert('Le candidat a été ajouté');
      setState({ ...state, nom: '', numInscription: '' });
      document.getElementById('nom').value = '';
      document.getElementById('numInscription').value = '';
      document.getElementById('numInscription').focus();
    } else {
      showAlert('Le candidat a été mis à jour.');
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const goBack = event.type !== 'submit';
    const { commission, centredexamen, nom, numInscription, date_, heure, heureLocale } = state;

    if (nom === '') {
      showAlert('Un nom est nécessaire !', 'error');
      document.getElementById('nom').focus();
      return false;
    }

    if (numInscription === '') {
      showAlert("Un numéro d'inscription est nécessaire !", 'error');
      document.getElementById('numInscription').focus();
      return false;
    }

    const com = Commission.findOne({ _id: commission });
    if (com === undefined) {
      showAlert('Une commission est nécessaire !', 'error');
      return false;
    }

    const cen = Centre.findOne({ _id: centredexamen });
    if (cen === undefined) {
      showAlert("Un centre d'accueil est nécessaire !", 'error');
      return false;
    }

    let heureStr;
    if (heureLocale !== '') {
      heureStr = `${heure} (Paris) / ${heureLocale} (locale)`;
    } else {
      heureStr = heure;
    }

    const cand = {
      centre: centredexamen,
      candidat: nom,
      numInscription,
      date: date_,
      heure: heureStr,
      commission,
    };
    if (candidat.candidat === undefined) {
      Meteor.call('list.createCandidat', cand, (err) => handleResult(goBack, err));
    } else {
      Meteor.call(
        'list.updateCandidat',
        {
          candidatId: candidat._id,
          data: cand,
        },
        (err) => handleResult(goBack, err),
      );
    }
    return false;
  };

  const renderTextfield = () => {
    return (
      <div className={classes.formControl}>
        <TextField
          name="numInscription"
          margin="dense"
          autoFocus
          id="numInscription"
          label="Numéro d'inscription"
          value={state.numInscription}
          fullWidth
          onChange={handleChange}
        />
        <TextField name="nom" margin="dense" id="nom" label="Nom" value={state.nom} fullWidth onChange={handleChange} />
        <TextField
          name="date_"
          margin="dense"
          id="date_"
          label="Date"
          type="date"
          value={state.date_}
          fullWidth
          onChange={handleChange}
        />
        <TextField
          name="heure"
          margin="dense"
          id="heure"
          label="Heure (de Paris)"
          type="time"
          value={state.heure}
          fullWidth
          onChange={handleChange}
        />
        <TextField
          name="heureLocale"
          margin="dense"
          id="heureLocale"
          label="Heure locale si nécessaire"
          type="time"
          value={state.heureLocale}
          fullWidth
          onChange={handleChange}
        />
      </div>
    );
  };

  return (
    <div className={classes.main}>
      <Paper className={classes.paper}>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={openSnack}
          autoHideDuration={5000}
          onClose={handleCloseSnack}
        >
          <Alert onClose={handleCloseSnack} severity={snackData.severity}>
            {snackData.text}
          </Alert>
        </Snackbar>
        <form onSubmit={handleSubmit}>
          <FormGroup>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel htmlFor="filled-centredexamen-native-simple">
                Centre d&apos;accueil (académie + nom)
              </InputLabel>
              <Select
                native
                value={state.centredexamen}
                onChange={handleChange}
                label="Centre d'accueil (académie + nom)"
                inputProps={{
                  name: 'centredexamen',
                  id: 'filled-centredexamen-native-simple',
                }}
              >
                <option aria-label="None" value="" />
                {renderCentreDexamen(state)}
              </Select>
            </FormControl>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel htmlFor="filled-commission-native-simple">
                Commission (n° de concours + n° de commission + intitulé de commission)
              </InputLabel>
              <Select
                native
                value={state.commission}
                onChange={handleChange}
                label="Commission (n° de concours + n° de commission + intitulé de commission)"
                inputProps={{
                  name: 'commission',
                  id: 'filled-commission-native-simple',
                }}
              >
                <option aria-label="None" value="" />
                {renderCommissions(state)}
              </Select>
            </FormControl>
            {renderTextfield()}
          </FormGroup>
          <div className={classes.validate}>
            <Button
              className={classes.button}
              variant="outlined"
              color="secondary"
              onClick={() => history.push('/admincandidats')}
            >
              Retour
            </Button>
            <div className={classes.spaceBetween} />
            {candidat.candidat === undefined ? (
              <>
                <Button type="submit" variant="outlined" color="primary" className={classes.button}>
                  Soumettre et continuer
                </Button>
                <Button
                  type="button"
                  variant="outlined"
                  color="primary"
                  className={classes.button}
                  onClick={handleSubmit}
                >
                  Soumettre et retour
                </Button>
              </>
            ) : (
              <Button
                type="button"
                variant="outlined"
                color="primary"
                className={classes.button}
                onClick={handleSubmit}
              >
                Modifier
              </Button>
            )}
          </div>
        </form>
      </Paper>
    </div>
  );
}

SaisieCandidat.propTypes = {
  candidat: PropTypes.objectOf(PropTypes.element).isRequired,
};

export default withTracker(
  ({
    match: {
      params: { id },
    },
  }) => {
    let candidat = {};
    if (id !== 'new') {
      Meteor.subscribe('list.one', { candidatId: id });
      candidat = List.findOne({ _id: id }) || {};
    }
    Meteor.subscribe('centre.all');
    Meteor.subscribe('commission.all');
    Meteor.subscribe('academie.all');
    Meteor.subscribe('concours.all');
    return {
      candidat,
    };
  },
)(SaisieCandidat);
