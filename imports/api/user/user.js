import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import SimpleSchema from 'simpl-schema';
import { listUsers } from '../../startup/server/listUsers.json';
import RegEx from '../regExp';

const AppRoles = ['admin', 'saisie'];

Meteor.users.schema = new SimpleSchema({
  username: {
    type: String,
    optional: true,
    label: "Nom d'utilisateur",
  },
  firstName: {
    type: String,
    optional: true,
    label: 'Prénom',
  },
  lastName: {
    type: String,
    optional: true,
    label: 'Nom',
  },
  emails: {
    type: Array,
    optional: true,
    label: 'Adresses électroniques',
  },
  'emails.$': {
    type: Object,
  },
  'emails.$.address': {
    type: String,
    regEx: RegEx.Email,
    label: 'Adresse électronique',
  },
  'emails.$.verified': {
    type: Boolean,
    label: 'Adresse vérifiée',
  },
  createdAt: {
    type: Date,
    label: 'Date de création',
  },
  profile: {
    type: Object,
    optional: true,
    blackbox: true,
    label: 'Profil',
  },
  // Make sure this services field is in your schema if you're using element of the accounts packages
  services: {
    type: Object,
    optional: true,
    blackbox: true,
    label: 'Services',
  },
  // In order to avoid an 'Exception in setInterval callback' from Meteor
  heartbeat: {
    type: Date,
    optional: true,
    label: 'Intervalle',
  },
  isActive: {
    type: Boolean,
    defaultValue: false,
    label: 'Utilisateur activé',
  },
});

Meteor.users.attachSchema(Meteor.users.schema);

Meteor.users.adminFields = {
  username: 1,
  firstName: 1,
  lastName: 1,
  emails: 1,
  createdAt: 1,
  isActive: 1,
};

if (Meteor.isServer) {
  if (Meteor.settings.private && Meteor.settings.private.fillWithFakeData) {
    if (Meteor.users.find().count() < 1) {
      console.log('initialize user(s)');

      listUsers.forEach((u) => {
        // username@password
        const [username, password] = u.split('@');
        Accounts.createUser({
          username,
          password,
        });
        // auto created users are active by default
        Meteor.users.update({ username }, { $set: { isActive: true } });
      });
    }
  }
}

export default AppRoles;
