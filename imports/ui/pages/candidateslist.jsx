import React, { useEffect, useState } from 'react';
import { Session } from 'meteor/session';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Redirect, Link } from 'react-router-dom';
import { moment } from 'meteor/momentjs:moment';

import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

import { candidatsFilter, expandCandidatList, getNumCommission } from '../utils';

const { createSliderWithTooltip } = Slider;
const Range = createSliderWithTooltip(Slider.Range);

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
  functions: {
    display: 'flex',
    flexDirection: 'row',
    marginBottom: '30px',
  },
  unselectable: {
    userSelect: 'none',
    mozUserSelect: 'none',
    webkitUserSelect: 'none',
    msUserSelect: 'none',
  },
  cell: {
    textTransform: 'uppercase',
  },
  titleBackgroud: {
    background: '#99ddff',
    borderRadius: '3px 3px 0 0',
    display: 'flex',
    justifyContent: 'space-between',
  },
  title: {
    margin: theme.spacing(3),
    fontSize: '120%',
    alignContent: 'flex-start',
  },
  titleRight: {
    alignContent: 'flex-end',
  },
  toLeft: {
    width: '15%',
    margin: `${theme.spacing(1)}px 0`,
  },
  toRight: {
    width: '10%',
    margin: `${theme.spacing(1)}px 0`,
  },
  toCenter: {
    width: '100%',
    margin: `${theme.spacing(1)}px 50px`,
  },
  button: {
    margin: theme.spacing(3),
  },
  table: {
    margin: `0px ${theme.spacing(3)}px`,
  },
  primary: {
    color: theme.palette.primary.main,
    borderColor: theme.palette.primary.main,
  },
  secondary: {
    color: theme.palette.secondary.purple,
    borderColor: theme.palette.secondary.purple,
  },
}));

function CandidatListComponent(props) {
  const classes = useStyles();
  const [redirect, setRedirect] = useState(false);
  const [codeJury, setCodeJury] = useState('');
  const [codeJuryOk, setCodeJuryOk] = useState(undefined);
  const [searchInput, setSearchInput] = useState('');
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const [uniqDate, setUniqDate] = useState([]);
  const [steps, setSteps] = useState({});
  const [rangeLength, setRangeLength] = useState(10);
  const { candidatList, match, path, commission, thirdColumnTitle, place } = props;

  // sort dates without duplicates
  useEffect(() => {
    candidatList.sort((a, b) => {
      return a.date > b.date ? 1 : a.date < b.date ? -1 : 0;
    });
    const date = [];
    const newSteps = {};
    candidatList.map((el) => date.push(el.date));
    // deletion of duplicates
    const uniq = (a) => [...new Set(a)];
    // define labels by newSteps
    uniq(date).forEach(function displayDate(el, i) {
      newSteps[i] = <p style={{ width: '80px' }}>{moment(el).format('D MMM')}</p>;
    });
    const newRangeLength = uniq(date).length;
    setUniqDate(uniq(date));
    setSteps(newSteps);
    setRangeLength(newRangeLength);
  }, [candidatList]);

  const renderRedirect = () => {
    if (redirect) {
      return <Redirect to={path} />;
    }
    return null;
  };

  const handleSearchBar = (e) => {
    setSearchInput(e.target.value);
  };

  const handleCodeJury = (e) => {
    setCodeJury(e.target.value);
  };

  // get label for tipFormat from rangeSlide
  const dateTipFormatter = (value) => {
    return moment(uniqDate[value]).format('D MMM YY');
  };

  // Get value of tipFormat to list candidates by selected range date
  const getDateRange = (value) => {
    setStartDate(uniqDate[value[0]]);
    setEndDate(uniqDate[value[1]]);
  };

  const linkToVideoConf = (el, color) => {
    if (path === '/commission') {
      // jury can choose between main and backup domain
      return (
        <>
          <Button className={color} component={Link} to={`${match.url}/${el._id}/main`} variant="outlined">
            Connexion
          </Button>
          <Button className={color} component={Link} to={`${match.url}/${el._id}/backup`} variant="outlined">
            Connexion de secours
          </Button>
        </>
      );
    }
    return (
      <Button className={color} component={Link} to={`${match.url}/${el._id}`} variant="outlined">
        Connexion
      </Button>
    );
  };

  const checkCodeJury = () => {
    let code = '';
    const codes = Session.get('codes_jury');
    if (codes !== undefined) {
      code = codes[commission] || '';
    }
    Meteor.call('commission.checkCode', { idCommission: commission, codeJury: code }, (err, res) => {
      if (err) {
        setCodeJuryOk(false);
        console.log(err);
      } else {
        setCodeJuryOk(res);
      }
    });
  };

  const submitCodeJury = () => {
    Meteor.call('commission.checkCode', { idCommission: commission, codeJury }, (err, res) => {
      if (err) {
        console.log(err);
      } else if (res === true) {
        // code OK : store in session for future checks
        let codes = Session.get('codes_jury');
        if (codes === undefined) codes = {};
        codes[commission] = codeJury;
        Session.set('codes_jury', codes);
        setCodeJuryOk(true);
      } else {
        // si non valide : afficher un message
        console.log('MAUVAIS CODE');
        setCodeJury('');
      }
    });
  };

  const handleKeyUp = (evt) => {
    if (evt.keyCode === 13) submitCodeJury();
  };

  const renderAskCodeJury = () => {
    return (
      <Paper className={classes.root}>
        {renderRedirect()}
        <div className={classes.titleBackgroud}>
          <p className={classes.title}>
            Saisie du code pour la commission n°
            {getNumCommission(commission)}
          </p>
        </div>
        <div className={classes.functions}>
          <TextField
            autoFocus
            className={classes.button}
            placeholder="Code du jury"
            value={codeJury}
            onChange={handleCodeJury}
            onKeyUp={handleKeyUp}
          />
          <Button className={classes.button} onClick={submitCodeJury} color="secondary" variant="outlined">
            Valider
          </Button>
        </div>
        <div className={classes.toLeft}>
          <Button className={classes.button} onClick={() => setRedirect(true)} color="secondary" variant="outlined">
            Retour
          </Button>
        </div>
      </Paper>
    );
  };

  const renderCheckCode = () => {
    return (
      <Paper className={classes.root}>
        {renderRedirect()}
        <div className={classes.titleBackgroud}>
          <p className={classes.title}>Vérification du code en cours...</p>
        </div>
      </Paper>
    );
  };

  const renderList = (cList) => {
    // add information to candidatList
    const displayList = expandCandidatList(cList);
    const len = rangeLength - 1;
    const eachCandidat = () => {
      const list = candidatsFilter(searchInput, startDate, endDate, displayList).map((el) => {
        let thirdColumn = '';
        if (path === '/commission') {
          thirdColumn = el.nomCentre;
        } else {
          thirdColumn = `(${el.numCom}) ${el.nomConcours}`;
        }
        let color = '';
        if (el.color === true) {
          color = classes.secondary;
        } else {
          color = classes.primary;
        }
        return (
          <TableRow key={el.candidat} hover>
            <TableCell>{linkToVideoConf(el, color)}</TableCell>
            <TableCell>{moment(el.date).utc().format('ll')}</TableCell>
            <TableCell>{el.heure}</TableCell>
            <TableCell>{el.candidat}</TableCell>
            <TableCell>{thirdColumn}</TableCell>
          </TableRow>
        );
      });
      return list;
    };
    return (
      <Paper className={classes.root}>
        {renderRedirect()}
        <div className={classes.titleBackgroud}>
          <p className={classes.title}>Liste des candidats {place}</p>
          <div className={classes.titleRight}>
            <TextField
              autoFocus
              className={classes.button}
              placeholder="Recherche"
              onChange={handleSearchBar}
              value={searchInput}
            />
            <Button className={classes.button} onClick={() => setRedirect(true)} color="secondary" variant="outlined">
              Retour
            </Button>
          </div>
        </div>

        <div className={classes.functions}>
          <div className={classes.toCenter}>
            <p style={{ color: '#989898' }}>Sélection des dates</p>
            <Range
              className={classes.unselectable}
              marks={steps}
              dots
              allowCross={false}
              min={0}
              onAfterChange={getDateRange}
              max={len}
              defaultValue={[0, len]}
              tipFormatter={(value) => dateTipFormatter(value)}
              tipProps={{
                placement: 'top',
                prefixCls: 'rc-slider-tooltip',
              }}
            />
          </div>
        </div>

        <div className={classes.table}>
          <Table className={classes.unselectable}>
            <TableHead>
              <TableRow className={classes.cell}>
                <TableCell />
                <TableCell>date de convocation</TableCell>
                <TableCell>horaire de passage</TableCell>
                <TableCell>candidats</TableCell>
                <TableCell>{thirdColumnTitle}</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>{eachCandidat()}</TableBody>
          </Table>
        </div>

        <div className={classes.toLeft}>
          <Button className={classes.button} onClick={() => setRedirect(true)} color="secondary" variant="outlined">
            Retour
          </Button>
        </div>
      </Paper>
    );
  };

  if (path === '/commission') {
    if (codeJuryOk === true) {
      return renderList(candidatList);
    }
    if (codeJuryOk === false) {
      return renderAskCodeJury();
    }
    // check if jury code has already been given
    checkCodeJury();
    return renderCheckCode();
  }
  return renderList(candidatList);
}

CandidatListComponent.defaultProps = {
  commission: '',
};

CandidatListComponent.propTypes = {
  path: PropTypes.string.isRequired,
  match: PropTypes.objectOf(PropTypes.element).isRequired,
  candidatList: PropTypes.arrayOf(PropTypes.element).isRequired,
  thirdColumnTitle: PropTypes.string.isRequired,
  place: PropTypes.string.isRequired,
  commission: PropTypes.string,
};

export default CandidatListComponent;
