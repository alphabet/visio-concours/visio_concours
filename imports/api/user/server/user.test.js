// /* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */

import { PublicationCollector } from 'meteor/johanbrook:publication-collector';
import { chai, assert } from 'meteor/practicalmeteor:chai';
import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { _ } from 'meteor/underscore';
import faker from 'faker';
import { Random } from 'meteor/random';
import { Factory } from 'meteor/dburles:factory';
import { Accounts } from 'meteor/accounts-base';
import './publications';
import { addUser, removeUser, setActive, addRole, removeRole } from '../methods';

Factory.define('user', Meteor.users, {
  createdAt: new Date(),
  services: {
    password: {
      bcrypt: '$2a$10$w91o/mi5lRyuPdDKSUaZZOdcSXw1NfLRXY4LV7WajsqsVs5.OAw2u',
    },
  },
  username: () => Random.id(),
});

describe('user', function () {
  describe('mutators', function () {
    it('builds correctly from factory', function () {
      const user = Factory.create('user');
      assert.typeOf(user, 'object');
    });
  });
  describe('publications', function () {
    let userId;
    let adminUserId;
    beforeEach(function () {
      Meteor.users.remove({});
      Meteor.roles.remove({});
      Meteor.roleAssignment.remove({});
      const email = faker.internet.email();
      adminUserId = Accounts.createUser({
        email: 'eole@ac-test.fr',
        username: 'eole',
        password: 'eole',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Roles.createRole('admin');
      Roles.addUsersToRoles(adminUserId, 'admin');
      Meteor.users.update(adminUserId, { $set: { isActive: true } });

      userId = Accounts.createUser({
        email,
        username: email,
        password: 'toto',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Meteor.users.update(userId, { $set: { isActive: true } });
      _.times(4, () => {
        Factory.create('user');
      });
    });
    describe('user.all', function () {
      it('sends all users to admin user', function (done) {
        const collector = new PublicationCollector({ userId: adminUserId });
        collector.collect('user.all', (collections) => {
          chai.assert.equal(collections.users.length, 6);
          done();
        });
      });
      it('sends no users to non admin user', function (done) {
        const collector = new PublicationCollector({ userId });
        collector.collect('user.all', (collections) => {
          chai.assert.equal(collections.users, undefined);
          done();
        });
      });
    });
    describe('roles.admin', function () {
      it('sends all admin users to admin user', function (done) {
        const collector = new PublicationCollector({ userId: adminUserId });
        collector.collect('roles.admin', (collections) => {
          assert.equal(collections['role-assignment'].length, 1);
          const assignment = collections['role-assignment'][0];
          assert.equal(assignment.user._id, adminUserId);
          assert.equal(assignment.role._id, 'admin');
          done();
        });
      });
      it('sends no admin users to non admin user', function (done) {
        const collector = new PublicationCollector({ userId });
        collector.collect('roles.admin', (collections) => {
          chai.assert.equal(collections['role-assignment'], undefined);
          done();
        });
      });
    });
  });
  describe('methods', function () {
    let userId;
    let adminUserId;
    beforeEach(function () {
      Meteor.users.remove({});
      Meteor.roles.remove({});
      Meteor.roleAssignment.remove({});
      const email = faker.internet.email();
      adminUserId = Accounts.createUser({
        email: 'eole@ac-test.fr',
        username: 'eole',
        password: 'eole',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Roles.createRole('admin');
      Roles.addUsersToRoles(adminUserId, 'admin');
      Meteor.users.update(adminUserId, { $set: { isActive: true } });

      userId = Accounts.createUser({
        email,
        username: email,
        password: 'toto',
        structure: faker.company.companyName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
      Meteor.users.update(userId, { $set: { isActive: true } });
    });
    describe('addUser', function () {
      it('does anyone add a new user with an email in the whiteDomains', function () {
        addUser._execute({}, { email: 'yo@yo.gouv.fr', password: 'toto' });
        const user = Meteor.users.findOne({ username: 'yo@yo.gouv.fr' });
        assert.equal(user.username, 'yo@yo.gouv.fr');
      });
      it('does not add a new user if email not in the whiteDomains', function () {
        assert.throws(
          () => {
            addUser._execute({}, { email: 'yo@yo.fr', password: 'toto' });
          },
          Meteor.Error,
          /api.user.addUser.unauthorizedEmail/,
        );
      });
    });
    describe('removeUser', function () {
      it('does user with admin role remove a non admin user', function () {
        let user = Meteor.users.findOne({ _id: userId });
        assert.typeOf(user, 'object');
        removeUser._execute({ userId: adminUserId }, { userId });
        user = Meteor.users.findOne({ _id: userId });
        assert.equal(user, undefined);
      });
      it('does not user with admin role remove an admin user', function () {
        assert.throws(
          () => {
            removeUser._execute({ userId: adminUserId }, { userId: adminUserId });
          },
          Meteor.Error,
          /api.user.removeUser.cannotRemoveAdmin/,
        );
      });
      it('does not user with no role remove a user', function () {
        assert.throws(
          () => {
            removeUser._execute({ userId }, { userId });
          },
          Meteor.Error,
          /api.user.removeUser.notPermitted/,
        );
      });
      it('does not remove a user if no user is connected', function () {
        assert.throws(
          () => {
            removeUser._execute({}, { userId });
          },
          Meteor.Error,
          /api.user.removeUser.notPermitted/,
        );
      });
    });
    describe('setActive', function () {
      it('does user with admin role unset active a non admin user', function () {
        let user = Meteor.users.findOne({ _id: userId });
        assert.equal(user.isActive, true);
        setActive._execute({ userId: adminUserId }, { userId, active: false });
        user = Meteor.users.findOne({ _id: userId });
        assert.equal(user.isActive, false);
      });
      it('does not user with admin role unset active an admin user', function () {
        assert.throws(
          () => {
            setActive._execute({ userId: adminUserId }, { userId: adminUserId, active: false });
          },
          Meteor.Error,
          /api.user.setActive.cannotDisableAdmin/,
        );
      });
      it('does not user with no role unset active a user', function () {
        assert.throws(
          () => {
            setActive._execute({ userId }, { userId, active: false });
          },
          Meteor.Error,
          /api.user.setActive.notPermitted/,
        );
      });
      it('does not unset active a user if no user is connected', function () {
        assert.throws(
          () => {
            setActive._execute({}, { userId, active: false });
          },
          Meteor.Error,
          /api.user.setActive.notPermitted/,
        );
      });
      it('does not user with admin role unset active an unknown user', function () {
        assert.throws(
          () => {
            setActive._execute({ userId: adminUserId }, { userId: Random.id(), active: false });
          },
          Meteor.Error,
          /api.user.setActive.unknownUser/,
        );
      });
    });
    describe('addRole', function () {
      it('does user with admin role add role to a non admin user', function () {
        Roles.createRole('saisie');
        addRole._execute({ userId: adminUserId }, { userId, role: 'saisie' });
        assert.equal(Roles.userIsInRole(userId, 'saisie'), true);
      });
      it('does not user with no role add role to a user', function () {
        Roles.createRole('saisie');
        assert.throws(
          () => {
            addRole._execute({ userId }, { userId, role: 'saisie' });
          },
          Meteor.Error,
          /api.user.addRole.notPermitted/,
        );
      });
      it('does not add role to a user if no user is connected', function () {
        Roles.createRole('saisie');
        assert.throws(
          () => {
            addRole._execute({}, { userId, role: 'saisie' });
          },
          Meteor.Error,
          /api.user.addRole.notPermitted/,
        );
      });
      it('does not user with admin role add role to an unknown user', function () {
        Roles.createRole('saisie');
        assert.throws(
          () => {
            addRole._execute({ userId: adminUserId }, { userId: Random.id(), role: 'saisie' });
          },
          Meteor.Error,
          /api.user.addRole.unknownUser/,
        );
      });
    });
    describe('removeRole', function () {
      it('does user with admin role remove role to a non admin user', function () {
        Roles.createRole('saisie');
        addRole._execute({ userId: adminUserId }, { userId, role: 'saisie' });
        assert.equal(Roles.userIsInRole(userId, 'saisie'), true);
        removeRole._execute({ userId: adminUserId }, { userId, role: 'saisie' });
        assert.equal(Roles.userIsInRole(userId, 'saisie'), false);
      });
      it('does not user with no role remove role to a user', function () {
        Roles.createRole('saisie');
        assert.throws(
          () => {
            removeRole._execute({ userId }, { userId, role: 'saisie' });
          },
          Meteor.Error,
          /api.user.removeRole.notPermitted/,
        );
      });
      it('does not remove role to a user if no user is connected', function () {
        Roles.createRole('saisie');
        assert.throws(
          () => {
            removeRole._execute({}, { userId, role: 'saisie' });
          },
          Meteor.Error,
          /api.user.removeRole.notPermitted/,
        );
      });
      it('does not user with admin role remove role to an unknown user', function () {
        Roles.createRole('saisie');
        assert.throws(
          () => {
            removeRole._execute({ userId: adminUserId }, { userId: Random.id(), role: 'saisie' });
          },
          Meteor.Error,
          /api.user.removeRole.unknownUser/,
        );
      });
    });
  });
});
