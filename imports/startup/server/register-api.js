import '../../api/user/user';
import '../../api/user/methods';
import '../../api/user/server/publications';
import '../../api/academie/academie';
import '../../api/academie/methods';
import '../../api/academie/server/publications';
import '../../api/concours/concours';
import '../../api/concours/methods';
import '../../api/concours/server/publications';
import '../../api/centre/centre';
import '../../api/centre/methods';
import '../../api/centre/server/publications';
import '../../api/commission/commission';
import '../../api/commission/methods';
import '../../api/commission/server/publications';
import '../../api/list/list';
import '../../api/list/methods';
import '../../api/log/log';
import '../../api/log/methods';
import '../../api/list/server/publications';
import '../../api/list/server/import';
import '../../api/migrations';
