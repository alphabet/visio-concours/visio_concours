import SimpleSchema from 'simpl-schema';
import { Random } from 'meteor/random';
import { Factory } from 'meteor/dburles:factory';
import RegEx from '../regExp';

const Centre = new Mongo.Collection('centre');

// Deny all client-side updates since we will be using methods to manage this collection
Centre.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

Centre.schema = new SimpleSchema({
  name: {
    type: String,
    min: 1,
    label: "Intitulé du centre d'accueil",
  },
  numCentre: {
    type: String,
    min: 1,
    index: true,
    unique: true,
    label: "Numéro du Centre d'accueil",
  },
  academie: {
    type: String,
    regEx: RegEx.Id,
    min: 1,
    label: "Identifiant de l'académie",
  },
});

Centre.attachSchema(Centre.schema);

Centre.publicFields = {
  _id: 1,
  name: 1,
  numCentre: 1,
  academie: 1,
};

Factory.define('centre', Centre, {
  name: () => Random.id(),
  numCentre: () => Random.id(),
  academie: () => Random.id(),
});

export default Centre;
