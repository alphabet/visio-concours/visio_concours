import { Migrations } from 'meteor/percolate:migrations';
import { Meteor } from 'meteor/meteor';
import '../imports/startup/server/ValidationError';
import '../imports/startup/server/register-api';
import '../imports/startup/server/accounts';

Meteor.startup(() => {
  Migrations.migrateTo('latest');
});
