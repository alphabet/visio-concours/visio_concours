import SimpleSchema from 'simpl-schema';
import { Random } from 'meteor/random';
import { Factory } from 'meteor/dburles:factory';
import RegEx from '../regExp';

const Commission = new Mongo.Collection('commission');

// Deny all client-side updates since we will be using methods to manage this collection
Commission.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

Commission.schema = new SimpleSchema({
  name: {
    type: String,
    min: 1,
    label: 'Intitulé de la commission',
  },
  numCommission: {
    type: String,
    min: 1,
    label: 'Numéro de commission',
  },
  concours: {
    type: String,
    regEx: RegEx.Id,
    min: 1,
    label: 'Identifiant du concours',
  },
  codeJury: {
    type: String,
    label: "Code d'accès à la commission",
    defaultValue: '',
  },
});

Commission.attachSchema(Commission.schema);

Commission.publicFields = {
  _id: 1,
  name: 1,
  numCommission: 1,
  concours: 1,
  codeJury: 1,
};

Factory.define('commission', Commission, {
  name: () => Random.id(),
  numCommission: () => Random.id(),
  concours: () => Random.id(),
});

export default Commission;
