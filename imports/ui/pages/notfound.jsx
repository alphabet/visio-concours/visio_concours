import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';

import { Button, Grid, Typography } from '@material-ui/core';

const styles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  form: {
    padding: theme.spacing(3),
    position: 'relative',
    textAlign: 'center',
    backgroundColor: 'white',
    marginTop: theme.spacing(3),
    width: 400,
  },
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%', // Fix IE 11 issue.
  },
  submit: {
    margin: theme.spacing(2),
    width: 150,
  },
}));

/** Render a Not Found page if the user enters a URL that doesn't match element route. */
export default function NotFound() {
  const classes = styles();
  const [redirect, setRedirect] = useState(false);

  const renderRedirect = () => {
    if (redirect) {
      return <Redirect to="/home" />;
    }
    return '';
  };

  return (
    <div className={classes.root}>
      <div className={classes.form}>
        {renderRedirect()}
        <Grid container component="main" className={classes.container} spacing={2}>
          <Grid item xs={12}>
            <Typography variant="h6">Page inexistante</Typography>
          </Grid>
          <Grid item xs={12} elevation={6}>
            <Button className={classes.submit} onClick={() => setRedirect(true)} color="secondary" variant="outlined">
              Retour
            </Button>
          </Grid>
        </Grid>
      </div>
    </div>
  );
}
